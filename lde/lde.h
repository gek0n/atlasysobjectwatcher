#ifndef __LDE_H__
#define __LDE_H__

extern INT __stdcall LDE(PVOID inPtr, INT type);

typedef enum {
  LDE_BITNESS_32 = 32,
  LDE_BITNESS_64 = 64,
} lde_bitness_t;

static __inline size_t GetInstructionLen(void *addr, lde_bitness_t bitness)
{
  return LDE(addr, bitness);
}

#endif /* __LDE_H__ */
