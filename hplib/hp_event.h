/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_EVENT_H__
#define __HP_EVENT_H__

typedef enum {
   HP_EVENT_NOTIFY,
   HP_EVENT_SYNC,
   HP_EVENT_MAX
} hp_event_type_t;

#define HP_WAIT_NO_TIMEOUT -1
#define HP_WAIT_NO_STOPOBJ -2

#if defined(HP_WIN_KM)
  #include <hplib/nt/os_event.h>
#elif defined(HP_WIN_UM)
  #include <hplib/win32/os_event.h>
#elif defined(HP_LINUX_KM)
  #include <hplib/lnx/os_event.h>
#elif defined(HP_OSX_KM)
  #include <hplib/xnu/os_event.h>
#elif defined(HP_OSX_UM)
  #include <hplib/posix/os_event.h>
#endif

int32 hp_Sleep(uint32 toUs, logical interrupt);

int32 hp_EventCreate(hp_event_t *evt, hp_event_type_t type);
int32 hp_EventWait(hp_event_t *evt, int32 toMs, logical interrupt);
void hp_EventSet(hp_event_t *evt);
void hp_EventClear(hp_event_t *evt);
logical hp_EventIsSignaled(hp_event_t *evt);
void hp_EventDestroy(hp_event_t *evt);

#endif
