#ifndef __HP_SORT_H__
#define __HP_SORT_H__

void hp_Qsort(
  void *base,
  size_t nel,
  size_t width,
  int32 (*compar)(const void *, const void *));

#endif // __HP_SORT_H__
