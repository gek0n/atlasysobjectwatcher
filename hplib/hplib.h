/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPLIB_H__
#define __HPLIB_H__

#include <hplib/hp_def.h>
#include <hplib/hp_inc.h>
#include <hplib/hp_macro.h>
#include <hplib/hp_types.h>
#include <hplib/hp_tweaks.h>
#include <hplib/hp_atomic.h>
#include <hplib/hp_dlist.h>
#include <hplib/hp_log.h>
#include <hplib/hp_error.h>
#include <hplib/hp_heap.h>
#include <hplib/hp_malloc.h>
#include <hplib/hp_strops.h>
#include <hplib/hp_memops.h>
#include <hplib/hp_fileops.h>
#include <hplib/hp_time.h>
#include <hplib/hp_sysmod.h>
#include <hplib/hp_security.h>
#include <hplib/hp_pptricks.h>
#include <hplib/hp_event.h>
#include <hplib/hp_spinlock.h>
#include <hplib/hp_mutex.h>
#include <hplib/hp_thread.h>
#include <hplib/hp_membuf.h>
#include <hplib/hp_iterator.h>
#include <hplib/hp_bitmap.h>
#include <hplib/hp_idtinfo.h>
#include <hplib/hp_rundown.h>
#include <hplib/hp_byteops.h>
#include <hplib/hp_hashops.h>
#include <hplib/hp_queue.h>
#include <hplib/hp_stack.h>
#include <hplib/hp_fsdir.h>
#include <hplib/hp_sysinfo.h>
#include <hplib/hp_math.h>
#include <hplib/hp_heappool.h>
#include <hplib/hp_hashtable.h>
#include <hplib/hp_cache.h>
#include <hplib/hp_netutils.h>
#include <hplib/hp_time.h>
#include <hplib/hp_random.h>
#include <hplib/hp_uuid.h>
#include <hplib/hp_exectx.h>
#include <hplib/hp_snprintf.h>
#include <hplib/hp_growablebuf.h>
#include <hplib/hp_cpu.h>
#include <hplib/hp_components.h>
#include <hplib/hp_drvinstall.h>
#include <hplib/hp_svcinstall.h>
#include <hplib/hp_base64.h>
#include <hplib/hp_uri.h>
#include <hplib/hp_sort.h>
#include <hplib/hp_slock.h>
#include <hplib/hp_dpc.h>
#include <hplib/hp_workitem.h>

#if defined(HP_WIN_KM)
  #include <hplib/nt/os_inc.h>
#endif

#if defined(HP_WIN_UM)
  #include <hplib/win32/os_inc.h>
#endif

#if defined(HP_OSX_KM)
  #include <hplib/xnu/os_inc.h>
#endif

#if defined(HP_OSX_UM)
  #include <hplib/posix/os_inc.h>
#endif

#if defined(HP_LINUX_KM)
  #include <hplib/lnx/os_inc.h>
#endif

int32 HplibIni(void);
void HplibFin(void);

int32 HplibIni2(void);
void HplibFin2(void);

#endif /* __HPLIB_H__ */
