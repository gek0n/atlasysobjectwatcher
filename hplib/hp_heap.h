/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __CTHEAP_H__
#define __CTHEAP_H__

#if HP_TWEAK_MALLOC_TRACKING_ENABLE
void *hp_MallocAtomicDbg(size_t size, const char *file, int line);
void hp_FreeAtomicDbg(void *ptr, const char *file, int line);
void *hp_ReallocAtomicDbg(void *ptr, size_t size, const char *file, int32 line);

#define hp_MallocAtomic(size) hp_MallocAtomicDbg(size, __FILE__, __LINE__)
#define hp_FreeAtomic(ptr) hp_FreeAtomicDbg(ptr, __FILE__, __LINE__)
#define hp_ReallocAtomic(ptr, size) hp_ReallocAtomicDbg(ptr, size, __FILE__, __LINE__)
#else
void *hp_MallocAtomic(size_t size);
void hp_FreeAtomic(void *ptr);
void *hp_ReallocAtomic(void *ptr, size_t size);
#endif

int32 ct_HeapIni(void);
void ct_HeapFin(void);


#endif /* __CTHEAP_H__ */
