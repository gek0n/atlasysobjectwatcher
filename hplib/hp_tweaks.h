/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_TWEAKS_H__
#define __HP_TWEAKS_H__

/************************************************************************/
/* Compile-time settings for hplib                                      */
/************************************************************************/

/* Log all threads before and after executing thread function on the DBG_IMPORTANT level */
#define HP_TWEAK_THREAD_LOGGING_ENABLE FALSE

/* Use lock-free heap implementation */
#define HP_TWEAK_USE_LOCKFREE_MALLOC TRUE

/* Track memory allocated by hp_Malloc() */
#if defined(HP_DEBUG)
#define HP_TWEAK_MALLOC_TRACKING_ENABLE TRUE
#endif

/* Use paged pool where appropriate */
#ifdef HP_WIN_KM
#define HP_ENGAGE_PAGED_POOL
#endif

#endif /* __HP_TWEAKS_H__ */
