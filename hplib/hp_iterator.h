/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_ITERATOR_H__
#define __HP_ITERATOR_H__

typedef enum hp_iter_ret {
  HP_ITER_RET_CONTINUE = 1,
  HP_ITER_RET_CONTINUE_HIGH = 2,
  HP_ITER_RET_CONTINUE_LOW = 3,
  HP_ITER_RET_STOP = -1,
  HP_ITER_RET_FAIL = -2,
} hp_iter_ret_t;

#define HP_ITER_CONTINUE(RET) ((RET) > 0)
#define HP_ITER_STOP(RET)     ((RET) == -1)
#define HP_ITER_FAIL(RET)     ((RET) == -2)

#endif /* __HP_ITERATOR_H__ */
