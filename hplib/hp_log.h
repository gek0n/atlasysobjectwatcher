/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPLOG_H__
#define __HPLOG_H__

typedef enum
{
  HP_LOGLEVEL_TRACE = 0,
  HP_LOGLEVEL_DEBUG,
  HP_LOGLEVEL_SCRIPT,
  HP_LOGLEVEL_WARNING,
  HP_LOGLEVEL_ERROR,
  HP_LOGLEVEL_INIFIN,
  HP_LOGLEVEL_IMPORTANT,
  HP_LOGLEVEL_OFF // should go last
} hp_log_level_t;

extern hp_log_level_t gLogLevel;
extern char const *gComponentName;


logical HpLogPrintInternal(hp_log_level_t lvl, const char *func, const char *fmt, ...);
logical HpLogPrintVInternal(hp_log_level_t lvl, const char *func, const char *fmt, va_list args);


typedef logical (*hp_LoggerFunction_t)(hp_log_level_t lvl, const char *func, const char *fmt, ...);
typedef logical (*hp_LoggerFunctionV_t)(hp_log_level_t lvl, const char *func, const char *fmt, va_list args);


extern hp_LoggerFunction_t gLoggerFn;
extern hp_LoggerFunctionV_t gLoggerFnV;

#define HpLogIniFin(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_INIFIN) ? FALSE : gLoggerFn(HP_LOGLEVEL_INIFIN, __FUNCTION__, fmt, ## __VA_ARGS__)

#define HpLogImp(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_IMPORTANT) ? FALSE : gLoggerFn(HP_LOGLEVEL_IMPORTANT, __FUNCTION__, fmt, ## __VA_ARGS__)

#define HpLogTrace(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_TRACE) ? FALSE : gLoggerFn(HP_LOGLEVEL_TRACE, __FUNCTION__, fmt, ## __VA_ARGS__)

#define HpLogDbg(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_DEBUG) ? FALSE : gLoggerFn(HP_LOGLEVEL_DEBUG, __FUNCTION__, fmt, ## __VA_ARGS__)

#define HpLogWrn(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_WARNING) ? FALSE : gLoggerFn(HP_LOGLEVEL_WARNING, __FUNCTION__, fmt, ## __VA_ARGS__)

#define HpLogErr(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_ERROR) ? FALSE : gLoggerFn(HP_LOGLEVEL_ERROR, __FUNCTION__, fmt, ## __VA_ARGS__)

#define HpLogScript(fmt, ...) \
  (gLogLevel > HP_LOGLEVEL_SCRIPT) ? FALSE : gLoggerFn(HP_LOGLEVEL_SCRIPT, __FUNCTION__, fmt, ## __VA_ARGS__)

#endif /* __HPLOG_H__ */
