/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#pragma once

/* cryptographic random */

int32 hp_CryptoRand(void* buffer, uint32 size);

int32 hp_CryptoIni(void);
void hp_CryptoFin(void);

