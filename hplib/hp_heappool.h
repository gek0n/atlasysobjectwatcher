/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __CTHEAPPOOL_H__
#define __CTHEAPPOOL_H__


#define MAX_CPUS 8
#define GET_CPUID() (hp_ProcessorCount() % MAX_CPUS)

#define MAX_MANAGED_SIZE 128*1024

#define MAX_HEAP_POOLS 2048*MAX_CPUS
#define MAX_HEAP_POOLS_DIR_ENTRIES 256

#define NULL_SCALAR_UP (0)
#define NO_POOL_INDEX ((uint16)(-1))

#define SIZE_BITS 3
#define MIN_SIZE_BITS 3
#define SIZE_BITS_MASK ((1 << SIZE_BITS) - 1)

#if HP_TWEAK_MALLOC_TRACKING_ENABLE
#define TAG_ALLOCATED 0xbabaeba0
#define TAG_ALLOCATED_HP 0xbabaeba3
#define TAG_ALLOCATED_SEEN 0xbabaeba1
#define TAG_ALLOCATED_REPORTED 0xbabaeba2
#define TAG_FREED 0xdeadbeef
#define TAG_FREED_DEBUG 0xdeadbeed

 /* hp_heappool_debug_block_t must be aligned on 1 byte boundary */
#pragma pack(push, 1)
typedef struct {
  int32       magic;
  uint16      line;
  const char *file;
} hp_heappool_debug_block_t, *hp_heappool_debug_block_ptr_t;
#pragma pack(pop)
#endif // HP_TWEAK_MALLOC_TRACKING_ENABLE

/* OPTIMIZE_SURPLUS_ALLOCATIONS allows the memory manager and hash table
 * implementation to use the extra memory allocated due to rounding.
 * It has been demonstrated to save memory most of the time, though
 * there have been cases where things got worse, particularly when heavy
 * logging was enabled.
 */
#define OPTIMIZE_SURPLUS_ALLOCATIONS TRUE

typedef hp_atomic32_t hp_heappool_up_t;

typedef struct {
  union {
    struct {
      uint16 index;
      uint16 update_count;
    } up;
    hp_heappool_up_t scalar_up;
  } u;
} volatile hp_up_t;

typedef union {
  hp_up_t up;
  hp_up_t next;
} hp_heappool_free_elem_t;

typedef enum {
  POOL_NATIVE_MANAGED_KIND,
  POOL_NATIVE_UNMANAGED_KIND,
  POOL_HEAP_KIND
} PoolKindT;

typedef enum {
  ag_align_none,
  ag_align_32,
  ag_align_64
} hp_heappool_align_t;

typedef struct hp_heappool {
  hp_up_t     head;
  hp_up_t     this_pool;
  hp_up_t     next_pool;
  void     *base_address;
  unsigned int elem_size;
  unsigned short num_elems;
  unsigned short size_index;
} hp_heappool_t, *hp_heappool_ptr_t;

typedef struct {
  int destroyed;
  hp_up_t heap_pools_dir[MAX_CPUS][MAX_HEAP_POOLS_DIR_ENTRIES];
  volatile hp_heappool_ptr_t heap_pools_table[MAX_HEAP_POOLS];
  hp_atomic32_t pool_count;
  hp_atomic32_t cs_miss_count;
  hp_atomic64_t bytes_alloc;
  hp_atomic64_t gross_bytes_alloc;
  hp_atomic64_t gross_bytes_freed;
  hp_atomic32_t num_alloc;
  hp_atomic32_t num_frees;
  hp_atomic32_t pc_in_malloc;
  hp_atomic32_t pc_in_free;

  hp_atomic64_t native_bytes_alloc;
  hp_atomic32_t num_native_alloc;
  hp_atomic32_t num_native_free;

  hp_atomic32_t last_pool_count;
  hp_atomic32_t last_cs_miss_count;
  hp_atomic64_t last_bytes_alloc;
  hp_atomic64_t last_gross_bytes_alloc;
  hp_atomic64_t last_gross_bytes_freed;
  hp_atomic64_t last_native_bytes_alloc;
  hp_atomic32_t last_num_native_alloc;
  hp_atomic32_t last_num_native_free;
  hp_atomic32_t last_num_alloc;
  hp_atomic32_t last_num_frees;
  hp_atomic32_t last_pc_in_malloc;
  hp_atomic32_t last_pc_in_free;

  hp_atomic32_t fake_fail_ratio;
} hp_heap_data_t;

extern hp_heap_data_t *gHeapData;

#define NULL_UP 0

int hp_heappool_up_is_locked(hp_heappool_up_t up);
hp_heappool_up_t hp_heappool_up_lock(hp_heappool_up_t up);
hp_heappool_up_t hp_heappool_up_unlock(hp_heappool_up_t up);
int hp_heappool_up_valid(hp_heappool_ptr_t hp, hp_heappool_up_t up);

int hp_heappool_up_mem_lock(hp_heappool_ptr_t hp, hp_heappool_up_t up);
int hp_heappool_up_mem_unlock(hp_heappool_ptr_t hp, hp_heappool_up_t up);

int hp_heappool_is_null_up(hp_heappool_up_t up);
hp_heappool_up_t hp_heappool_next_null_up(void);

#if HP_TWEAK_MALLOC_TRACKING_ENABLE
  #define next_null_up() hp_heappool_next_null_up()
  #define is_null_up(X) hp_heappool_is_null_up(X)
#else
  #define next_null_up() NULL_UP
  #define is_null_up(X) ((X) == NULL_UP)
#endif

size_t hp_heappool_get_header_size(size_t align_size);
int hp_heappool_size_to_index(size_t elem_size);
size_t hp_heappool_index_to_size(int index);
size_t hp_heappool_l_get_size(void *p);
void *hp_heappool_up_to_ptr(hp_heappool_ptr_t hp, hp_heappool_up_t up);

hp_heappool_up_t hp_heappool_hp_alloc_up(hp_heappool_ptr_t hp);

void *hp_heappool_alloc(hp_heappool_ptr_t hp);
void hp_heappool_free(hp_heappool_ptr_t hp, void *p);

hp_heappool_ptr_t hp_createHeapPool(
  size_t     req_num_elems,
  size_t     elem_size,
  hp_heappool_align_t align,
  PoolKindT  pool_kind);

void hp_deleteHeapPool(hp_heappool_ptr_t hp);

void hp_LogHeapPoolStat(void);

int32 HeapPoolIni(void);
void HeapPoolFin(void);


#endif /* __CTHEAPPOOL_H__ */
