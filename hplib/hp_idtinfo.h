/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPIDTINFO_H__
#define __HPIDTINFO_H__

typedef struct {
  unsigned OffsetLo : 16;   //bits 15....0
  unsigned Selector : 16;   //segment selector
  unsigned Reserved : 13;   //bits we don't need
  unsigned Dpl : 2;         //descriptor privilege level
  unsigned Present : 1;     //segment present flag
  unsigned OffsetHi : 16;   //bits 31...16
#if defined(HP_64)
  unsigned OffsetHiHi : 32; // bits 63 .... 32
  unsigned reserved1 : 32;  // space put in by processor spec 
#endif
} idt_gate_t;

idt_gate_t *hp_GetIdtGate(uint32 idtNumber);
void *hp_GetHandlerAddr(uint32 idtNumber);

int32 hp_IdtInfoIni(void);
void hp_IdtInfoFin(void);

#endif /* __HPIDTINFO_H__ */
