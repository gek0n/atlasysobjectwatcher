/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_BITMAP_H__
#define __HP_BITMAP_H__

typedef struct hp_bitmap {
  uint32 *buffer;
  uint32 size;
} hp_bitmap_t;

/**
* @brief Initialise a new bitmap.
*
* PARAMS
* @param bits Bitmap pointer
* @param buf Memory for the bitmap
* @param size Size of buf in bits
*
* buf must be aligned on a uint32 suitable boundary, to a multiple of 32 unsigned chars
* in size (irrespective of size given).
*/
void hp_BitmapCreate(hp_bitmap_t *bits, void *buf, uint32 size);

void hp_BitmapSnapshot(hp_bitmap_t *dst, hp_bitmap_t *src);

/**
* @brief Set all bits in a bitmap.
*
* @param bits Bitmap pointer
*/
void hp_BitmapSetAll(hp_bitmap_t *bits);

/**
* @brief Clear all bits in a bitmap.
*
* @param bits Bitmap pointer
*/
void hp_BitmapClearAll(hp_bitmap_t *bits);

/**
* @brief Set a range of bits in a bitmap.
*
* @param bits  Bitmap pointer
* @param start First bit to set
* @param count Number of consecutive bits to set
*/
void hp_BitmapSet(hp_bitmap_t *bits, uint32 start, uint32 count);

/**
* @brief Clear bits in a bitmap.
*
* @param bits  Bitmap pointer
* @param start First bit to clear
* @param count Number of consecutive bits to clear
*/
void hp_BitmapClear(hp_bitmap_t *bits, uint32 start, uint32 count);

/**
* @brief Determine if part of a bitmap is set.
*
* @param bits  Bitmap pointer
* @param start First bit to check from
* @param count Number of consecutive bits to check
*
* @retval 1, If count bits from start are set.
* @retval 0, Otherwise.
*/
logical hp_BitmapAreBitsSet(hp_bitmap_t *bits, uint32 start, uint32 count);

/**
* @brief Determine if part of a bitmap is clear.
*
* @param bits  Bitmap pointer
* @param start First bit to check from
* @param count Number of consecutive bits to check
*
* @retval TRUE, If count bits from start are clear.
* @retval FALSE, Otherwise.
*/
logical hp_BitmapAreBitsClear(hp_bitmap_t *bits, uint32 start, uint32 count);

/**
* @brief Find a block of set bits in a bitmap.
*
* @param bits  Bitmap pointer
* @param count Number of consecutive set bits to find
* @param hint  Suggested starting position for set bits
*
* @return The bit at which the match was found, or HPE_NOTFOUND if no match was found.
*/
int32 hp_BitmapFindSetBits(hp_bitmap_t *bits, uint32 count, uint32 hint);

/**
* @brief Find a block of clear bits in a bitmap.
*
* @param bits  Bitmap pointer
* @param count Number of consecutive clear bits to find
* @param hint  Suggested starting position for clear bits
*
* @return The bit at which the match was found, or HPE_NOTFOUND if no match was found.
*/
int32 hp_BitmapFindClearBits(hp_bitmap_t *bits, uint32 count, uint32 hint);

/**
* @brief Find a block of set bits in a bitmap, and clear them if found.
*
* @param bits  Bitmap pointer
* @param count Number of consecutive set bits to find
* @param hint  Suggested starting position for set bits
*
* @return The bit at which the match was found, or HPE_NOTFOUND if no match was found.
*/
int32 hp_BitmapFindSetBitsAndClear(hp_bitmap_t *bits, uint32 count, uint32 hint);

/**
* @brief Find a block of clear bits in a bitmap, and set them if found.
*
* @param bits  Bitmap pointer
* @param count Number of consecutive clear bits to find
* @param hint  Suggested starting position for clear bits
*
* @return The bit at which the match was found, or HPE_NOTFOUND if no match was found.
*/
int32 hp_BitmapFindClearBitsAndSet(hp_bitmap_t *bits, uint32 count, uint32 hint);

/**
* @brief Find the number of set bits in a bitmap.
*
* @param bits Bitmap pointer
*
* @return The number of set bits.
*/
uint32 hp_BitmapNumberOfSetBits(hp_bitmap_t *bits);

/**
* @brief Find the number of clear bits in a bitmap.
*
* @param bits Bitmap pointer
*
* @return The number of clear bits.
*/
uint32 hp_BitmapNumberOfClearBits(hp_bitmap_t *bits);


#endif /* __HP_BITMAP_H__ */
