/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPSTROPS_H__
#define __HPSTROPS_H__

char* hp_StrDup(const char *str);
void hp_StrFree(char *str);
char const* hp_StrRChr(const char *str, uint8 ch);
char* hp_StrCat(char *s1, const char *s2, size_t n);

size_t hp_StrSpn(const char *s1, const char *s2);
char *hp_StrpBrk(const char *s1, const char *s2);
char* hp_StrStr(const char *in, const char *str);

logical hp_StrStartsWith(const char *str, const char *end);
logical hp_StrEndsWith(const char *str, const char *end);

/* handy string formatting functions that allocate buffers as needed */
char* hp_SprintfEx(const char *fmt, ...);
char* hp_SprintfVEx(const char *fmt, va_list args);

#if defined(HP_WIN)
  #define hp_StrCpy(dst, src, size) strcpy_s(dst, size, src)
#else
  #define hp_StrCpy(dst, src, size) strlcpy(dst, src, size)
#endif

int64 hp_StrToQ(const char *nptr, char **endptr, uint32 base);

#endif /* __HPSTROPS_H__ */
