/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_DRVINSTALL_H__
#define __HP_DRVINSTALL_H__

enum {
  HPLIB_DRVARG_VENDOR = 0,
  HPLIB_DRVARG_TEMPDIR,
  HPLIB_DRVARG_MAX
};

int32 hp_DrvInstall(const char *drvPath, const char *drvName);
int32 hp_DrvRun(const char *drvName, char **args);
void hp_DrvStop(const char *drvName);
void hp_DrvUninstall(const char *drvName);

#endif // __HP_DRVINSTALL_H__
