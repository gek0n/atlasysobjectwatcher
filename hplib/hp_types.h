/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPTYPES_H__
#define __HPTYPES_H__

/************************************************************************/
/* Basic integers type                                                  */
/************************************************************************/
#ifndef _UINT8
#define _UINT8
typedef unsigned char       uint8;
#endif

#ifndef _INT8
#define _INT8
typedef signed char         int8;
#endif

#ifndef _UINT16
#define _UINT16
typedef unsigned short      uint16;
#endif

#ifndef _INT16
#define _INT16
typedef signed short        int16;
#endif

#ifndef _UINT32
#define _UINT32
typedef unsigned int        uint32;
#endif

#ifndef _INT32
#define _INT32
typedef signed int          int32;
#endif

#ifndef _UINT64
#define _UINT64
typedef unsigned long long  uint64;
#endif

#ifndef _INT64
#define _INT64
typedef signed long long    int64;
#endif

#ifndef _INT128
#define _INT128
typedef struct { int64 low; int64 high; } int128;
#endif

/************************************************************************/
/* BOOL type                                                            */
/************************************************************************/
typedef int                 logical;

/************************************************************************/
/* Pointer size signed integer type                                     */
/************************************************************************/
#ifndef _PTRT
  #if defined(HP_WIN)
    #define _PTRT
    typedef LONG_PTR       ptr_t;
    typedef ULONG_PTR      uptr_t;
  #elif defined(HP_LINUX) || defined (HP_OSX)
    #define _PTRT
    typedef long           ptr_t;
    typedef unsigned long  uptr_t;
  #endif
#endif

typedef void* hp_handle_t;

typedef int32 (hp_func_t)(void);

typedef int32 (hp_ini_func_t)(void);
typedef void (hp_fin_func_t)(void);

/************************************************************************/
/* Basic types limits                                                   */
/************************************************************************/
#ifndef INT8_MAX
#define INT8_MAX   0x7f
#endif

#ifndef INT8_MIN
#define INT8_MIN   (-0x7f - 1)
#endif

#ifndef UINT8_MAX
#define UINT8_MAX  (0x7fU * 2U + 1U)
#endif

#ifndef INT16_MAX
#define INT16_MAX  0x7fff
#endif

#ifndef INT16_MIN
#define INT16_MIN  (-0x7fff - 1)
#endif

#ifndef UINT16_MAX
#define UINT16_MAX (0x7fffU * 2U + 1U)
#endif

#ifndef INT32_MAX
#define INT32_MAX  0x7fffffffL
#endif

#ifndef INT32_MIN
#define INT32_MIN  (-0x7fffffffL - 1L)
#endif

#ifndef UINT32_MAX
#define UINT32_MAX (0x7fffffffUL * 2UL + 1UL)
#endif

#ifndef INT64_MAX
#define INT64_MAX  0x7fffffffffffffffLL
#endif

#ifndef INT64_MIN
#define INT64_MIN  (-0x7fffffffffffffffLL - 1LL)
#endif

#ifndef UINT64_MAX
#define UINT64_MAX (0x7fffffffffffffffULL * 2ULL + 1ULL)
#endif

/************************************************************************/
/* Some constants                                                       */
/************************************************************************/
#ifndef NULL
#define NULL ((void*)0)
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#endif /* __HPTYPES_H__ */
