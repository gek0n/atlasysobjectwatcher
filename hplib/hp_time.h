/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_TIME_H__
#define __OS_TIME_H__


typedef uint64 hp_time_t;

#define HP_TIME_SPAN_MILLISECOND 1  // i.e., hp_time_t is in msecs
#define HP_TIME_SPAN_SECOND 1000
#define HP_TIME_SPAN_MINUTE 60000
#define HP_TIME_SPAN_HOUR 3600000
#define HP_TIME_SPAN_DAY 86400000


#define HP_TICKS_PER_MSEC 10000
#define HP_EPOCH_BIAS   116444736000000000LL


hp_time_t HP_INLINE hp_WindowsTimeToUnixTime(uint64 t)
{
  return (t - HP_EPOCH_BIAS) / HP_TICKS_PER_MSEC;
}

uint64 HP_INLINE hp_UnixTimeToWindowsTime(hp_time_t t)
{
  return t * HP_TICKS_PER_MSEC + HP_EPOCH_BIAS;
}


hp_time_t hp_GetSystemTime(void);
hp_time_t hp_GetLocalTime(void);

void hp_ConvertTimeToTimeFields(
  hp_time_t t,
  uint32 *year,
  uint32 *month,
  uint32 *day,
  uint32 *h,
  uint32 *m,
  uint32 *s,
  uint32 *ms);

uint64 hp_GetHighResTs(void);

#endif // __OS_TIME_H__
