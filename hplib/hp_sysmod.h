/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_SYSMOD_H__
#define __HP_SYSMOD_H__

#if defined(HP_WIN)
  #define HP_NT_NAME "ntoskrnl"
#endif

void* hp_LoadModule(const char *name);
void  hp_UnloadModule(void *handle);
int32 hp_LdrCallEntryPoint(void *handle);
void* hp_GetModuleHandle(const char *name);
void* hp_GetModuleHandleFromAddr(void *addr, size_t *size);
void* hp_GetSymbolFromModuleHandle(void *handle, const char *symName);
void* hp_GetSymbolFromModuleName(const char *modName, const char *symName);

#if defined(HP_UM)
void* hp_MapModule(const char *name);
void hp_UnmapModule(void *handle);
void* hp_GetKernelModuleHandle(const char *name, logical *newCreated);
#endif

int32 hp_SysmodIni(void);
void hp_SysmodFin(void);

#endif /* __HP_SYSMOD_H__ */
