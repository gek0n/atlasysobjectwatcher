/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_THREAD_H__
#define __HP_THREAD_H__

typedef struct hp_thread hp_thread_t;

typedef void (hp_thread_func_t)(hp_thread_t *thread, void *ctx);

#if defined(HP_WIN_KM)
  #include <hplib/nt/os_thread.h>
#elif defined(HP_WIN_UM)
  #include <hplib/win32/os_thread.h>
#elif defined(HP_LINUX_KM)
  #include <hplib/lnx/os_thread.h>
#elif defined(HP_OSX_KM)
  #include <hplib/xnu/os_thread.h>
#elif defined(HP_OSX_UM)
  #include <hplib/posix/os_thread.h>
#endif

uint32 hp_GetPidByTid(uint32 tid);

#if defined(HP_KM)
void* hp_GetCurrentProcessObj(void);
logical hp_IsSystemProcess(void* process);
void* hp_GetSystemProcessObj(void);
void* hp_GetCurrentThreadObj(void);
#endif

void* hp_GetCurrentThreadObj(void);

uint32 hp_GetProcessId(void *obj); /* NULL of current */
uint32 hp_GetThreadId(void *obj);  /* NULL of current */

logical hp_IsOurThread(uint32 tid);

#if HP_TWEAK_THREAD_LOGGING_ENABLE
int32 __hp_ThreadCreate(hp_thread_t *thread, hp_thread_func_t *func, const char *name, void *ctx);
#define hp_ThreadCreate(THREAD, FUNC, ARG) \
  __hp_ThreadCreate(THREAD, FUNC, #FUNC, ARG)
#else
int32 hp_ThreadCreate(hp_thread_t *thread, hp_thread_func_t *func, void *ctx);
#endif
void hp_ThreadWaitForThreadStart(hp_thread_t *thread);
void hp_ThreadDestroy(hp_thread_t *thread);

typedef enum {
  HP_THREAD_PRIORITY_LOW = 0,
  HP_THREAD_PRIORITY_NORMAL = 1,
  HT_THREAD_PRIORITY_HIGH = 2
} hp_thread_priority_t;

int32 hp_setCurrentThreadPriority(hp_thread_priority_t p);

logical hp_IsThreadTerminating(hp_thread_t *thread);

int32 hp_ThreadsIni(void);
void hp_ThreadsFin(void);

#endif // HP_THREAD_H__
