/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */
#ifndef __HP_WORKITEM_H__
#define __HP_WORKITEM_H__

#if defined(HP_WIN_KM)
#include <hplib/nt/os_workitem.h>
#endif


#endif // __HP_WORKITEM_H__
