/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_THREAD_H__
#define __OS_THREAD_H__

typedef struct hp_thread {
  hp_thread_func_t *func;
  void             *ctx;
  hp_event_t        startEvent;
  HANDLE            thread;
  logical           terminating;
#if HP_TWEAK_THREAD_LOGGING_ENABLE
  const char       *funcName;
#endif
} hp_thread_t;

#endif // OS_THREAD_H__
