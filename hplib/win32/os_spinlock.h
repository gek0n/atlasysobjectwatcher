/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_SPINLOCK_H__
#define __OS_SPINLOCK_H__

typedef struct hp_spinlock {
  CRITICAL_SECTION lock;
} hp_spinlock_t;

#endif // __OS_SPINLOCK_H__
