/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_EVENT_H__
#define __OS_EVENT_H__

typedef struct hp_event {
  HANDLE evnt;
  hp_event_type_t type;
} hp_event_t;

#endif // __OS_EVENT_H__
