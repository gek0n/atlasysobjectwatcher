/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_MUTEX_H__
#define __HP_MUTEX_H__

#if defined(HP_WIN_KM)
  #include <hplib/nt/os_mutex.h>
#elif defined(HP_WIN_UM)
  #include <hplib/win32/os_mutex.h>
#elif defined(HP_LINUX_KM)
  #include <hplib/lnx/os_mutex.h>
#elif defined(HP_OSX_KM)
  #include <hplib/xnu/os_mutex.h>
#elif defined(HP_OSX_UM)
  #include <hplib/posix/os_mutex.h>
#endif

int32 hp_MutexCreate(hp_mutex_t *mtx);
void hp_MutexAcquire(hp_mutex_t *mtx);
void hp_MutexRelease(hp_mutex_t *mtx);
void hp_MutexDestroy(hp_mutex_t *mtx);

#endif // HP_MUTEX_H__
