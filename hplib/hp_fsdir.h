/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_FSDIR_H__
#define __HP_FSDIR_H__

typedef enum
{
  DIR_ENTRY_DIRECTORY = 0,
  DIR_ENTRY_FILE,
  DIR_ENTRY_DRIVE_REMOVABLE,
  DIR_ENTRY_DRIVE_FIXED,
  DIR_ENTRY_DRIVE_CDROM

} hp_direntry_type_t;

typedef struct
{
  hp_direntry_type_t type;
  char const* name;
  uint32 nameLen;
  char const* path;
  uint32 pathLen;
  uint64 size;
  hp_time_t atime;
  hp_time_t mtime;
  hp_time_t ctime;

} hp_direntry_info_t;

typedef int32 (hp_dir_iterator)(hp_direntry_info_t *entry, void *ctx);

int32 hp_ReadDir(const char *root, hp_dir_iterator *iterator, void *ctx);
int32 hp_ForEachReadDir(const char *root, hp_dir_iterator *iterator, void *arg);

#endif // __HP_FSDIR_H__
