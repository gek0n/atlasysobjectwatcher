/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_BYTEOPS_H__
#define __HP_BYTEOPS_H__

HP_INLINE uint32 hp_Rot32(uint32 x, int8 r)
{
  return (x << r) | (x >> (32 - r));
}

HP_INLINE uint64 hp_Rot64(uint64 x, int8 r)
{
  return (x << r) | (x >> (64 - r));
}

#endif /* __HP_BYTEOPS_H__ */
