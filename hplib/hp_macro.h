/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_MACRO_H__
#define __HP_MACRO_H__

#define HP_UNUSED_PARAM(P) (void)(P)
#define HP_NOTHING
#define HP_OBSOLETE
#define HP_FORBIDDEN
#define HP_NOTREACHED   hp_Assert(0)
#define HP_NORETURN     __declspec(noreturn)

#if !defined(STRINGIFY)
   #define STRINGIFY(x) #x
   #define TOSTRING(x) STRINGIFY(x)
#endif

#define KB 1024
#define MB (1024 * KB)
#define GB (1024 * MB)

/* TODO */
#if defined(HP_WIN_KM)
  #define hp_Assert(A)  NT_ASSERT(A)
#elif defined(HP_OSX) || defined(HP_LINUX_KM)
  #define hp_Assert(A) _Assert(A)
#else
  #define hp_Assert(A)  do { if (!(A)) hp_Debugger(); } while(0)
#endif

#ifdef HP_DEBUG
#define hp_Verify(x) hp_Assert(x)
#else
#define hp_Verify(x) HP_UNUSED_PARAM(x)
#endif

#if defined(HP_WIN)
  #define hp_Debugger() __debugbreak()
#else
  #define hp_Debugger()
#endif

#if defined(HP_WIN_KM)
  #define hp_IsAddrValid(ADDR) ((ADDR) != NULL)
#elif defined(HP_WIN_UM)
  #define hp_IsAddrValid(ADDR) ((ADDR) != NULL)
#elif defined(HP_OSX_UM)
  #define hp_IsAddrValid(ADDR) (ADDR > 0) // need explicit comparison to avoid unreachable warning
#elif defined(HP_OSX_KM)
  //#warning "TODO: hp_IsAddrValid()"
  #define hp_IsAddrValid(ADDR) (ADDR > 0)
#elif defined(HP_LINUX_KM)
  #define hp_IsAddrValid(ADDR) (TRUE)
#else
  #define hp_IsAddrValid(ADDR)
#endif

#if defined (HP_WIN_KM)
  #define hp_IsPrevModeKernel() (ExGetPreviousMode() == KernelMode)
#endif

#if defined (HP_WIN_KM)

  #define HP_PROTECTED_READ_BEGIN(address, length, alignment) \
    __try { \
      if (!hp_IsPrevModeKernel()) ProbeForRead((address), (length), (alignment));  \


  #define HP_PROTECTED_READ_HANDLE_ERROR() \
      } \
      __except(EXCEPTION_EXECUTE_HANDLER) { \


  #define HP_PROTECTED_READ_END() \
      } \


  #define HP_PROTECTED_WRITE_BEGIN(address, length, alignment) \
    __try { \
      if (!hp_IsPrevModeKernel()) ProbeForWrite((address), (length), (alignment)); \


  #define HP_P_ROTECTED_WRITE_HANDLE_ERROR() HP_PROTECTED_READ_HANDLE_ERROR()

  #define HP_PROTECTED_WRITE_END() HP_PROTECTED_READ_END()


#else

  #define HP_PROTECTED_READ_BEGIN(address, length, alignment) \
    if (hp_IsAddrValid(address)) { \


  #define HP_PROTECTED_READ_HANDLE_ERROR() \
    } \
    else { \


  #define HP_PROTECTED_READ_END() \
    } \


  #define HP_PROTECTED_WRITE_BEGIN(address, length, alignment) HP_PROTECTED_READ_BEGIN(address, length, alignment)

  #define HP_P_ROTECTED_WRITE_HANDLE_ERROR() HP_PROTECTED_READ_HANDLE_ERROR()

  #define HP_PROTECTED_WRITE_END() HP_PROTECTED_READ_END()

#endif


#if defined(HP_WIN_KM)
  #define HP_FLOATINGPOINT_START()    \
  {                                   \
    KFLOATING_SAVE  floatState = {0}; \
    KeSaveFloatingPointState(&floatState)


  #define HP_FLOATINGPOINT_END()              \
    KeRestoreFloatingPointState(&floatState); \
  }

#elif defined(HP_OSX_KM)

  #define HP_FLOATINGPOINT_START() \
  { \
    char fxsave_region[512] __attribute__((aligned(16))); \
    __asm__ __volatile__ ("fxsaveq %0" : "=m"(*fxsave_region)); \
    __asm__ __volatile__ ("fwait");

  #define HP_FLOATINGPOINT_END() \
    __asm__ __volatile__("fxrstorq %0" : : "m"(*fxsave_region)); \
  }

#else

#define HP_FLOATINGPOINT_START()
#define HP_FLOATINGPOINT_END()

#endif

#if defined(HP_WIN_KM)
  #define HP_CALL_WITH_STACK_EXPANSION(FUNC, ARG) \
    KeExpandKernelStackAndCallout((FUNC), (ARG), MAXIMUM_EXPANSION_SIZE)
#else
  #define HP_CALL_WITH_STACK_EXPANSION(FUNC, ARG) \
    (FUNC)(ARG)
#endif

#if defined(HP_WIN)
  #define HP_YIELD_EXECUTION() \
  { \
    YieldProcessor(); \
  }
#else
  #define HP_YIELD_EXECUTION()
#endif

#if defined(HP_WIN)
  #if !defined(CDECL)
    #define CDECL    __cdecl
  #endif

  #if !defined(FASTCALL)
    #define FASTCALL __fastcall
  #endif

  #if !defined(STDCALL)
    #define STDCALL  __stdcall
  #endif
#if defined(HP_32)
  #define CRTFUNC CDECL
#else
  #define CRTFUNC
#endif

#else
  #define CDECL
  #define STDCALL
  #define CRTFUNC
#endif

#define HP_COUNTOF(ARRAY) (sizeof((ARRAY)) / sizeof((ARRAY)[0]))

#if defined(HP_WIN)
  #define HP_INLINE __forceinline
#elif defined(HP_LINUX)
  #define HP_INLINE static inline __attribute__((always_inline))
#elif defined(HP_OSX)
  #define HP_INLINE inline __attribute__((always_inline))
#endif

#if defined(HP_WIN)
  #define hp_Int3() DbgBreakPoint()
#elif defined(HP_LINUX_KM)
  #define hp_Int3() BUG()
#elif defined(HP_OSX_KM)
  #define hp_Int3() asm("int3")
#elif defined(HP_OSX_UM)
  #define hp_Int3() abort()
#else
  #error hp_Int3
#endif

#if defined(HP_WIN)
  #define HP_STATIC_ASSERT(EXPR) C_ASSERT(EXPR)
#else
  #define HP_STATIC_ASSERT(EXPR)
#endif

#if defined (HP_WIN)
  #define HP_TRY __try
  #define HP_CATCH __except(EXCEPTION_EXECUTE_HANDLER)
  #define HP_ENDTRY
#elif defined (HP_OSX_UM)
  #define HP_TRY TRY
  #define HP_CATCH FINALLY
  #define HP_ENDTRY ETRY;
#else
  #define HP_TRY
  #define HP_CATCH
  #define HP_ENDTRY
#endif

#if defined(HP_KM)
#if defined(HP_WIN)
#define IS_SOFTWARE_INTERRUPT() (KeGetCurrentIrql() == APC_LEVEL || KeGetCurrentIrql() == DISPATCH_LEVEL)
#else
#error You need IS_SOFTWARE_INTERRUPT macro
#endif
#endif

#if defined(HP_WIN_KM)
#define IS_LESS_DISPATCH_LEVEL() (KeGetCurrentIrql() < DISPATCH_LEVEL)
#endif

#if defined(HP_32)
  #define HP_MEMALIGN_BOUND 8
#else
  #define HP_MEMALIGN_BOUND 16
#endif

#if defined(HP_WIN)
  #define HP_DECLSPEC_ALIGN(VAL) __declspec(align(VAL))
#else
  #define HP_DECLSPEC_ALIGN(VAL) __attribute__ ((aligned (VAL)))
#endif

#define HP_ZERO_MEM(MEM, SIZE) \
  memset((MEM), 0, (SIZE))

#define HP_ZERO_STRUCT(STRUCT) \
  HP_ZERO_MEM(HP_GET_ADDR(STRUCT), sizeof((STRUCT)))

#define HP_ALIGNED_FIELD HP_DECLSPEC_ALIGN(HP_MEMALIGN_BOUND)

#define HP_ALIGN_DOWN_BY(LENGTH, ALIGN) \
    ((uptr_t)(LENGTH) & ~(ALIGN - 1))

#define HP_ALIGN_UP_BY(LENGTH, ALIGN) \
    (HP_ALIGN_DOWN_BY(((uptr_t)(LENGTH) + ALIGN - 1), ALIGN))

#define HP_BYTESTOBITS(VAL) (VAL * 8)
#define HP_BITSTOBYTES(VAL) (VAL / 8)

#define __FILE_BASE__ (hp_FileBaseName(__FILE__) ? hp_FileBaseName(__FILE__) : __FILE__)

#define HP_SMALL_PAGE_SIZE 0x1000

#define HP_IS_PTR_SMALL_PAGE_ALIGNED(VA) \
  (((ptr_t)(VA) % HP_SMALL_PAGE_SIZE) == 0)

#define HP_SMALL_PAGE_OFFSET(VA) \
  (((ptr_t)(VA)) & 0xFFF)

#define HP_SMALL_PAGE_ALIGN(VA) \
  ((void *)((uptr_t)(VA) & ~(HP_SMALL_PAGE_SIZE - 1)))

#define HP_FIELD_SIZE(TYPE, FIELD) (sizeof(((TYPE *)0)->FIELD))

#define HP_FIELD_OFFSET(TYPE, FIELD) ((int32)(ptr_t)&(((TYPE *)0)->FIELD))

#define HP_CONTAINING_RECORD(ADDR, TYPE, FIELD) \
  ((TYPE *)((char *)(ADDR) - HP_FIELD_OFFSET(TYPE, FIELD)))

#if defined(HP_LINUX) || defined(HP_OSX)
#define HP_UNUSED __attribute__ ((unused))
#else
#define HP_UNUSED
#endif

#if defined(HP_WIN)
#define HP_CASSERT(EXPR, STR) static_assert((EXPR), STR)
#else
#define HP_CASSERT(EXPR, STR)
#endif

#if defined(HP_64)
  #define HP_PTR_TO_UINT32(PTR)  ((uint32)(uptr_t)(PTR)) 
#else
  #define HP_PTR_TO_UINT32(PTR)  ((uint32)(PTR)) 
#endif

#endif /* __HP_MACRO_H__ */
