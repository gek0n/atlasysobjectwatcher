/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_MEMOPS_H__
#define __HP_MEMOPS_H__

typedef int32 (hp_protect_mem_cb)(void *addr, size_t size, void *arg);

int32 hp_ProtectMemOp(void *dst, size_t size, hp_protect_mem_cb *cb, void *arg);

#if defined(HP_WIN)
#pragma optimize("", off)
#endif
HP_INLINE void hp_InswapMemory(void *addr, size_t size)
{
  HP_UNUSED_PARAM(size);

  // TODO: We should implement something smarter here
  uint64 val = *(uint64 *)addr;
  HP_UNUSED_PARAM(val);
}
#if defined(HP_WIN)
#pragma optimize("", on)
#endif

int32 MemCpyProtect(void *dst, void *src, size_t len);
void* hp_MemChr(const void *s, uint8 c, size_t n);

#endif /* __HP_MEMOPS_H__ */
