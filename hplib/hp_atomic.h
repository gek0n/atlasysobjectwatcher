/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_ATOMIC_H__
#define __HP_ATOMIC_H__

#if defined(HP_WIN)
typedef volatile int32  hp_atomic32_t;
typedef volatile int64  hp_atomic64_t;
typedef volatile int128 hp_atomic128_t;
typedef volatile void*  hp_atomicptr_t;
#else
typedef int32  hp_atomic32_t;
typedef int64  hp_atomic64_t;
typedef __int128 hp_atomic128_t;
typedef void*  hp_atomicptr_t;
#endif


#define hp_AtomicSet(a, newVal) *(a) = (newVal)
#define hp_AtomicGet(a) (*(a))

void hp_AtomicStore64(hp_atomic64_t* dst, int64 newValue);
void hp_AtomicStore32(hp_atomic32_t* dst, int32 newValue);

int32 hp_AtomicAdd(hp_atomic32_t *atom, int32 val);
int64 hp_AtomicAdd64(hp_atomic64_t *atom, int64 val);
int32 hp_AtomicInc(hp_atomic32_t *atom);
int64 hp_AtomicInc64(hp_atomic64_t *atom);
int32 hp_AtomicDecrement(hp_atomic32_t *atom);
int64 hp_AtomicDecrement64(hp_atomic64_t *atom);
logical hp_AtomicTestAndSet(hp_atomic32_t *atom, int32 bit);
logical hp_AtomicTestAndClear(hp_atomic32_t *atom, int32 bit);
logical hp_AtomicTestAndSetPtr(hp_atomicptr_t *ptr, int32 bit);
logical hp_AtomicTestAndClearPtr(hp_atomicptr_t *ptr, int32 bit);
logical hp_AtomicCas(hp_atomic32_t *atom, int32 *comp, int32 newVal);
logical hp_AtomicCas64(hp_atomic64_t *atom, int64 *comp, int64 newVal);
logical hp_AtomicCasPtr(hp_atomicptr_t *atom, void **comp, void *newVal);
logical hp_AtomicCasPtr2(hp_atomicptr_t *atom, void **comp, void **newVal); // compare and swap two ptrs at once

#if defined(HP_64)
logical hp_AtomicTestAndSet64(hp_atomic64_t *atom, int32 bit);
logical hp_AtomicTestAndClear64(hp_atomic64_t *atom, int32 bit);
#endif

// pointer-sized atomic integer
#if defined(HP_32)
typedef hp_atomic32_t hp_atomicx_t;

#define hp_AtomicAddX hp_AtomicAdd(atom, val)
#define hp_AtomicIncrementX(atom) hp_AtomicInc(atom)
#define hp_AtomicDecrementX(atom) hp_AtomicDecrement(atom)
#define hp_AtomicCasX(atom, comp, newVal) hp_AtomicCas(atom, comp, newVal)
#define hp_AtomicStoreX(dst, newValue) hp_AtomicStore32(dst, newValue)

#elif defined(HP_64)
typedef hp_atomic64_t hp_atomicx_t;

#define hp_AtomicAddX hp_AtomicAdd64(atom, val)
#define hp_AtomicIncrementX(atom) hp_AtomicInc64(atom)
#define hp_AtomicDecrementX(atom) hp_AtomicDecrement64(atom)
#define hp_AtomicCasX(atom, comp, newVal) hp_AtomicCas64(atom, comp, newVal)
#define hp_AtomicStoreX(dst, newValue) hp_AtomicStore64(dst, newValue)

#endif


#if defined(HP_WIN)

#if defined(HP_64)

// Guarantees that every preceding store is globally visible before any subsequent store
#define hp_MemoryBarrier()  __faststorefence()

#elif defined(HP_32)

#define hp_MemoryBarrier()  MemoryBarrier()

#endif

// Guarantees that every load instruction that precedes, in program order, the load fence instruction is globally visible
// before any load instruction that follows the fence in program order. Performs an implicit _ReadBarrier().
#define hp_lfence() _mm_lfence()

// Guarantees that every memory access that precedes, in program order, the memory fence instruction is globally visible
// before any memory instruction that follows the fence in program order. Performs an implicit _WriteBarrier().
#define hp_mfence() _mm_mfence()

// Guarantees that every preceding store is globally visible before any subsequent store. Peforms an implicit _ReadWriteBarrier().
#define hp_sfence() _mm_sfence()

#elif defined(HP_OSX)

#include <stdatomic.h>

#define hp_lfence() asm volatile ("lfence" ::: "memory")
#define hp_mfence() asm volatile ("mfence" ::: "memory")
#define hp_sfence() asm volatile ("sfence" ::: "memory")
#define hp_MemoryBarrier() hp_mfence()

#endif // HP_WIN | HP_OSX

#endif // __HP_ATOMIC_H__
