/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_COMPONENTS_H__
#define __HP_COMPONENTS_H__

typedef struct hp_component_info {
  const char    *name;
  hp_ini_func_t *ini;
  hp_fin_func_t *fin;
} hp_component_info_t;

typedef struct hp_components_group {
  const char          *name;
  int32                num;
  hp_component_info_t  components[];
} hp_components_group_t;

// Components group definition macro
#define HP_COMPONENTS_GROUP_START(NAME) \
  static hp_components_group_t NAME = { \
    HP_STRINGIFY(NAME),                 \
    0,                                  \
    {

#define HP_COMPONENT(INI, FIN) \
  {HP_STRINGIFY(INI), INI, FIN}

#define HP_COMPONENTS_GROUP_END  \
  {NULL, NULL, NULL} \
  }};

// Components group initialization and finalization
#define HP_COMPONENTS_GROUP_INI(NAME) \
  __CompGroupIni(HP_GET_ADDR(NAME))

#define HP_COMPONENTS_GROUP_FIN(NAME) \
  __CompGroupFin(HP_GET_ADDR(NAME))

int32 __CompGroupIni(hp_components_group_t *group);
void __CompGroupFin(hp_components_group_t *group);

#endif // __HP_COMPONENTS_H__
