/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPDEF_H__
#define __HPDEF_H__

/*
* Defines target OS for building
* - HP_WIN      - windows
* - HP_LINUX    - linux
* - HP_OSX      - osx
* - HP_WIN_KM   - windows kernelmode
* - HP_WIN_UM   - windows usermode
* - HP_LINUX_KM - linux kernelmode
* - HP_LINUX_UM - linux usermode
* - HP_OSX_KM   - osx kernelmode
* - HP_OSX_UM   - osx usermode
*/
#if defined(_WIN32) && defined(KERNEL_MODE)
  #define HP_WIN
  #define HP_WIN_KM
  #define HP_KM
  #define OS_DIR_NAME nt
#elif defined(_WIN32) && defined(USER_MODE)
  #define HP_WIN
  #define HP_WIN_UM
  #define HP_UM
  #define OS_DIR_NAME win32
#elif defined(__linux__) && defined(__KERNEL__)
  #define HP_LINUX
  #define HP_LINUX_KM
  #define HP_KM
  #define OS_DIR_NAME lnx
#elif defined(__linux__)
  #define HP_LINUX
  #define HP_LINUX_UM
  #define HP_UM
  #define OS_DIR_NAME posix
#elif defined(__MACH__) && defined(KERNEL)
  #define HP_OSX
  #define HP_OSX_KM
  #define HP_KM
  #define OS_DIR_NAME xnu
#elif defined(__MACH__)
  #define HP_OSX
  #define HP_OSX_UM
  #define HP_UM
  #define OS_DIR_NAME posix
#else
#error Unsupported OS
#endif

/*
* Defines target bitness for building
* - HP_32 - 32 bit target
* - HP_64 - 64 bit target
*/
#if defined(HP_WIN)
  #if defined(_X86_)
    #define HP_32
  #elif defined(_AMD64_)
    #define HP_64
  #endif
#elif defined(HP_LINUX) || defined(HP_OSX)
  #if defined(__i386__)
    #define HP_32
  #elif defined(__x86_64__)
    #define HP_64
#endif

#if defined(HP_OSX) && defined(HP_32)
  #error Invalid OS X target bitness!
#endif
#endif

/*
* Defines build type
* - HP_DEBUG   - debug build
* - HP_RELEASE - release build
*/
#if defined(HP_WIN)
  #if DBG
    #define HP_DEBUG
  #else
    #define HP_RELEASE
  #endif
#elif defined(HP_LINUX)
  #if DBG
    #define HP_DEBUG
  #else
    #define HP_RELEASE
  #endif
#elif defined(HP_OSX)
  #if DEBUG
    #define HP_DEBUG
  #else
    #define HP_RELEASE
  #endif
#endif

#if defined(HP_WIN)
  #define HP_OS_FAMILY_NAME "nt"
#elif defined(HP_LINUX)
  #define HP_OS_FAMILY_NAME "linux"
#elif defined(HP_OSX)
  #define HP_OS_FAMILY_NAME "osx"
#endif


#endif /* __HPDEF_H__ */
