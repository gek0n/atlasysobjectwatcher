/* ------------------------------------------------------------------------- */
/* (c) Copyright 2008-2013 Atlas Consultants Group, Inc.                     */
/* (c) Copyright 2012-2013 CounterTack, Inc.                                 */
/* All Rights Reserved                                                       */
/* ------------------------------------------------------------------------- */

#ifndef __CTHASHTABLE_H__
#define __CTHASHTABLE_H__


typedef struct hp_hashtable hp_hashtable_t;

/*
 * Thread Safe Wait Free Hash Table Functions
 *
 * This set of functions allows for a fast key based insertion and lookup
 * of data in a thread safe, wait free fashion.
 */
typedef int (hp_HashKeyCompareT) (const void *LeftKey, const void *RightKey, void *Ctx);
typedef unsigned int (hp_HashFunctionT) (const void *Key, void *Ctx);
typedef void* (hp_HashKeyGetT) (const void *Data, void *Ctx);


typedef enum {
   /* Use smaller increments (about 10% increase) instead of doubling
    * the bucket block size when running out of space.
    */
   HASH_TABLE_SMALL_INCREMENT_FLAG = 1,

   /* Attempt to free empty hash blocks.
    * This is off by default, because of potential thread safety issues
    * that have not been resolved yet.
    */
   HASH_TABLE_GARBAGE_COLLECT_FLAG = 2,

   /* When set would not allow the table to grow beyond the initial size. */
   HASH_TABLE_FIXED_SIZE_FLAG = 4,

   /* Hash table descriptor is constant and should not be freed */
   HASH_CONSTANT_DESCRIPTOR = 8
} hp_HashTableFlagValuesT;

/*
 * hp_HashTableCreate() will return a pointer to a data structure that would
 * contain the mapping between any type of data and a key associated with it.

 * Arguments:
 * ----------
 * KeyGet() - function that returns the value of type "(void *)" that
 * uniquely identifies the item.  Providing NULL will result in the address
 * of the data being treated as the key.

 * KeyCompare() - returns TRUE if the two keys are the same.  Providing NULL,
 * will result in the keys being compared as two scalars.

 * HashFunction() - returns an integer that represents a hash value of the key.
 * For example if a key is a string, the hash may be equal to the sum of all
 * full words plus any leftover bytes.  Providing NULL will result in the
 * Key being treated as a hash value.
 *
 * Ctx - context data to be associated with the table
 */
hp_hashtable_t * hp_HashTableCreate(
   hp_HashKeyGetT KeyGet,
   hp_HashKeyCompareT KeyCompare,
   hp_HashFunctionT HashFunction,
   size_t ElemSize,
   size_t InitialSize,
   unsigned int Flags,
   void *Ctx
);

typedef struct
{
   hp_HashKeyGetT     *KeyGet;
   hp_HashKeyCompareT *KeyCompare;
   hp_HashFunctionT   *HashFunction;
   size_t              ElemSize;
   unsigned int        Flags;
} hp_HashTableDescriptorT;

typedef const hp_HashTableDescriptorT *hp_HashTableDescriptorPtrT;

hp_hashtable_t * hp_HashTableCreateEx(
   hp_HashTableDescriptorPtrT Descriptor,
   size_t InitialSize,
   void *Ctx
);

size_t hp_HashTableNumElements(hp_hashtable_t * HashPtr);


/*
 * Inserts the given pointer to the data in the hash table.
 * Parameters
 *    HashPtr - pointer to the hash table
 *    Data         - pointer to the data to insert
 * Returns
 *    Pointer to the data inserted in the table
 */
void *hp_HashTableInsert(
   hp_hashtable_t * HashPtr,
   void *Data
);

/*
 * Lookup the piece of data that matches a given key.
 * Optionally, copy the found data into the CopyBuffer ensuring
 * the integrity of data (not being freed halfway through).
 */
void *hp_HashTableLookup(
   hp_hashtable_t * HashPtr,
   const void *Key,
   void *CopyBuffer
);

/*
 * Remove a hash table entry with a given key.  Do not free the storage
 * Return a pointer to the data for the removed entry if removed
 * NULL otherwise.
 */
void *hp_HashTableRemoveByKey(
   hp_hashtable_t * HashPtr,
   const void *Key
);

/*
 * Delete a hash table entry with a given key.
 * Return 0 on success.
 */
int hp_HashTableDeleteByKey(
   hp_hashtable_t * HashPtr,
   const void *Key
);

/*
 * Free the hash table entry for a given data pointer.
 * Return 0 on success.
 */
int hp_HashTableFreeItem(
   hp_hashtable_t * HashPtr,
   void *Item
);

/*
 * Compute the hash value of a null terminated string.
 */
uint64 sd_StringCrc64(const unsigned char *s);
uint64 sd_DataCrc64(const void *s, uint64 l);


/* This is the general type for an iterator function.
 * Return NULL if continue iteration.
 * Any other value will stop iteration.
 */
typedef void* (*hp_IteratorFunctionT) (void *, void *);

/*
 * Iterate over all the hash table elements.
 * Pass a copy of the data to the callback function to assure
 * that the data is not modified while being processed by the callback.
 * Return the last value returned by the IteratorFunction if any,
 * NULL otherwise.
 */
void *hp_HashTableIterate(
   hp_hashtable_t * HashPtr,
   hp_IteratorFunctionT IteratorFunction,
   void *Ctx
);

/* Same as hp_HashTableIterate, only limit the scope to those elements that
 * match the key.
 */
void *hp_HashTableIterateByKey(
   hp_hashtable_t * HashPtr,
   hp_IteratorFunctionT IteratorFunction,
   const void *Key,
   void *Ctx
);

/*
 * Iterate over all the hash table elements in place.
 * The data passed is the actual data stored in the hash table.
 * The data can be modified (e.g. deleted, reinserted, etc.), while
 * being processed by the callback function.
 * Return 0 if finished iterating through all elements.
 * Return the value returned by IteratorFunction otherwise.
 */
void* hp_HashTableIterateNoCopy(
   hp_hashtable_t * HashPtr,
   hp_IteratorFunctionT IteratorFunction,
   void *Ctx
);

/* Same as hp_HashTableIterate, only limit the scope to those elements that
 * match the key.
 */
void* hp_HashTableIterateByKeyNoCopy(
   hp_hashtable_t * HashPtr,
   hp_IteratorFunctionT IteratorFunction,
   const void *Key,
   void *Ctx
);

/*
 * Free the data structures allocated for the hash table.
 */
void hp_HashTableFree(hp_hashtable_t * HashPtr);
unsigned int  hp_DefaultHashFunction(const void *Key, void *Ctx);
void *hp_DefaultKeyGet(const void *Data, void *Ctx);
int hp_DefaultKeyCompare(const void *LeftKey, const void *RightKey, void *Ctx);

typedef struct hp_HashLruT
{
	hp_hashtable_t * HashTablePtr;
	size_t Size;
  void* Cache[1];
} hp_HashLruT, *hp_HashLruPtrT;

hp_HashLruPtrT hp_HashLruCreate(
   hp_hashtable_t * HashTablePtr,
   size_t Size
);

void* hp_HashLruLookup(
   hp_HashLruPtrT HashLruPtr,
   const void *Key,
   void* ctx
);

void hp_HashLruInsert(
   hp_HashLruPtrT HashLruPtr,
   void* data
);

void hp_HashLruFree(hp_HashLruPtrT HashLruPtr);


#endif /* __CTHASHTABLE_H__ */
