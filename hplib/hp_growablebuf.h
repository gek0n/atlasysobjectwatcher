/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#pragma once

typedef struct hp_growable_buffer
{
  void         *base;
  size_t        size;
  size_t        limit;
  size_t        position;
} hp_growable_buffer_t;


#define HP_GROWABLE_BUFFER_UNLIMITED  ((size_t)-1)

logical hp_CreateGrowableBuffer(struct hp_growable_buffer *buf, size_t size, size_t limit);
void hp_DestroyGrowableBuffer(struct hp_growable_buffer *buf);

void *hp_GetWriteableBuffer(struct hp_growable_buffer *buf, size_t required);

HP_INLINE void *hp_GetReadableBuffer(struct hp_growable_buffer *buf, size_t* size)
{
  *size = buf->position;
  return buf->base;
}

HP_INLINE void hp_ResetGrowableBuffer(struct hp_growable_buffer *buf)
{
  buf->position = 0;
}
