/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_SECURITY_H__
#define __HP_SECURITY_H__


#else
hp_access_token_t hp_GetProcessAccessToken(void* processObj, int32 access);

#endif // __HP_SECURITY_H__
