/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_EXECTX_H__
#define __HP_EXECTX_H__

typedef struct hp_exectx {

#if defined(HP_32)
  uptr_t edi;
  uptr_t esi;
  uptr_t ebp;
  uptr_t ebx;
  uptr_t edx;
  uptr_t ecx;
  uptr_t eax;
  uptr_t flags;
  uptr_t esp;
#else
  uptr_t r15;
  uptr_t r14;
  uptr_t r13;
  uptr_t r12;
  uptr_t r11;
  uptr_t r10;
  uptr_t r9;
  uptr_t r8;
  uptr_t rdi;
  uptr_t rsi;
  uptr_t rbp;
  uptr_t rbx;
  uptr_t rdx;
  uptr_t rcx;
  uptr_t rax;
  uptr_t flags;
  uptr_t rsp;
#endif

} hp_exectx_t;

typedef enum {
  HP_CALLCONV_DEFAULT = 0, /* __fastcall 64, __stdcall 32 */
  HP_CALLCONV_CDECL,       /* __cdecl */
  HP_CALLCONV_STDCALL,     /* __stdcall */
  HP_CALLCONV_FASTCALL,    /* __fastcall */
} hp_callconv_t;

// Get the pointer to the stack value indexed by [num]
uptr_t* hp_GetStackNumberPtr(int32 num, hp_exectx_t* regs);
#define hp_GetStackNumber(NUM, EXECTX) (*(hp_GetStackNumberPtr(NUM, EXECTX)))

// Get the pointer to the parameter with number [num]
uptr_t* hp_GetParamPtr(int32 num, hp_exectx_t *regs, hp_callconv_t conv);
#define hp_GetParam(NUM, EXECTX, CONV) (*(hp_GetParamPtr(NUM, EXECTX, CONV)))

// Get the pointer to the return address
uptr_t* hp_GetReturnPtr(hp_exectx_t* regs);
#define hp_GetReturn(EXECTX) (*(hp_GetReturnPtr(EXECTX)))

#endif // __HP_EXECTX_H__
