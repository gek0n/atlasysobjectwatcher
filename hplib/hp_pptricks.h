/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_PPTRICKS_H__
#define __HP_PPTRICKS_H__

#define HP_STRINGIFY(X) #X
#define HP_TOSTRING(X) HP_STRINGIFY(X)

#define HP_MERGE1(A, B)     A##B
#define HP_MERGE(A, B)      HP_MERGE1(A, B)

#define HP_MERGE_COUNTER(A) HP_MERGE(A, __COUNTER__)
#define HP_MERGE_LINE(A)    HP_MERGE(A, __LINE__)

#define HP_INC1(X)         (X + 1)
#define HP_INC(X)          HP_INC1(X)

#if defined(HP_WIN_KM)
// On windows kernel-mode build __COUNTER__ starts from 2
#define HP_COUNTERSHIFT 2
#else
#define HP_COUNTERSHIFT 0
#endif

#define HP_GETCOUNTER (__COUNTER__ - HP_COUNTERSHIFT)

#define INC(x) HP_MERGE(INC_, x)
#define INC_0  1
#define INC_1  2
#define INC_2  3
#define INC_3  4
#define INC_4  5
#define INC_5  6
#define INC_6  7
#define INC_7  8
#define INC_8  9
#define INC_9  10
#define INC_10 11
#define INC_11 12
#define INC_12 13
#define INC_13 14
#define INC_14 15
#define INC_15 16
#define INC_16 17
#define INC_17 18
#define INC_18 19
#define INC_19 20
#define INC_20 21
#define INC_21 22
#define INC_22 23
#define INC_23 24
#define INC_24 25
#define INC_25 26
#define INC_26 27
#define INC_27 28
#define INC_28 29
#define INC_29 30
#define INC_30 31
#define INC_31 32
#define INC_32 33
#define INC_33 34
#define INC_34 35
#define INC_35 36
#define INC_36 37
#define INC_37 38
#define INC_38 39
#define INC_39 40
#define INC_40 41
#define INC_41 42
#define INC_42 43
#define INC_43 44
#define INC_44 45
#define INC_45 46
#define INC_46 47
#define INC_47 48
#define INC_48 49
#define INC_49 50
#define INC_50 51
#define INC_51 52
#define INC_52 53
#define INC_53 54
#define INC_54 55
#define INC_55 56
#define INC_56 57
#define INC_57 58
#define INC_58 59
#define INC_59 60
#define INC_60 61
#define INC_61 62
#define INC_62 63
#define INC_63 64
#define INC_64 64

#define DEC(x) HP_MERGE(DEC_, x)
#define DEC_0  0
#define DEC_1  0
#define DEC_2  1
#define DEC_3  2
#define DEC_4  3
#define DEC_5  4
#define DEC_6  5
#define DEC_7  6
#define DEC_8  7
#define DEC_9  8
#define DEC_10 9
#define DEC_11 10
#define DEC_12 11
#define DEC_13 12
#define DEC_14 13
#define DEC_15 14
#define DEC_16 15
#define DEC_17 16
#define DEC_18 17
#define DEC_19 18
#define DEC_20 19
#define DEC_21 20
#define DEC_22 21
#define DEC_23 22
#define DEC_24 23
#define DEC_25 24
#define DEC_26 25
#define DEC_27 26
#define DEC_28 27
#define DEC_29 28
#define DEC_30 29
#define DEC_31 30
#define DEC_32 31
#define DEC_33 32
#define DEC_34 33
#define DEC_35 34
#define DEC_36 35
#define DEC_37 36
#define DEC_38 37
#define DEC_39 38
#define DEC_40 39
#define DEC_41 40
#define DEC_42 41
#define DEC_43 42
#define DEC_44 43
#define DEC_45 44
#define DEC_46 45
#define DEC_47 46
#define DEC_48 47
#define DEC_49 48
#define DEC_50 49
#define DEC_51 50
#define DEC_52 51
#define DEC_53 52
#define DEC_54 53
#define DEC_55 54
#define DEC_56 55
#define DEC_57 56
#define DEC_58 57
#define DEC_59 58
#define DEC_60 59
#define DEC_61 60
#define DEC_62 61
#define DEC_63 62
#define DEC_64 63

#define HP_GET_ADDR(X) &(X)

#define __HP_GET_ADDR(_, X)   &X,

/************************************************************************/
/*                                                                      */
/************************************************************************/

#define VA_SIZE(...) INVOKE( VA_GET_SIZE VA_OB INVOKE(VA_SPEC##__VA_ARGS__()), 0, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 VA_CB )

#define VA_OB (
#define VA_CB )
#define VA_SPEC() 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
#define VA_GET_SIZE(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_,n,...) n

#define INVOKE( ... ) INVOKE_A( __VA_ARGS__ )
#define INVOKE_A( ... ) __VA_ARGS__

/************************************************************************/
/*                                                                      */
/************************************************************************/

#define VA_FOR(MACRO, DATA, ...) INVOKE( CAT(VA_FOR, VA_SIZE(__VA_ARGS__)) ( MACRO, DATA, (__VA_ARGS__) ) )

#define VA_APPLY(x) x
#define VA_FIRST(a, ...) a
#define VA_WO_FIRST(a, ...) __VA_ARGS__

#define VA_FOR0(m,d,x)
#define VA_FOR1(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )
#define VA_FOR2(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR1( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR3(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR2( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR4(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR3( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR5(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR4( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR6(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR5( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR7(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR6( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR8(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR7( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR9(m,d,x)  m( d, VA_APPLY(VA_FIRST x) )  VA_FOR8( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR10(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR9( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR11(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR10( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR12(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR11( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR13(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR12( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR14(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR13( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR15(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR14( m, d, (VA_APPLY(VA_WO_FIRST x)))
#define VA_FOR16(m,d,x) m( d, VA_APPLY(VA_FIRST x) )  VA_FOR15( m, d, (VA_APPLY(VA_WO_FIRST x)))

#define CAT(x,y) CAT_A(x, y)
#define CAT_A(x,y) x##y

#endif /* __HP_PPTRICKS_H__ */
