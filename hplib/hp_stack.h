/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_STACK_H__
#define __HP_STACK_H__



// lock-free stack based on http://liblfds.org


typedef struct hp_stack_node
{
  struct hp_stack_node *next;
} hp_stack_node_t;


typedef struct hp_stack hp_stack_t;

hp_stack_t *hp_StackCreate();
void hp_StackDestroy(hp_stack_t *stack);

void hp_StackPush(hp_stack_t *stack, hp_stack_node_t *node);
hp_stack_node_t *hp_StackPop(hp_stack_t *stack);


#endif // __HP_STACK_H__
