/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_NETUTILS_H__
#define __HP_NETUTILS_H__

#if defined(HP_WIN)
  #if defined(HP_UM)
  #if !defined _WINSOCK_DEPRECATED_NO_WARNINGS
    #define _WINSOCK_DEPRECATED_NO_WARNINGS
  #endif
    #include <winsock2.h>
    #include <ws2tcpip.h>
  #endif

  #include <ws2def.h>
  #include <ws2ipdef.h>

  #if defined(HP_KM)
    #define htons(V) RtlUshortByteSwap(V)
    #define htonl(V) RtlUlongByteSwap(V)
    #define ntohs(V) htons(V)
    #define ntohl(V) htonl(V)

    int32 inet_pton(int32 family, const char *src, void *dst);
    char* inet_ntop(int32 family, void *addr, char *dst, size_t dstSize);
  #endif

  const char* inet_ntop_ex(struct sockaddr_storage *addr, char *dst, size_t dstSize);
#endif


typedef struct {
  uint16 src;
  uint16 dst;
  uint16 len;
  uint16 checksum;
} hp_net_udp_hdr_t;

typedef struct {
  uint16 id;      // identification number
  union {
    struct {
      uint16 rd : 1;    // recursion desired
      uint16 tc : 1;    // truncated message
      uint16 aa : 1;    // authoritive answer
      uint16 opcode : 4;// purpose of message
      uint16 qr : 1;    // query/response flag
      uint16 rcode : 4; // response code
      uint16 cd : 1;    // checking disabled
      uint16 ad : 1;    // authenticated data
      uint16 z : 1;     // its z! reserved
      uint16 ra : 1;    // recursion available
    } f;

    uint16 uf;
  };
  uint16 q_count;    // number of question entries
  uint16 ans_count;  // number of answer entries
  uint16 auth_count; // number of authority entries
  uint16 add_count;  // number of resource entries
} hp_net_dns_hdr_t;

typedef struct {
  uint16 qtype;
  uint16 qclass;
} hp_net_dns_question_t;

typedef struct {
  uint16 type;
  uint16 cls;
  uint32 ttl;
  uint16 len;
} hp_net_dns_answer_t;

#endif /* __HP_NETUTILS_H__ */
