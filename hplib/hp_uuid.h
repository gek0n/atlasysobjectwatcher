/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_UUID_H__
#define __HP_UUID_H__

#define HP_UUID_STR_LEN 37
#define HP_UUID_LEN 16

void hp_CreateRandomUUID(char *uuidBuf, size_t len);
void hp_FormatUUID(char *strBuf, size_t len, const uint8 *uuid);
int32 hp_packFormattedUUID(uint8 *buf, const char *str);

char *hp_CreateUuidString();

#endif // __HP_UUID_H__
