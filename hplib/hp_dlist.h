/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_DLIST_H__
#define __HP_DLIST_H__

typedef struct hp_dlist      hp_dlist_t;
typedef struct hp_dlist_node hp_dlist_node_t;

struct hp_dlist {
  hp_dlist_node_t *next;
  hp_dlist_node_t *prev;
};

struct hp_dlist_node {
  hp_dlist_node_t *next;
  hp_dlist_node_t *prev;
};

#define HP_HEAD_TO_NODE(HEAD) ((hp_dlist_node_t *)(HEAD))

#define hp_DlistForEach(HEAD, NODE) \
  for ((NODE) = (HEAD)->next; \
       (NODE) != HP_HEAD_TO_NODE((HEAD)); \
       (NODE) = (NODE)->next)

#define hp_DlistForEachSafe(HEAD, NODE, NEXT) \
  for ((NODE) = (HEAD)->next, (NEXT) = (NODE)->next; \
       (NODE) != HP_HEAD_TO_NODE((HEAD)); \
       (NODE) = (NEXT), (NEXT) = (NODE)->next)

HP_INLINE void hp_DlistInit(hp_dlist_t *head)
{
  head->next = head->prev = HP_HEAD_TO_NODE(head);
}

HP_INLINE void hp_DlistInsertBack(hp_dlist_t *head, hp_dlist_node_t *node)
{
  node->prev = head->prev;
  node->next = HP_HEAD_TO_NODE(head);
  head->prev->next = node;
  head->prev = node;
}

HP_INLINE void hp_DlistInsertFront(hp_dlist_t *head, hp_dlist_node_t *node)
{
  node->next = head->next;
  node->prev = HP_HEAD_TO_NODE(head);
  head->next->prev = node;
  head->next = node;
}

HP_INLINE void hp_DlistRemove(hp_dlist_node_t *node)
{
  node->prev->next = node->next;
  node->next->prev = node->prev;
}

HP_INLINE logical hp_DlistIsEmpty(hp_dlist_t *head)
{
  return (head->prev == HP_HEAD_TO_NODE(head) || head->next == HP_HEAD_TO_NODE(head));
}

HP_INLINE hp_dlist_node_t* hp_DListPullFront(hp_dlist_t *head)
{
  hp_dlist_node_t* node;

  if (hp_DlistIsEmpty(head)) {
    return NULL;
  }

  node = head->next;
  hp_DlistRemove(node);
  return node;
}

HP_INLINE hp_dlist_node_t* hp_DListPullBack(hp_dlist_t *head)
{
  hp_dlist_node_t* node;

  if (hp_DlistIsEmpty(head)) {
    return NULL;
  }

  node = head->prev;
  hp_DlistRemove(node);
  return node;
}

HP_INLINE void hp_DlistDeinit(hp_dlist_t *head)
{
  HP_UNUSED_PARAM(head);

  HP_NOTHING;
}

#endif /* __HP_DLIST_H__ */
