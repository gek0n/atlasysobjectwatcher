/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_MATH_H__
#define __HP_MATH_H__

int32 hp_log2(uint32 x);
int64 hp_pow(int32 x, int32 y);

#define hp_Abs(X) ((X)<0?(-(X)):(X))

#endif
