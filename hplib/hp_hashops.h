/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_HASHOPS_H__
#define __HP_HASHOPS_H__

uint32 hp_MurmurHash3(const void *key, int32 len, uint32 seed);

#endif /* __HP_HASHOPS_H__ */
