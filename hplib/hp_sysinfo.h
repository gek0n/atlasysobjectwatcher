/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_SYSINFO_H__
#define __HP_SYSINFO_H__

uint32 hp_ProcessorCount(void);
char*  hp_ComputerNameGet(void);
uint64 hp_PhysicalMemoryGet(void);
char*  hp_GetOSRelease(void);
char*  hp_GetKernelVersion(void);
char*  hp_GetCPUID(void);
char*  hp_GetOSVendor(void);

typedef struct {
  uint32 versionMajor;
  uint32 versionMinor;
  uint32 build;
  uint32 sp;
} hp_os_version_t;

void hp_GetOSVersion(hp_os_version_t *ver);

int32 hp_SysinfoIni(void);
void  hp_SysinfoFin(void);

#endif
