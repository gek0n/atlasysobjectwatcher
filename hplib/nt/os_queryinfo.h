/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_QUERYINFO_H__
#define __HP_QUERYINFO_H__

// "size" is optional, return size of return buffer
void* hp_QuerySysInfo(uint32 infoClass, size_t *size);
void hp_FreeSysInfo(void *info);

int32
hp_QueryThreadInfo(
  HANDLE handle,
  void *thread,
  uint32 infoClass,
  void  *buf,
  size_t size
);

int32
hp_QueryProcessInfo(
  HANDLE handle,
  void *process,
  uint32 infoClass,
  void  *buf,
  size_t size,
  uint32 *dataSize
);

HP_INLINE logical hp_IsPassiveLevel()
{
#ifdef HP_KM
	return (KeGetCurrentIrql() < DISPATCH_LEVEL);
#else
	return TRUE;
#endif
}



#endif /* __HP_QUERYINFO_H__ */
