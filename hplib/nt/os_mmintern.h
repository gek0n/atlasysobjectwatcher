/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_MMINTERN_H__
#define __OS_MMINTERN_H__

#define HP_PFN_FROM_PA(PA)  ((PA) >> PAGE_SHIFT)
#define HP_PFN_FROM_VA(VA)  HP_PFN_FROM_PA(HP_PA_FROM_VA(VA))

#define HP_PA_FROM_VA(VA)   (MmGetPhysicalAddress((VA)).QuadPart)

#endif // __OS_MMINTERN_H__
