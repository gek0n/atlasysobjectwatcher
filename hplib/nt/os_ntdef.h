/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_NTDEF_H__
#define __HP_NTDEF_H__

#if defined(HP_UM)

#define NTAPI        __stdcall
#define NTSYSAPI     DECLSPEC_IMPORT
#define NTSYSCALLAPI DECLSPEC_IMPORT

typedef LONG NTSTATUS, *PNTSTATUS;

#define NT_SUCCESS(Status)  (((NTSTATUS)(Status)) >= 0)


NTSYSAPI
ULONG
__cdecl
DbgPrintEx(
  ULONG ComponentId,
  ULONG Level,
  PCSTR Format,
  ...
);

NTSYSAPI
NTSTATUS
NTAPI
RtlGetVersion(
  PRTL_OSVERSIONINFOW lpVersionInformation
);


#define STATUS_SUCCESS                   ((NTSTATUS)0x00000000L)
#define STATUS_CANCELLED                 ((NTSTATUS)0xC0000120L)
#define STATUS_CONNECTION_REFUSED        ((NTSTATUS)0xC0000236L)
#define STATUS_IO_TIMEOUT                ((NTSTATUS)0xC00000B5L)

#define STATUS_BUFFER_TOO_SMALL          ((NTSTATUS)0xC0000023L)
#define STATUS_BUFFER_OVERFLOW           ((NTSTATUS)0x80000005L)
#define STATUS_INFO_LENGTH_MISMATCH      ((NTSTATUS)0xC0000004L)
#define STATUS_OBJECT_NAME_NOT_FOUND     ((NTSTATUS)0xC0000034L)
#define STATUS_OBJECT_PATH_NOT_FOUND     ((NTSTATUS)0xC000003AL)
#define STATUS_OBJECT_NAME_INVALID       ((NTSTATUS)0xC0000033L)
#define STATUS_OBJECT_PATH_SYNTAX_BAD    ((NTSTATUS)0xC000003BL)
#define STATUS_SHARING_VIOLATION         ((NTSTATUS)0xC0000043L)
#define STATUS_NO_MORE_ENTRIES           ((NTSTATUS)0x8000001AL)
#define STATUS_NOT_ALL_ASSIGNED          ((NTSTATUS)0x00000106L)

/*NtCreateFile Flags*/
#define FILE_DIRECTORY_FILE                     0x00000001
#define FILE_WRITE_THROUGH                      0x00000002
#define FILE_SEQUENTIAL_ONLY                    0x00000004
#define FILE_NO_INTERMEDIATE_BUFFERING          0x00000008
#define FILE_SYNCHRONOUS_IO_ALERT               0x00000010
#define FILE_SYNCHRONOUS_IO_NONALERT            0x00000020
#define FILE_NON_DIRECTORY_FILE                 0x00000040
#define FILE_CREATE_TREE_CONNECTION             0x00000080
#define FILE_COMPLETE_IF_OPLOCKED               0x00000100
#define FILE_NO_EA_KNOWLEDGE                    0x00000200
#define FILE_OPEN_FOR_RECOVERY                  0x00000400
#define FILE_RANDOM_ACCESS                      0x00000800
#define FILE_DELETE_ON_CLOSE                    0x00001000
#define FILE_OPEN_BY_FILE_ID                    0x00002000
#define FILE_OPEN_FOR_BACKUP_INTENT             0x00004000
#define FILE_NO_COMPRESSION                     0x00008000
#define FILE_RESERVE_OPFILTER                   0x00100000
#define FILE_OPEN_REPARSE_POINT                 0x00200000
#define FILE_OPEN_NO_RECALL                     0x00400000
#define FILE_OPEN_FOR_FREE_SPACE_QUERY          0x00800000

/*Definitions for Object Creation*/
#define OBJ_INHERIT                             0x00000002L
#define OBJ_PERMANENT                           0x00000010L
#define OBJ_EXCLUSIVE                           0x00000020L
#define OBJ_CASE_INSENSITIVE                    0x00000040L
#define OBJ_OPENIF                              0x00000080L
#define OBJ_OPENLINK                            0x00000100L
#define OBJ_KERNEL_HANDLE                       0x00000200L
#define OBJ_FORCE_ACCESS_CHECK                  0x00000400L
#define OBJ_VALID_ATTRIBUTES                    0x000007F2L

#define FILE_SUPERSEDE                  0x00000000
#define FILE_OPEN                       0x00000001
#define FILE_CREATE                     0x00000002
#define FILE_OPEN_IF                    0x00000003
#define FILE_OVERWRITE                  0x00000004
#define FILE_OVERWRITE_IF               0x00000005
#define FILE_MAXIMUM_DISPOSITION        0x00000005


/*File Information Classes for NtQueryInformationFile*/
typedef enum _FILE_INFORMATION_CLASS
{
  FileDirectoryInformation = 1,
  FileFullDirectoryInformation,
  FileBothDirectoryInformation,
  FileBasicInformation,
  FileStandardInformation,
  FileInternalInformation,
  FileEaInformation,
  FileAccessInformation,
  FileNameInformation,
  FileRenameInformation,
  FileLinkInformation,
  FileNamesInformation,
  FileDispositionInformation,
  FilePositionInformation,
  FileFullEaInformation,
  FileModeInformation,
  FileAlignmentInformation,
  FileAllInformation,
  FileAllocationInformation,
  FileEndOfFileInformation,
  FileAlternateNameInformation,
  FileStreamInformation,
  FilePipeInformation,
  FilePipeLocalInformation,
  FilePipeRemoteInformation,
  FileMailslotQueryInformation,
  FileMailslotSetInformation,
  FileCompressionInformation,
  FileObjectIdInformation,
  FileCompletionInformation,
  FileMoveClusterInformation,
  FileQuotaInformation,
  FileReparsePointInformation,
  FileNetworkOpenInformation,
  FileAttributeTagInformation,
  FileTrackingInformation,
  FileIdBothDirectoryInformation,
  FileIdFullDirectoryInformation,
  FileValidDataLengthInformation,
  FileShortNameInformation,
  FileIoCompletionNotificationInformation,
  FileIoStatusBlockRangeInformation,
  FileIoPriorityHintInformation,
  FileSfioReserveInformation,
  FileSfioVolumeInformation,
  FileHardLinkInformation,
  FileProcessIdsUsingFileInformation,
  FileNormalizedNameInformation,
  FileNetworkPhysicalNameInformation,
  FileIdGlobalTxDirectoryInformation,
  FileMaximumInformation
} FILE_INFORMATION_CLASS, *PFILE_INFORMATION_CLASS;

typedef enum _THREADINFOCLASS {
  ThreadBasicInformation = 0,
  ThreadTimes = 1,
  ThreadPriority = 2,
  ThreadBasePriority = 3,
  ThreadAffinityMask = 4,
  ThreadImpersonationToken = 5,
  ThreadDescriptorTableEntry = 6,
  ThreadEnableAlignmentFaultFixup = 7,
  ThreadEventPair_Reusable = 8,
  ThreadQuerySetWin32StartAddress = 9,
  ThreadZeroTlsCell = 10,
  ThreadPerformanceCount = 11,
  ThreadAmILastThread = 12,
  ThreadIdealProcessor = 13,
  ThreadPriorityBoost = 14,
  ThreadSetTlsArrayAddress = 15,   // Obsolete
  ThreadIsIoPending = 16,
  ThreadHideFromDebugger = 17,
  ThreadBreakOnTermination = 18,
  ThreadSwitchLegacyState = 19,
  ThreadIsTerminated = 20,
  ThreadLastSystemCall = 21,
  ThreadIoPriority = 22,
  ThreadCycleTime = 23,
  ThreadPagePriority = 24,
  ThreadActualBasePriority = 25,
  ThreadTebInformation = 26,
  ThreadCSwitchMon = 27,   // Obsolete
  ThreadCSwitchPmu = 28,
  ThreadWow64Context = 29,
  ThreadGroupInformation = 30,
  ThreadUmsInformation = 31,   // UMS
  ThreadCounterProfiling = 32,
  ThreadIdealProcessorEx = 33,
  ThreadCpuAccountingInformation = 34,
  ThreadSuspendCount = 35,
  ThreadActualGroupAffinity = 41,
  ThreadDynamicCodePolicyInfo = 42,
  ThreadSubsystemInformation = 45,
  MaxThreadInfoClass = 48,
} THREADINFOCLASS;

typedef struct _FILE_BASIC_INFORMATION {
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  ULONG FileAttributes;
} FILE_BASIC_INFORMATION, *PFILE_BASIC_INFORMATION;

typedef struct _FILE_RENAME_INFORMATION {
  BOOLEAN ReplaceIfExists;
  HANDLE RootDirectory;
  ULONG FileNameLength;
  WCHAR FileName[1];
} FILE_RENAME_INFORMATION, *PFILE_RENAME_INFORMATION;

typedef enum _FSINFOCLASS {
  FileFsVolumeInformation = 1,
  FileFsLabelInformation,         // 2
  FileFsSizeInformation,          // 3
  FileFsDeviceInformation,        // 4
  FileFsAttributeInformation,     // 5
  FileFsControlInformation,       // 6
  FileFsFullSizeInformation,      // 7
  FileFsObjectIdInformation,      // 8
  FileFsDriverPathInformation,    // 9
  FileFsVolumeFlagsInformation,   // 10
  FileFsSectorSizeInformation,    // 11
  FileFsDataCopyInformation,      // 12
  FileFsMetadataSizeInformation,  // 13
  FileFsMaximumInformation
} FS_INFORMATION_CLASS, *PFS_INFORMATION_CLASS;

#define DEVICE_TYPE ULONG

typedef struct _FILE_FS_DEVICE_INFORMATION {
  DEVICE_TYPE DeviceType;
  ULONG Characteristics;
} FILE_FS_DEVICE_INFORMATION, *PFILE_FS_DEVICE_INFORMATION;

#define FILE_REMOVABLE_MEDIA                        0x00000001
#define FILE_READ_ONLY_DEVICE                       0x00000002
#define FILE_FLOPPY_DISKETTE                        0x00000004
#define FILE_WRITE_ONCE_MEDIA                       0x00000008
#define FILE_REMOTE_DEVICE                          0x00000010
#define FILE_DEVICE_IS_MOUNTED                      0x00000020
#define FILE_VIRTUAL_VOLUME                         0x00000040
#define FILE_AUTOGENERATED_DEVICE_NAME              0x00000080
#define FILE_DEVICE_SECURE_OPEN                     0x00000100
#define FILE_CHARACTERISTIC_PNP_DEVICE              0x00000800
#define FILE_CHARACTERISTIC_TS_DEVICE               0x00001000
#define FILE_CHARACTERISTIC_WEBDAV_DEVICE           0x00002000
#define FILE_CHARACTERISTIC_CSV                     0x00010000
#define FILE_DEVICE_ALLOW_APPCONTAINER_TRAVERSAL    0x00020000
#define FILE_PORTABLE_DEVICE                        0x00040000

typedef struct _STRING {
  USHORT Length;
  USHORT MaximumLength;
  PCHAR Buffer;
} ANSI_STRING;

typedef ANSI_STRING *PANSI_STRING;
typedef ANSI_STRING CONST *PCANSI_STRING;

typedef struct _UNICODE_STRING {
  USHORT Length;
  USHORT MaximumLength;
  PWCH   Buffer;
} UNICODE_STRING;
typedef UNICODE_STRING *PUNICODE_STRING;
typedef UNICODE_STRING CONST *PCUNICODE_STRING;

NTSYSAPI
NTSTATUS
NTAPI
RtlUnicodeStringToAnsiString(
  PANSI_STRING     DestinationString,
  PCUNICODE_STRING SourceString,
  BOOLEAN          AllocateDestinationString
);

NTSYSAPI
VOID
NTAPI
RtlFreeAnsiString(
  PANSI_STRING AnsiString
);

char _RTL_CONSTANT_STRING_type_check(const void *s);
#define _RTL_CONSTANT_STRING_remove_const_macro(s) (s)
#define RTL_CONSTANT_STRING(s) \
{ \
    sizeof( s ) - sizeof( (s)[0] ), \
    sizeof( s ) / sizeof(_RTL_CONSTANT_STRING_type_check(s)), \
    _RTL_CONSTANT_STRING_remove_const_macro(s) \
}

typedef struct _OBJECT_ATTRIBUTES {
  ULONG Length;
  HANDLE RootDirectory;
  PUNICODE_STRING ObjectName;
  ULONG Attributes;
  PVOID SecurityDescriptor;        // Points to type SECURITY_DESCRIPTOR
  PVOID SecurityQualityOfService;  // Points to type SECURITY_QUALITY_OF_SERVICE
} OBJECT_ATTRIBUTES;
typedef OBJECT_ATTRIBUTES *POBJECT_ATTRIBUTES;

#define RTL_CONSTANT_OBJECT_ATTRIBUTES(n, a) \
    { sizeof(OBJECT_ATTRIBUTES), NULL, RTL_CONST_CAST(PUNICODE_STRING)(n), a, NULL, NULL }

NTSYSAPI
NTSTATUS
NTAPI
RtlStringFromGUID(
  REFGUID Guid,
  PUNICODE_STRING GuidString
);

LONG NTSYSAPI
NTAPI
RtlCompareUnicodeString(
  IN PCUNICODE_STRING String1,
  IN PCUNICODE_STRING String2,
  IN BOOLEAN          CaseInSensitive
);

VOID NTSYSAPI
NTAPI
RtlInitUnicodeString(
  IN OUT  PUNICODE_STRING DestinationString,
  IN OPTIONAL PCWSTR SourceString
);

NTSYSAPI
NTSTATUS
NTAPI
RtlAppendUnicodeToString(
  PUNICODE_STRING Destination,
  PCWSTR Source
);

VOID
NTAPI
RtlFreeUnicodeString(
  PUNICODE_STRING UnicodeString
);

HP_INLINE
VOID
RtlInitEmptyUnicodeString(
  PUNICODE_STRING UnicodeString,
  PWCHAR Buffer,
  USHORT BufferSize
)
{
  UnicodeString->Length = 0;
  UnicodeString->MaximumLength = BufferSize;
  UnicodeString->Buffer = Buffer;
}

NTSYSAPI
VOID
NTAPI
RtlCopyUnicodeString(
  _Inout_ PUNICODE_STRING DestinationString,
  _In_opt_ PCUNICODE_STRING SourceString
);

#define RTL_REGISTRY_ABSOLUTE     0   /* Path is a full path */
#define RTL_REGISTRY_SERVICES     1   /* \Registry\Machine\System\CurrentControlSet\Services\ */
#define RTL_REGISTRY_CONTROL      2   /* \Registry\Machine\System\CurrentControlSet\Control\ */
#define RTL_REGISTRY_WINDOWS_NT   3   /* \Registry\Machine\Software\Microsoft\Windows NT\CurrentVersion\ */
#define RTL_REGISTRY_DEVICEMAP    4   /* \Registry\Machine\Hardware\DeviceMap\ */
#define RTL_REGISTRY_USER         5   /* \Registry\User\CurrentUser\ */
#define RTL_REGISTRY_MAXIMUM      6

NTSYSAPI
NTSTATUS
NTAPI
RtlCreateRegistryKey(
  ULONG RelativeTo,
  PWSTR Path
);

NTSYSAPI
NTSTATUS
NTAPI
RtlWriteRegistryValue(
  ULONG RelativeTo,
  PCWSTR Path,
  PCWSTR ValueName,
  ULONG ValueType,
  PVOID ValueData,
  ULONG ValueLength
);

NTSYSAPI
NTSTATUS
NTAPI
RtlDeleteRegistryValue(
  ULONG RelativeTo,
  PCWSTR Path,
  PCWSTR ValueName
);

FORCEINLINE
LUID
NTAPI_INLINE
RtlConvertLongToLuid(
  _In_ LONG Long
)
{
  LUID TempLuid;
  LARGE_INTEGER TempLi;

  TempLi.QuadPart = Long;
  TempLuid.LowPart = TempLi.u.LowPart;
  TempLuid.HighPart = TempLi.u.HighPart;
  return(TempLuid);
}

NTSYSAPI
NTSTATUS
NTAPI
ZwDeleteKey(
  HANDLE KeyHandle
);

NTSYSAPI
NTSTATUS
NTAPI
ZwLoadDriver(
  PUNICODE_STRING DriverServiceName
);

NTSYSAPI
NTSTATUS
NTAPI
ZwUnloadDriver(
  PUNICODE_STRING DriverServiceName
);

NTSYSAPI
NTSTATUS
NTAPI
ZwOpenProcessTokenEx(
  _In_ HANDLE ProcessHandle,
  _In_ ACCESS_MASK DesiredAccess,
  _In_ ULONG HandleAttributes,
  _Out_ PHANDLE TokenHandle
);

#define SE_MIN_WELL_KNOWN_PRIVILEGE         (2L)
#define SE_CREATE_TOKEN_PRIVILEGE           (2L)
#define SE_ASSIGNPRIMARYTOKEN_PRIVILEGE     (3L)
#define SE_LOCK_MEMORY_PRIVILEGE            (4L)
#define SE_INCREASE_QUOTA_PRIVILEGE         (5L)


#define SE_MACHINE_ACCOUNT_PRIVILEGE        (6L)
#define SE_TCB_PRIVILEGE                    (7L)
#define SE_SECURITY_PRIVILEGE               (8L)
#define SE_TAKE_OWNERSHIP_PRIVILEGE         (9L)
#define SE_LOAD_DRIVER_PRIVILEGE            (10L)
#define SE_SYSTEM_PROFILE_PRIVILEGE         (11L)
#define SE_SYSTEMTIME_PRIVILEGE             (12L)
#define SE_PROF_SINGLE_PROCESS_PRIVILEGE    (13L)
#define SE_INC_BASE_PRIORITY_PRIVILEGE      (14L)
#define SE_CREATE_PAGEFILE_PRIVILEGE        (15L)
#define SE_CREATE_PERMANENT_PRIVILEGE       (16L)
#define SE_BACKUP_PRIVILEGE                 (17L)
#define SE_RESTORE_PRIVILEGE                (18L)
#define SE_SHUTDOWN_PRIVILEGE               (19L)
#define SE_DEBUG_PRIVILEGE                  (20L)
#define SE_AUDIT_PRIVILEGE                  (21L)
#define SE_SYSTEM_ENVIRONMENT_PRIVILEGE     (22L)
#define SE_CHANGE_NOTIFY_PRIVILEGE          (23L)
#define SE_REMOTE_SHUTDOWN_PRIVILEGE        (24L)
#define SE_UNDOCK_PRIVILEGE                 (25L)
#define SE_SYNC_AGENT_PRIVILEGE             (26L)
#define SE_ENABLE_DELEGATION_PRIVILEGE      (27L)
#define SE_MANAGE_VOLUME_PRIVILEGE          (28L)
#define SE_IMPERSONATE_PRIVILEGE            (29L)
#define SE_CREATE_GLOBAL_PRIVILEGE          (30L)
#define SE_TRUSTED_CREDMAN_ACCESS_PRIVILEGE (31L)
#define SE_RELABEL_PRIVILEGE                (32L)
#define SE_INC_WORKING_SET_PRIVILEGE        (33L)
#define SE_TIME_ZONE_PRIVILEGE              (34L)
#define SE_CREATE_SYMBOLIC_LINK_PRIVILEGE   (35L)
#define SE_DELEGATE_SESSION_USER_IMPERSONATE_PRIVILEGE   (36L)
#define SE_MAX_WELL_KNOWN_PRIVILEGE         (SE_DELEGATE_SESSION_USER_IMPERSONATE_PRIVILEGE)


typedef struct _FILE_DIRECTORY_INFORMATION {
  ULONG NextEntryOffset;
  ULONG FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  ULONG FileAttributes;
  ULONG FileNameLength;
  WCHAR FileName[1];
} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

typedef struct _CLIENT_ID {
  HANDLE UniqueProcess;
  HANDLE UniqueThread;
} CLIENT_ID;

typedef struct _IO_STATUS_BLOCK {
  union DUMMYUNIONNAME {
    NTSTATUS Status;
    PVOID Pointer;
  } DUMMYUNION;

  ULONG_PTR Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

typedef VOID
(NTAPI *PIO_APC_ROUTINE)(
  IN PVOID ApcContext,
  IN PIO_STATUS_BLOCK IoStatusBlock,
  IN ULONG Reserved);

#define InitializeObjectAttributes(p, n, a, r, s) { \
  (p)->Length = sizeof(OBJECT_ATTRIBUTES);          \
  (p)->RootDirectory = (r);                         \
  (p)->Attributes = (a);                            \
  (p)->ObjectName = (n);                            \
  (p)->SecurityDescriptor = (s);                    \
  (p)->SecurityQualityOfService = NULL;             \
}

#pragma warning(push)
#pragma warning(disable: 4201)
typedef struct _PROCESS_DEVICEMAP_INFORMATION_EX {
  union {
    struct {
      HANDLE DirectoryHandle;
    } Set;
    struct {
      ULONG DriveMap;
      UCHAR DriveType[32];
    } Query;
  } DUMMYUNIONNAME;
  ULONG flags;
} PROCESS_DEVICEMAP_INFORMATION_EX, *PPROCESS_DEVICEMAP_INFORMATION_EX;
#pragma warning(pop)

typedef enum _PROCESSINFOCLASS {
  ProcessBasicInformation = 0,
  ProcessQuotaLimits = 1,
  ProcessIoCounters = 2,
  ProcessVmCounters = 3,
  ProcessTimes = 4,
  ProcessBasePriority = 5,
  ProcessRaisePriority = 6,
  ProcessDebugPort = 7,
  ProcessExceptionPort = 8,
  ProcessAccessToken = 9,
  ProcessLdtInformation = 10,
  ProcessLdtSize = 11,
  ProcessDefaultHardErrorMode = 12,
  ProcessIoPortHandlers = 13,   // Note: this is kernel mode only
  ProcessPooledUsageAndLimits = 14,
  ProcessWorkingSetWatch = 15,
  ProcessUserModeIOPL = 16,
  ProcessEnableAlignmentFaultFixup = 17,
  ProcessPriorityClass = 18,
  ProcessWx86Information = 19,
  ProcessHandleCount = 20,
  ProcessAffinityMask = 21,
  ProcessPriorityBoost = 22,
  ProcessDeviceMap = 23,
  ProcessSessionInformation = 24,
  ProcessForegroundInformation = 25,
  ProcessWow64Information = 26,
  ProcessImageFileName = 27,
  ProcessLUIDDeviceMapsEnabled = 28,
  ProcessBreakOnTermination = 29,
  ProcessDebugObjectHandle = 30,
  ProcessDebugFlags = 31,
  ProcessHandleTracing = 32,
  ProcessIoPriority = 33,
  ProcessExecuteFlags = 34,
  ProcessTlsInformation = 35,
  ProcessCookie = 36,
  ProcessImageInformation = 37,
  ProcessCycleTime = 38,
  ProcessPagePriority = 39,
  ProcessInstrumentationCallback = 40,
  ProcessThreadStackAllocation = 41,
  ProcessWorkingSetWatchEx = 42,
  ProcessImageFileNameWin32 = 43,
  ProcessImageFileMapping = 44,
  ProcessAffinityUpdateMode = 45,
  ProcessMemoryAllocationMode = 46,
  ProcessGroupInformation = 47,
  ProcessTokenVirtualizationEnabled = 48,
  ProcessOwnerInformation = 49,
  ProcessWindowInformation = 50,
  ProcessHandleInformation = 51,
  ProcessMitigationPolicy = 52,
  ProcessDynamicFunctionTableInformation = 53,
  ProcessHandleCheckingMode = 54,
  ProcessKeepAliveCount = 55,
  ProcessRevokeFileHandles = 56,
  ProcessWorkingSetControl = 57,
  ProcessHandleTable = 58,
  ProcessCheckStackExtentsMode = 59,
  ProcessCommandLineInformation = 60,
  ProcessProtectionInformation = 61,
  ProcessMemoryExhaustion = 62,
  ProcessFaultInformation = 63,
  ProcessTelemetryIdInformation = 64,
  ProcessCommitReleaseInformation = 65,
  ProcessReserved1Information = 66,
  ProcessReserved2Information = 67,
  ProcessSubsystemProcess = 68,
  ProcessInPrivate = 70,
  ProcessRaiseUMExceptionOnInvalidHandleClose = 71,
  MaxProcessInfoClass                             // MaxProcessInfoClass should always be the last enum
} PROCESSINFOCLASS;

#define NtCurrentProcess() ( (HANDLE) (LONG_PTR) -1)
#define ZwCurrentProcess() NtCurrentProcess()

NTSYSAPI
NTSTATUS
NTAPI
ZwOpenSymbolicLinkObject(
  PHANDLE LinkHandle,
  ACCESS_MASK DesiredAccess,
  POBJECT_ATTRIBUTES ObjectAttributes
);

NTSYSAPI
NTSTATUS
NTAPI
ZwQuerySymbolicLinkObject(
  HANDLE LinkHandle,
  PUNICODE_STRING LinkTarget,
  PULONG ReturnedLength
);

typedef struct _FILE_STANDARD_INFORMATION {
  LARGE_INTEGER AllocationSize;
  LARGE_INTEGER EndOfFile;
  ULONG NumberOfLinks;
  BOOLEAN DeletePending;
  BOOLEAN Directory;
} FILE_STANDARD_INFORMATION, *PFILE_STANDARD_INFORMATION;

NTSYSAPI
NTSTATUS
NTAPI
ZwOpenFile(
  OUT PHANDLE FileHandle,
  IN ACCESS_MASK DesiredAccess,
  IN POBJECT_ATTRIBUTES ObjectAttributes,
  OUT PIO_STATUS_BLOCK IoStatusBlock,
  IN ULONG ShareAcces,
  IN ULONG OpenOptions
);

NTSTATUS
NTAPI
ZwCreateFile(
  OUT PHANDLE FileHandle,
  IN ACCESS_MASK DesiredAccess,
  IN POBJECT_ATTRIBUTES ObjectAttributes,
  OUT PIO_STATUS_BLOCK IoStatusBlock,
  IN PLARGE_INTEGER AllocationSize OPTIONAL,
  IN ULONG FileAttributes,
  IN ULONG ShareAccess,
  IN ULONG CreateDisposition,
  IN ULONG CreateOptions,
  IN PVOID EaBuffer OPTIONAL,
  IN ULONG EaLength
);

NTSYSAPI
NTSTATUS
ZwFlushBuffersFile(
  _In_ HANDLE FileHandle,
  _Out_ PIO_STATUS_BLOCK IoStatusBlock
);

NTSTATUS
NTAPI
ZwDeviceIoControlFile(
  _In_ HANDLE FileHandle,
  _In_opt_ HANDLE Event,
  _In_opt_ PIO_APC_ROUTINE ApcRoutine,
  _In_opt_ PVOID ApcContext,
  _Out_ PIO_STATUS_BLOCK IoStatusBlock,
  _In_ ULONG IoControlCode,
  _In_reads_bytes_opt_(InputBufferLength) PVOID InputBuffer,
  _In_ ULONG InputBufferLength,
  _Out_writes_bytes_opt_(OutputBufferLength) PVOID OutputBuffer,
  _In_ ULONG OutputBufferLength
);

NTSYSAPI
NTSTATUS
NTAPI
ZwDeleteFile(
  IN POBJECT_ATTRIBUTES ObjectAttributes
);

NTSYSAPI
NTSTATUS
NTAPI
ZwQueryDirectoryFile(
  IN HANDLE FileHandle,
  IN OPTIONAL HANDLE Event,
  IN OPTIONAL PIO_APC_ROUTINE ApcRoutine,
  IN OPTIONAL PVOID ApcContext,
  OUT PIO_STATUS_BLOCK IoStatusBlock,
  OUT PVOID FileInformation,
  IN ULONG Length,
  IN FILE_INFORMATION_CLASS FileInformationClass,
  IN BOOLEAN ReturnSingleEntry,
  IN OPTIONAL PUNICODE_STRING FileName,
  IN BOOLEAN RestartScan
);

NTSYSAPI
NTSTATUS
NTAPI
ZwQueryVolumeInformationFile(
  HANDLE FileHandle,
  PIO_STATUS_BLOCK IoStatusBlock,
  PVOID FsInformation,
  ULONG Length,
  FS_INFORMATION_CLASS FsInformationClass
);


typedef enum _KEY_VALUE_INFORMATION_CLASS {
  KeyValueBasicInformation,
  KeyValueFullInformation,
  KeyValuePartialInformation,
  KeyValueFullInformationAlign64,
  KeyValuePartialInformationAlign64,
  MaxKeyValueInfoClass
} KEY_VALUE_INFORMATION_CLASS;

typedef enum _KEY_INFORMATION_CLASS {
  KeyBasicInformation,
  KeyNodeInformation,
  KeyFullInformation,
  KeyNameInformation,
  KeyCachedInformation,
  KeyFlagsInformation,
  KeyVirtualizationInformation,
  KeyHandleTagsInformation,
  KeyTrustInformation,
  MaxKeyInfoClass
} KEY_INFORMATION_CLASS;

typedef struct _KEY_NODE_INFORMATION {
  LARGE_INTEGER LastWriteTime;
  ULONG   TitleIndex;
  ULONG   ClassOffset;
  ULONG   ClassLength;
  ULONG   NameLength;
  WCHAR   Name[1];
} KEY_NODE_INFORMATION, *PKEY_NODE_INFORMATION;

typedef struct _KEY_VALUE_FULL_INFORMATION {
  ULONG   TitleIndex;
  ULONG   Type;
  ULONG   DataOffset;
  ULONG   DataLength;
  ULONG   NameLength;
  WCHAR   Name[1];
} KEY_VALUE_FULL_INFORMATION, *PKEY_VALUE_FULL_INFORMATION;

typedef struct _KEY_VALUE_PARTIAL_INFORMATION {
  ULONG   TitleIndex;
  ULONG   Type;
  ULONG   DataLength;
  UCHAR Data[1];
} KEY_VALUE_PARTIAL_INFORMATION, *PKEY_VALUE_PARTIAL_INFORMATION;

NTSYSAPI
NTSTATUS
NTAPI
ZwOpenKey(
  PHANDLE             pKeyHandle,
  ACCESS_MASK          DesiredAccess,
  POBJECT_ATTRIBUTES   ObjectAttributes
);

NTSTATUS ZwCreateKey(
  PHANDLE            KeyHandle,
  ACCESS_MASK        DesiredAccess,
  POBJECT_ATTRIBUTES ObjectAttributes,
  ULONG              TitleIndex,
  PUNICODE_STRING    Class,
  ULONG              CreateOptions,
  PULONG             Disposition
);

NTSYSAPI
NTSTATUS
NTAPI
ZwSetValueKey(
  HANDLE               KeyHandle,
  PUNICODE_STRING      ValueName,
  ULONG                TitleIndex,
  ULONG                Type,
  PVOID                Data,
  ULONG                DataSize
);

NTSTATUS ZwQueryValueKey(
       HANDLE                      KeyHandle,
       PUNICODE_STRING             ValueName,
       KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
  _Out_opt_ PVOID                       KeyValueInformation,
       ULONG                       Length,
      PULONG                      ResultLength
);

NTSYSAPI
NTSTATUS
NTAPI
ZwEnumerateKey(
  HANDLE KeyHandle,
  ULONG Index,
  KEY_INFORMATION_CLASS KeyInformationClass,
  PVOID KeyInformation,
  ULONG Length,
  PULONG ResultLength
);

NTSYSAPI
NTSTATUS
NTAPI
ZwEnumerateValueKey(
  HANDLE KeyHandle,
  ULONG Index,
  KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
  PVOID KeyValueInformation,
  ULONG Length,
  PULONG ResultLength
);

NTSYSAPI
NTSTATUS
NTAPI
ZwClose(
  IN HANDLE Handle
);

NTSYSAPI
NTSTATUS
NTAPI
ZwQueryInformationFile(
  IN HANDLE FileHandle,
  OUT PIO_STATUS_BLOCK IoStatusBlock,
  OUT PVOID FileInformation,
  IN ULONG Length,
  IN FILE_INFORMATION_CLASS FileInformationClass
);

NTSYSAPI
NTSTATUS
NTAPI
ZwSetInformationFile(
  _In_ HANDLE FileHandle,
  _Out_ PIO_STATUS_BLOCK IoStatusBlock,
  _In_reads_bytes_(Length) PVOID FileInformation,
  _In_ ULONG Length,
  _In_ FILE_INFORMATION_CLASS FileInformationClass
);

NTSYSAPI
NTSTATUS
NTAPI
ZwReadFile(
  IN HANDLE FileHandle,
  IN HANDLE Event,
  IN PIO_APC_ROUTINE ApcRoutine,
  IN PVOID ApcContext,
  OUT PIO_STATUS_BLOCK IoStatusBlock,
  OUT PVOID Buffer,
  IN ULONG Length,
  IN PLARGE_INTEGER ByteOffset,
  IN PULONG Key
);

NTSYSAPI
NTSTATUS
NTAPI
ZwWriteFile(
  IN HANDLE FileHandle,
  IN HANDLE Event,
  IN PIO_APC_ROUTINE ApcRoutine,
  IN PVOID ApcContext,
  OUT PIO_STATUS_BLOCK IoStatusBlock,
  IN PVOID Buffer,
  IN ULONG Length,
  IN PLARGE_INTEGER ByteOffset,
  IN PULONG Key
);

BOOLEAN
NTAPI
RtlTimeToSecondsSince1970(
  PLARGE_INTEGER Time,
  PULONG ElapsedSeconds
);

NTSTATUS
NTAPI
ZwQueryInformationToken(
  HANDLE TokenHandle,
  TOKEN_INFORMATION_CLASS TokenInformationClass,
  PVOID TokenInformation,
  ULONG TokenInformationLength,
  PULONG ReturnLength
);

NTSTATUS
NTAPI
RtlConvertSidToUnicodeString(
  PUNICODE_STRING UnicodeString,
  PSID Sid,
  BOOLEAN AllocateDestinationString
);

typedef enum _SECTION_INHERIT {
  ViewShare = 1,
  ViewUnmap = 2
} SECTION_INHERIT;

NTSYSAPI
NTSTATUS
NTAPI
ZwCreateSection(
  PHANDLE SectionHandle,
  ACCESS_MASK DesiredAccess,
  POBJECT_ATTRIBUTES ObjectAttributes,
  PLARGE_INTEGER MaximumSize,
  ULONG SectionPageProtection,
  ULONG AllocationAttributes,
  HANDLE FileHandle
);

NTSYSAPI
NTSTATUS
NTAPI
ZwMapViewOfSection(
  HANDLE SectionHandle,
  HANDLE ProcessHandle,
  PVOID *BaseAddress,
  ULONG_PTR ZeroBits,
  SIZE_T CommitSize,
  PLARGE_INTEGER SectionOffset,
  PSIZE_T ViewSize,
  SECTION_INHERIT InheritDisposition,
  ULONG AllocationType,
  ULONG Win32Protect
);

NTSYSAPI
NTSTATUS
NTAPI
ZwUnmapViewOfSection(
  HANDLE ProcessHandle,
  PVOID BaseAddress
);

BOOL WINAPI ConvertStringSidToSidW(
  LPCWSTR StringSid,
  PSID    *Sid
);

NTSYSAPI
BOOLEAN
NTAPI
RtlValidSid(
  PSID Sid
);

NTSYSAPI
ULONG
NTAPI
RtlLengthSid(
  PSID Sid
);

NTSYSAPI
NTSTATUS
NTAPI
RtlCopySid(
  ULONG DestinationSidLength,
  PSID DestinationSid,
  PSID SourceSid
);

#endif

#if defined(HP_KM)

extern POBJECT_TYPE *LpcPortObjectType;
extern POBJECT_TYPE *MmSectionObjectType;

#ifdef HP_64
typedef struct _RTL_USER_PROCESS_PARAMETERS {
  UCHAR _PADDING0_[0x70];
  UNICODE_STRING CommandLine;
  UCHAR _PADDING1_[0x390];
} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;

typedef struct _PEB {
  UCHAR _PADDING0_[0x20];
  PRTL_USER_PROCESS_PARAMETERS ProcessParameters;
  UCHAR _PADDING1_[0x360];
} PEB, *PPEB;
#else
typedef struct _RTL_USER_PROCESS_PARAMETERS {
	UCHAR _PADDING0_[0x40];
	UNICODE_STRING CommandLine;
	UCHAR _PADDING1_[0x300];
} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;

typedef struct _PEB {
	UCHAR _PADDING0_[0x10];
	PRTL_USER_PROCESS_PARAMETERS ProcessParameters;
	UCHAR _PADDING1_[0x250];
} PEB, *PPEB;
#endif

typedef enum _OB_OPEN_REASON {
  ObCreateHandle,
  ObOPenHandle,
  ObDuplicateHandle,
  ObInheritHandle,
  ObMaxOpenReason
} OB_OPEN_REASON;

typedef enum _OBJ_HEADER_INFO_FLAG {
  HeaderCreatorInfoFlag = 0x1,
  HeaderNameInfoFlag = 0x2,
  HeaderHandleInfoFlag = 0x4,
  HeaderQuotaInfoFlag = 0x8,
  HeaderProcessInfoFlag = 0x10
} OBJ_HEADER_INFO_FLAG;

#define NUMBER_HASH_BUCKETS 37
#define OBJECT_LOCK_COUNT   4

typedef struct _OBJECT_CREATE_INFORMATION {
  ULONG Attributes;
  HANDLE RootDirectory;
  PVOID ParseContext;
  KPROCESSOR_MODE ProbeMode;
  ULONG PagedPoolCharge;
  ULONG NonPagedPoolCharge;
  ULONG SecurityDescriptorCharge;
  PSECURITY_DESCRIPTOR SecurityDescriptor;
  PSECURITY_QUALITY_OF_SERVICE SecurityQos;
  SECURITY_QUALITY_OF_SERVICE SecurityQualityOfService;
} OBJECT_CREATE_INFORMATION, *POBJECT_CREATE_INFORMATION;

#pragma warning(push)
#pragma warning(disable: 4201)
#pragma warning(disable: 4214)
struct _EX_PUSH_LOCK {
  union {
    struct {
      UINT64 Locked : 1;
      UINT64 Waiting : 1;
      UINT64 Waking : 1;
      UINT64 MultipleShared : 1;
      UINT64 Shared : 60;
    };
    UINT64 Value;
    PVOID Ptr;
  };
};

typedef struct _OBJECT_HEADER {
  LONG_PTR PointerCount;
  union {
    LONG_PTR HandleCount;
    PVOID NextToFree;
  };
  struct _EX_PUSH_LOCK Lock;
  UINT8 TypeIndex;
  UINT8 TraceFlags;
  UINT8 InfoMask;
  UINT8 Flags;
  UINT8 _PADDING0_[0x4];
  union {
    POBJECT_CREATE_INFORMATION ObjectCreateInfo;
    PVOID QuotaBlockCharged;
  };
  PVOID SecurityDescriptor;
  QUAD Body;
} OBJECT_HEADER, *POBJECT_HEADER;

typedef struct _OBJECT_TYPE_INITIALIZER {
  UINT16 Length;
  union {
    UINT8 ObjectTypeFlags;
    struct {
      UINT8 CaseInsensitive : 1;
      UINT8 UnnamedObjectsOnly : 1;
      UINT8 UseDefaultObject : 1;
      UINT8 SecurityRequired : 1;
      UINT8 MaintainHandleCount : 1;
      UINT8 MaintainTypeLIst : 1;
      UINT8 SupportsObjectCallbacks : 1;
    };
  };
  ULONG32 ObjectTypeCode;
  ULONG32 InvalidAttributes;
  GENERIC_MAPPING GenericMapping;
  ULONG32 ValidAccesMask;
  ULONG32 RetainAcces;
  POOL_TYPE PoolType;
  ULONG32 DefaultPagedPoolCharge;
  ULONG32 DefaultNonPagedPoolCharge;
  PVOID DumpProcedure;
  PVOID OpenProcedure;
  PVOID CloseProcedure;
  PVOID DeleteProcedure;
  PVOID ParseProcedure;
  PVOID SecurityProcedure;
  PVOID QueryNameProcedure;
  PVOID OkayToCloseProcedure;
  ULONG WaitObjectFlagMask;
  USHORT WaitObjectFlagOffset;
  USHORT WaitObjectPointerOffset;
} OBJECT_TYPE_INITIALIZER, *POBJECT_TYPE_INITIALIZER;

typedef struct _OBJECT_TYPE {
  LIST_ENTRY TypeList;
  UNICODE_STRING Name;
  PVOID DefaultObject;
  UINT8 Index;
  UINT8 _PADDING0_[0x3];
  ULONG32 TotalNumberOfObjects;
  ULONG32 TotalNumberOfHandles;
  ULONG32 HighWaterNumberOfObjects;
  ULONG32 HighWaterNumberOfHandles;
  UINT8 _PADDING1_[0x4];
  OBJECT_TYPE_INITIALIZER TypeInfo;
  struct _EX_PUSH_LOCK TypeLock;
  ULONG32 Key;
  UINT8 _PADDING2_[0x4];
  LIST_ENTRY CallbackList;
} OBJECT_TYPE, *POBJECT_TYPE;

typedef struct _OBJECT_HANDLE_COUNT_ENTRY {
  PEPROCESS Process;
  struct {
    ULONG32 HandleCount : 24;
    ULONG32 LockCount : 8;
  };
} OBJECT_HANDLE_COUNT_ENTRY, *POBJECT_HANDLE_COUNT_ENTRY;

#pragma warning(pop)

typedef struct _OBJECT_DIRECTORY_ENTRY {
  struct _OBJECT_DIRECTORY_ENTRY *ChainLink;
  PVOID Object;
  ULONG HashValue;
} OBJECT_DIRECTORY_ENTRY, *POBJECT_DIRECTORY_ENTRY;

typedef struct _OBJECT_DIRECTORY {
  POBJECT_DIRECTORY_ENTRY HashBuckets[NUMBER_HASH_BUCKETS];
  struct _EX_PUSH_LOCK Lock;
  struct _DEVICE_MAP *DeviceMap;
  ULONG32 SessionId;
  UINT8 _PADDING0_[0x4];
  void *NameSpaceEntry;
  ULONG32 Flag;
  UINT8 _PADDING1_[0x4];
} OBJECT_DIRECTORY, *POBJECT_DIRECTORY;

typedef struct _OBJECT_DIRECTORY_INFORMATION {
  UNICODE_STRING Name;
  UNICODE_STRING TypeName;
} OBJECT_DIRECTORY_INFORMATION, *POBJECT_DIRECTORY_INFORMATION;

typedef struct _DEVICE_MAP {
  POBJECT_DIRECTORY DosDevicesDirectory;
  POBJECT_DIRECTORY GlobalDosDevicesDirectory;
  ULONG ReferenceCount;
  ULONG DriveMap;
  UCHAR DriveType[32];
} DEVICE_MAP, *PDEVICE_MAP;

typedef struct _OBJECT_HANDLE_COUT_DATABASE {
  ULONG32 CountEntries;
  UINT8 _PADDING0_[0x4];
  OBJECT_HANDLE_COUNT_ENTRY HandleCountEntries[1];
} OBJECT_HANDLE_COUNT_DATABASE, *POBJECT_HANDLE_COUNT_DATABASE;

typedef struct _OBJECT_HEADER_CREATOR_INFO {
  LIST_ENTRY TypeList;
  void *CreatorUniqueProcess;
  UINT16 CreatorBackTraceIndex;
  UINT16 Reserved;
  UINT8 _PADDING0_[0x4];
} OBJECT_HEADER_CREATOR_INFO, * POBJECT_HEADER_CREATOR_INFO;

typedef struct _OBJECT_HEADER_NAME_INFO {
  POBJECT_DIRECTORY Directory;
  UNICODE_STRING Name;
  ULONG32 ReferenceCount;
  UINT8 _PADDING0_[0x4];
} OBJECT_HEADER_NAME_INFO, *POBJECT_HEADER_NAME_INFO;

typedef struct _OBJECT_HEADER_HANDLE_INFO {
  POBJECT_HANDLE_COUNT_DATABASE HandleCountDataBase;
  OBJECT_HANDLE_COUNT_ENTRY SingleEntry;
} OBJECT_HEADER_HANDLE_INFO, *POBJECT_HEADER_HANDLE_INFO;

typedef struct _OBJECT_HEADER_QUOTA_INFO {
  ULONG32 PagedPoolCharge;
  ULONG32 NonPagedPoolCharge;
  UINT8 _PADDING0_[0x4];
  void *SecurityDescriptorQuotaBlock;
  UINT64 Reserved;
} OBJECT_HEADER_QUOTA_INFO, *POBJECT_HEADER_QUOTA_INFO;

typedef struct _OBJECT_HEADER_PROCESS_INFO {
  PEPROCESS ExclusiveProcess;
  UINT64 Reserved;
} OBJECT_HEADER_PROCESS_INFO, *POBJECT_HEADER_PROCESS_INFO;

#define OBJECT_TO_OBJECT_HEADER(obj) \
  CONTAINING_RECORD(obj, OBJECT_HEADER, Body)

NTKERNELAPI
NTSTATUS
ObOpenObjectByName(
  POBJECT_ATTRIBUTES ObjectAttributes,
  POBJECT_TYPE ObjectType,
  KPROCESSOR_MODE AccessMode,
  PACCESS_STATE AccessState,
  ACCESS_MASK DesiredAccess,
  PVOID ParseContext,
  PHANDLE Handle
);

NTKERNELAPI
NTSTATUS
ObCreateObjectType(
  __in PUNICODE_STRING TypeName,
  __in POBJECT_TYPE_INITIALIZER ObjectTypeInitializer,
  __in_opt PSECURITY_DESCRIPTOR SecurityDescriptor,
  __out POBJECT_TYPE *ObjectType
);

NTKERNELAPI
NTSTATUS
ObCreateObject(
  KPROCESSOR_MODE ProbeMode,
  POBJECT_TYPE ObjectType,
  POBJECT_ATTRIBUTES ObjectAttributes,
  KPROCESSOR_MODE OwnershipMode,
  PVOID ParseContext,
  ULONG ObjectBodySize,
  ULONG PagedPoolCharge,
  ULONG NonPagedPoolCharge,
  PVOID *Object
);


PPEB
PsGetProcessPeb(
  IN PEPROCESS Process
);

NTKERNELAPI
POBJECT_TYPE
NTAPI
ObGetObjectType(
  IN PVOID Object
);

NTSYSAPI
NTSTATUS
NTAPI
ObReferenceObjectByName(
  IN PUNICODE_STRING ObjectPath,
  IN ULONG Attributes,
  IN PACCESS_STATE PassedAccessState OPTIONAL,
  IN ACCESS_MASK DesiredAccess OPTIONAL,
  IN POBJECT_TYPE ObjectType,
  IN KPROCESSOR_MODE AccessMode,
  IN OUT PVOID ParseContext OPTIONAL,
  OUT PVOID *ObjectPtr
);

NTSYSAPI
NTSTATUS
NTAPI
ZwQueryDirectoryObject(
  IN HANDLE DirectoryHandle,
  OUT PVOID Buffer,
  IN ULONG Length,
  IN BOOLEAN ReturnSingleEntry,
  IN BOOLEAN RestartScan,
  IN OUT PULONG Context,
  OUT PULONG ReturnLength OPTIONAL
);

NTKERNELAPI
VOID
KeGenericCallDpc(
  _In_ PKDEFERRED_ROUTINE Routine,
  _In_opt_ PVOID Context
);

NTKERNELAPI
VOID
KeSignalCallDpcDone(
  _In_ PVOID SystemArgument1
);

NTKERNELAPI
LOGICAL
KeSignalCallDpcSynchronize(
  _In_ PVOID SystemArgument2
);

#define UNWIND_HISTORY_TABLE_SIZE 12

typedef struct _RUNTIME_FUNCTION {
  DWORD32 BeginAddress;
  DWORD32 EndAddress;
  DWORD32 UnwindData;
} RUNTIME_FUNCTION, *PRUNTIME_FUNCTION;

#define UNW_FLAG_NHANDLER 0x0
#define UNW_FLAG_EHANDLER 0x1
#define UNW_FLAG_UHANDLER 0x2
#define UNW_FLAG_FHANDLER 0x3
#define UNW_FLAG_CHAININFO 0x4

typedef union _UNWIND_CODE {
  struct {
    UCHAR CodeOffset;
    UCHAR UnwindOp : 4;
    UCHAR OpInfo : 4;
  };
  USHORT FrameOffset;
} UNWIND_CODE, *PUNWIND_CODE;

typedef struct _UNWIND_INFO {
  UCHAR Version : 3;           // + 0x00 - Unwind info structure version
  UCHAR Flags : 5;             // + 0x00 - Flags (see above)
  UCHAR SizeOfProlog;          // + 0x01
  UCHAR CountOfCodes;          // + 0x02 - Count of unwind codes
  UCHAR FrameRegister : 4;     // + 0x03
  UCHAR FrameOffset : 4;       // + 0x03
  UNWIND_CODE UnwindCode[1];   // + 0x04 - Unwind code array
                               /*
                               UNWIND_CODE MoreUnwindCode[((CountOfCodes + 1) & ~1) - 1];
                               union {
                               OPTIONAL ULONG ExceptionHandler;    // Exception handler routine
                               OPTIONAL ULONG FunctionEntry;
                               };
                               OPTIONAL ULONG ExceptionData[];        // C++ Scope table structure
                               */
} UNWIND_INFO, *PUNWIND_INFO;

typedef struct _UNWIND_HISTORY_TABLE_ENTRY {
  ULONG64 ImageBase;
  PRUNTIME_FUNCTION FunctionEntry;
} UNWIND_HISTORY_TABLE_ENTRY, *PUNWIND_HISTORY_TABLE_ENTRY;

typedef struct _UNWIND_HISTORY_TABLE {
  ULONG Count;
  UCHAR Search;
  ULONG64 LowAddress;
  ULONG64 HighAddress;
  UNWIND_HISTORY_TABLE_ENTRY Entry[UNWIND_HISTORY_TABLE_SIZE];
} UNWIND_HISTORY_TABLE, *PUNWIND_HISTORY_TABLE;

PRUNTIME_FUNCTION
RtlLookupFunctionEntry(
  DWORD64 ControlPc,
  PDWORD64 ImageBase,
  PUNWIND_HISTORY_TABLE HistoryTable
);

PVOID
RtlVirtualUnwind(
  ULONG HandlerType,
  ULONG64 ImageBase,
  ULONG64 ControlPc,
  PRUNTIME_FUNCTION FunctionEntry,
  PCONTEXT ContextRecord,
  PVOID *HandlerData,
  PULONG64 EstablisherFrame,
  PVOID ContextPointers
);

#define PROCESS_TERMINATE                  (0x0001)  
#define PROCESS_CREATE_THREAD              (0x0002)  
#define PROCESS_SET_SESSIONID              (0x0004)  
#define PROCESS_VM_OPERATION               (0x0008)  
#define PROCESS_VM_READ                    (0x0010)  
#define PROCESS_VM_WRITE                   (0x0020)  
#define PROCESS_CREATE_PROCESS             (0x0080)  
#define PROCESS_SET_QUOTA                  (0x0100)  
#define PROCESS_SET_INFORMATION            (0x0200)  
#define PROCESS_QUERY_INFORMATION          (0x0400)  
#define PROCESS_SUSPEND_RESUME             (0x0800)  
#define PROCESS_QUERY_LIMITED_INFORMATION  (0x1000)  
#define PROCESS_SET_LIMITED_INFORMATION    (0x2000)  

#endif

NTSYSCALLAPI
NTSTATUS
NTAPI
ZwQueryInformationProcess(
  HANDLE ProcessHandle,
  PROCESSINFOCLASS ProcessInformationClass,
  PVOID ProcessInformation,
  ULONG ProcessInformationLength,
  PULONG ReturnLength
);

#define DRIVE_UNKNOWN     0
#define DRIVE_NO_ROOT_DIR 1
#define DRIVE_REMOVABLE   2
#define DRIVE_FIXED       3
#define DRIVE_REMOTE      4
#define DRIVE_CDROM       5
#define DRIVE_RAMDISK     6

typedef enum _SYSTEM_INFORMATION_CLASS {
  SystemBasicInformation = 0x0,
  SystemProcessorInformation = 0x1,
  SystemPerformanceInformation = 0x2,
  SystemTimeOfDayInformation = 0x3,
  SystemPathInformation = 0x4,
  SystemProcessInformation = 0x5,
  SystemCallCountInformation = 0x6,
  SystemDeviceInformation = 0x7,
  SystemProcessorPerformanceInformation = 0x8,
  SystemFlagsInformation = 0x9,
  SystemCallTimeInformation = 0xa,
  SystemModuleInformation = 0xb,
  SystemLocksInformation = 0xc,
  SystemStackTraceInformation = 0xd,
  SystemPagedPoolInformation = 0xe,
  SystemNonPagedPoolInformation = 0xf,
  SystemHandleInformation = 0x10,
  SystemObjectInformation = 0x11,
  SystemPageFileInformation = 0x12,
  SystemVdmInstemulInformation = 0x13,
  SystemVdmBopInformation = 0x14,
  SystemFileCacheInformation = 0x15,
  SystemPoolTagInformation = 0x16,
  SystemInterruptInformation = 0x17,
  SystemDpcBehaviorInformation = 0x18,
  SystemFullMemoryInformation = 0x19,
  SystemLoadGdiDriverInformation = 0x1a,
  SystemUnloadGdiDriverInformation = 0x1b,
  SystemTimeAdjustmentInformation = 0x1c,
  SystemSummaryMemoryInformation = 0x1d,
  SystemMirrorMemoryInformation = 0x1e,
  SystemPerformanceTraceInformation = 0x1f,
  SystemObsolete0 = 0x20,
  SystemExceptionInformation = 0x21,
  SystemCrashDumpStateInformation = 0x22,
  SystemKernelDebuggerInformation = 0x23,
  SystemContextSwitchInformation = 0x24,
  SystemRegistryQuotaInformation = 0x25,
  SystemExtendServiceTableInformation = 0x26,
  SystemPrioritySeperation = 0x27,
  SystemVerifierAddDriverInformation = 0x28,
  SystemVerifierRemoveDriverInformation = 0x29,
  SystemProcessorIdleInformation = 0x2a,
  SystemLegacyDriverInformation = 0x2b,
  SystemCurrentTimeZoneInformation = 0x2c,
  SystemLookasideInformation = 0x2d,
  SystemTimeSlipNotification = 0x2e,
  SystemSessionCreate = 0x2f,
  SystemSessionDetach = 0x30,
  SystemSessionInformation = 0x31,
  SystemRangeStartInformation = 0x32,
  SystemVerifierInformation = 0x33,
  SystemVerifierThunkExtend = 0x34,
  SystemSessionProcessInformation = 0x35,
  SystemLoadGdiDriverInSystemSpace = 0x36,
  SystemNumaProcessorMap = 0x37,
  SystemPrefetcherInformation = 0x38,
  SystemExtendedProcessInformation = 0x39,
  SystemRecommendedSharedDataAlignment = 0x3a,
  SystemComPlusPackage = 0x3b,
  SystemNumaAvailableMemory = 0x3c,
  SystemProcessorPowerInformation = 0x3d,
  SystemEmulationBasicInformation = 0x3e,
  SystemEmulationProcessorInformation = 0x3f,
  SystemExtendedHandleInformation = 0x40,
  SystemLostDelayedWriteInformation = 0x41,
  SystemBigPoolInformation = 0x42,
  SystemSessionPoolTagInformation = 0x43,
  SystemSessionMappedViewInformation = 0x44,
  SystemHotpatchInformation = 0x45,
  SystemObjectSecurityMode = 0x46,
  SystemWatchdogTimerHandler = 0x47,
  SystemWatchdogTimerInformation = 0x48,
  SystemLogicalProcessorInformation = 0x49,
  SystemWow64SharedInformationObsolete = 0x4a,
  SystemRegisterFirmwareTableInformationHandler = 0x4b,
  SystemFirmwareTableInformation = 0x4c,
  SystemModuleInformationEx = 0x4d,
  SystemVerifierTriageInformation = 0x4e,
  SystemSuperfetchInformation = 0x4f,
  SystemMemoryListInformation = 0x50,
  SystemFileCacheInformationEx = 0x51,
  SystemThreadPriorityClientIdInformation = 0x52,
  SystemProcessorIdleCycleTimeInformation = 0x53,
  SystemVerifierCancellationInformation = 0x54,
  SystemProcessorPowerInformationEx = 0x55,
  SystemRefTraceInformation = 0x56,
  SystemSpecialPoolInformation = 0x57,
  SystemProcessIdInformation = 0x58,
  SystemErrorPortInformation = 0x59,
  SystemBootEnvironmentInformation = 0x5a,
  SystemHypervisorInformation = 0x5b,
  SystemVerifierInformationEx = 0x5c,
  SystemTimeZoneInformation = 0x5d,
  SystemImageFileExecutionOptionsInformation = 0x5e,
  SystemCoverageInformation = 0x5f,
  SystemPrefetchPatchInformation = 0x60,
  SystemVerifierFaultsInformation = 0x61,
  SystemSystemPartitionInformation = 0x62,
  SystemSystemDiskInformation = 0x63,
  SystemProcessorPerformanceDistribution = 0x64,
  SystemNumaProximityNodeInformation = 0x65,
  SystemDynamicTimeZoneInformation = 0x66,
  SystemCodeIntegrityInformation = 0x67,
  SystemProcessorMicrocodeUpdateInformation = 0x68,
  SystemProcessorBrandString = 0x69,
  SystemVirtualAddressInformation = 0x6a,
  SystemLogicalProcessorAndGroupInformation = 0x6b,
  SystemProcessorCycleTimeInformation = 0x6c,
  SystemStoreInformation = 0x6d,
  SystemRegistryAppendString = 0x6e,
  SystemAitSamplingValue = 0x6f,
  SystemVhdBootInformation = 0x70,
  SystemCpuQuotaInformation = 0x71,
  SystemNativeBasicInformation = 0x72,
  SystemErrorPortTimeouts = 0x73,
  SystemLowPriorityIoInformation = 0x74,
  SystemBootEntropyInformation = 0x75,
  SystemVerifierCountersInformation = 0x76,
  SystemPagedPoolInformationEx = 0x77,
  SystemSystemPtesInformationEx = 0x78,
  SystemNodeDistanceInformation = 0x79,
  SystemAcpiAuditInformation = 0x7a,
  SystemBasicPerformanceInformation = 0x7b,
  SystemQueryPerformanceCounterInformation = 0x7c,
  SystemSessionBigPoolInformation = 0x7d,
  SystemBootGraphicsInformation = 0x7e,
  SystemScrubPhysicalMemoryInformation = 0x7f,
  SystemBadPageInformation = 0x80,
  SystemProcessorProfileControlArea = 0x81,
  SystemCombinePhysicalMemoryInformation = 0x82,
  SystemEntropyInterruptTimingInformation = 0x83,
  SystemConsoleInformation = 0x84,
  SystemPlatformBinaryInformation = 0x85,
  SystemThrottleNotificationInformation = 0x86,
  SystemHypervisorProcessorCountInformation = 0x87,
  SystemDeviceDataInformation = 0x88,
  SystemDeviceDataEnumerationInformation = 0x89,
  SystemMemoryTopologyInformation = 0x8a,
  SystemMemoryChannelInformation = 0x8b,
  SystemBootLogoInformation = 0x8c,
  SystemProcessorPerformanceInformationEx = 0x8d,
  SystemSpare0 = 0x8e,
  SystemSecureBootPolicyInformation = 0x8f,
  SystemPageFileInformationEx = 0x90,
  SystemSecureBootInformation = 0x91,
  SystemEntropyInterruptTimingRawInformation = 0x92,
  SystemPortableWorkspaceEfiLauncherInformation = 0x93,
  SystemFullProcessInformation = 0x94,
  SystemKernelDebuggerInformationEx = 0x95,
  SystemBootMetadataInformation = 0x96,
  SystemSoftRebootInformation = 0x97,
  SystemElamCertificateInformation = 0x98,
  SystemOfflineDumpConfigInformation = 0x99,
  SystemProcessorFeaturesInformation = 0x9a,
  SystemRegistryReconciliationInformation = 0x9b,
  MaxSystemInfoClass = 0x9c,
} SYSTEM_INFORMATION_CLASS;

typedef struct _SYSTEM_BASIC_INFORMATION {
  ULONG Reserved;
  ULONG TimerResolution;
  ULONG PageSize;
  ULONG NumberOfPhysicalPages;
  ULONG LowestPhysicalPageNumber;
  ULONG HighestPhysicalPageNumber;
  ULONG AllocationGranularity;
  ULONG_PTR MinimumUserModeAddress;
  ULONG_PTR MaximumUserModeAddress;
  ULONG_PTR ActiveProcessorsAffinityMask;
  CCHAR NumberOfProcessors;
} SYSTEM_BASIC_INFORMATION, *PSYSTEM_BASIC_INFORMATION;

typedef struct _RTL_PROCESS_MODULE_INFORMATION {
  HANDLE Section;
  PVOID MappedBase;
  PVOID ImageBase;
  ULONG ImageSize;
  ULONG Flags;
  USHORT LoadOrderIndex;
  USHORT InitOrderIndex;
  USHORT LoadCount;
  USHORT OffsetToFileName;
  UCHAR  FullPathName[256];
} RTL_PROCESS_MODULE_INFORMATION, *PRTL_PROCESS_MODULE_INFORMATION;

typedef struct _SYSTEM_PROCESS_INFORMATION {
  ULONG NextEntryOffset;
  ULONG NumberOfThreads;
  UINT8 Reserved1[48];
  PVOID Reserved2[3];
  HANDLE UniqueProcessId;
  PVOID Reserved3;
  ULONG HandleCount;
  UINT8 Reserved4[4];
  PVOID Reserved5[11];
  SIZE_T PeakPagefileUsage;
  SIZE_T PrivatePageCount;
  LARGE_INTEGER Reserved6[6];
} SYSTEM_PROCESS_INFORMATION;

typedef struct _RTL_PROCESS_MODULES {
  ULONG NumberOfModules;
  RTL_PROCESS_MODULE_INFORMATION Modules[1];
} RTL_PROCESS_MODULES, *PRTL_PROCESS_MODULES;

NTSYSAPI
NTSTATUS
NTAPI
ZwQuerySystemInformation(
  IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
  OUT PVOID SystemInformation,
  IN ULONG SystemInformationLength,
  OUT PULONG ReturnLength OPTIONAL
  );

typedef struct _TEB {
  PVOID todo;
} TEB, *PTEB;

typedef struct _THREAD_BASIC_INFORMATION {
  NTSTATUS ExitStatus;
  PTEB TebBaseAddress;
  CLIENT_ID ClientId;
  ULONG_PTR AffinityMask;
  uint32  Priority;
  LONG BasePriority;
} THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;

NTSTATUS
NTAPI
ZwQueryInformationThread(
  __in HANDLE ThreadHandle,
  __in THREADINFOCLASS ThreadInformationClass,
  __out_bcount(ThreadInformationLength) PVOID ThreadInformation,
  __in ULONG ThreadInformationLength,
  __out_opt PULONG ReturnLength
);

NTSYSAPI
NTSTATUS
NTAPI
ZwProtectVirtualMemory(
  IN HANDLE               ProcessHandle,
  IN OUT PVOID            *BaseAddress,
  IN OUT PULONG           NumberOfBytesToProtect,
  IN ULONG                NewAccessProtection,
  OUT PULONG              OldAccessProtection
  );

NTSYSAPI
NTSTATUS
NTAPI
ZwAlertThread(
  IN HANDLE ThreadHandle
  );

#ifdef HP_WIN_KM

#define INVALID_HANDLE_VALUE ((HANDLE)(LONG_PTR)-1)

NTSYSAPI
NTSTATUS
NTAPI
PsReferenceProcessFilePointer(
  PEPROCESS     pProcess,
  PFILE_OBJECT* ppFileObject
);

#endif // HP_WIN_KM

NTSYSAPI
NTSTATUS
NTAPI
ZwYieldExecution(
  VOID
  );


NTSTATUS
NTAPI
ZwAdjustPrivilegesToken(
  HANDLE TokenHandle,
  BOOLEAN DisableAllPrivileges,
  PTOKEN_PRIVILEGES NewState,
  ULONG BufferLength,
  PTOKEN_PRIVILEGES PreviousState,
  PULONG ReturnLength
);

typedef enum _SHUTDOWN_ACTION {
  ShutdownNoReboot,
  ShutdownReboot,
  ShutdownPowerOff
} SHUTDOWN_ACTION, *PSHUTDOWN_ACTION;

NTSYSAPI
NTSTATUS
NTAPI
NtShutdownSystem(
  IN SHUTDOWN_ACTION Action
);


#endif /* __HP_NTDEF_H__ */
