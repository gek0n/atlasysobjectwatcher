/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_MUTEX_H__
#define __OS_MUTEX_H__

typedef struct hp_mutex {
  KMUTANT mutex;
} hp_mutex_t;

#endif // OS_MUTEX_H__
