/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_FILE_H__
#define __OS_FILE_H__

typedef struct hp_file {
  HANDLE handle;
  uint64 offset;
} hp_file_t;

#define HP_PATH_MAX _MAX_PATH

#endif /* __OS_FILE_H__ */
