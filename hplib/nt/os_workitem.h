/*
* Copyright (c) 2017 Hyperionix, Inc.
* All rights reserved.
*/
#ifndef __HP_OS_WORKITEM_H__
#define __HP_OS_WORKITEM_H__

struct hp_workitem;

typedef void(*hp_workitem_proc_t)(struct hp_workitem *wi);

typedef struct hp_workitem {
  PIO_WORKITEM piowi;
  hp_workitem_proc_t callback;
  void *userData;
} hp_workitem_t;


int32 hp_CreateWorkItem(hp_workitem_t *wi, hp_workitem_proc_t callback, void *userData);
int32 hp_QueueWorkItem(hp_workitem_t *wi);
void hp_DestroyWorkItem(hp_workitem_t *wi);


#endif // __HP_OS_WORKITEM_H__
