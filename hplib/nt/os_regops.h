/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_REGOPS_H__
#define __HP_REGOPS_H__

typedef hp_iter_ret_t(hp_regkey_iterator_t)(wchar_t *root, wchar_t *name, void *arg);
typedef hp_iter_ret_t(hp_regval_iterator_t)(
                        wchar_t *root,
                        wchar_t *name,
                        int32 type,
                        void *data,
                        size_t dataSize,
                        void *arg);

PKEY_VALUE_PARTIAL_INFORMATION
hp_RegReadValue(wchar_t *keyName, wchar_t *valName);

int32 hp_RegWriteValue(
  wchar_t *keyName,  // IN
  wchar_t *valName,  // IN
  void    *data,     // IN
  size_t   dataSize, // IN
  uint32   type      // IN
);

int32 hp_RegEnumerateKey(wchar_t *keyName, hp_regkey_iterator_t *cb, void *arg);
int32 hp_RegEnumerateValue(wchar_t *keyName, hp_regval_iterator_t *cb, void *arg);

typedef struct {
  PCWSTR ValueName;
  ULONG  ValueType;
  PVOID ValueData;
  SIZE_T  ValueLength;
} RTL_WRITE_REG_VALUE;

int32
hp_RegWriteValues(
  uint32 relativeTo,
  wchar_t *path,
  RTL_WRITE_REG_VALUE *values,
  uint32 valuesNum
);

int32 hp_RegDeleteKey(uint32 relativeTo, wchar_t *path);


#endif // __HP_REGOPS_H__
