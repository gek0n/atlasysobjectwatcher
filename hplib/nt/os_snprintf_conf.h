#ifndef __OS_SNPRINTF_CONF_H__
#define __OS_SNPRINTF_CONF_H__

//#define HAVE_VSNPRINTF
//#define HAVE_SNPRINTF
//#define HAVE_VASPRINTF
//#define HAVE_ASPRINTF
#define HAVE_STDARG_H                 1
#define HAVE_FLOAT_H                  1
#define HAVE_LONG_DOUBLE              1
#define HAVE_LONG_LONG_INT            1
#define HAVE_UNSIGNED_LONG_LONG_INT   1
#define HAVE_UINTPTR_T                1
#define HAVE_PTRDIFF_T                1
#define HAVE_STDDEF_H                 1
#define NOT_USE_FLOAT                 1

#endif // __OS_SNPRINTF_CONF_H__
