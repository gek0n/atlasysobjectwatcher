/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_THREADOPS_H__
#define __OS_THREADOPS_H__

void* hp_ThreadObjGetByTid(ptr_t tid);

#endif /* __OS_THREADOPS_H__ */
