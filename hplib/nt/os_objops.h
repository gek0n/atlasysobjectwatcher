/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef _OS_INFOBJ_H__
#define _OS_INFOBJ_H__

typedef hp_iter_ret_t (hp_objdir_iterator_t)(const char *root, char *name, char *type, void *arg);

int32 hp_ObjDirectoryIterate(const char *root, hp_objdir_iterator_t *cb, void *arg);

/* Need to call hp_ObjDeref after successful call */
void* hp_ObjGetByHandle(HANDLE obj);

/* Need to call hp_ObjDeref after successful call */
void hp_ObjRef(void *object);

void hp_ObjDeref(void *object);
POBJECT_TYPE hp_ObjGetType(void *object);

/* Need to call hp_ObjFreeStr after successful call */
char* hp_ObjGetTypeStr(void *object);

/* Need to call hp_ObjFreeStr after successful call */
char* hp_ObjGetName(void *object, logical resolveVolumeLabel);

void hp_ObjChangeCallbackRegistration(POBJECT_TYPE objType, logical enable);

int32 hp_ObjGetAccessMask(HANDLE handle, uint32 *access);

typedef enum {
  HP_HANDLE_OP_CREATE,
  HP_HANDLE_OP_DUPLICATE
} hp_handle_op_type;

typedef
void
(hp_objhandle_callback_t)(
  POBJECT_TYPE       objType,
  void              *obj,
  hp_handle_op_type  opType,
  uint32             access,
  logical            preOperation,
  void              *arg);

typedef struct objcb hp_objcb_t;

int32
hp_ObjRegisterHandleCallback(
  POBJECT_TYPE *objTypes[],
  uint32 num,
  hp_objhandle_callback_t* cb,
  void *arg,
  hp_objcb_t **handle);

void hp_ObjDeregisterHandleCallback(hp_objcb_t *handle);

HANDLE hp_ObjGetHandle(void *obj);
void hp_ObjCloseHandle(HANDLE handle);

int32 hp_ObjopsIni(void);
void  hp_ObjopsFin(void);

#endif // OS_INFOBJ_H__
