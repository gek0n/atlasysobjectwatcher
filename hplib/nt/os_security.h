/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_SECURITY_H__
#define __OS_SECURITY_H__


HANDLE hp_GetProcessAccessToken(HANDLE processHandle, int32 access);

#ifdef HP_WIN_KM
HANDLE hp_GetProcessPrimaryAccessToken(void* processObj, int32 access);
#endif

int32 hp_TokenChangePrivilege(HANDLE tokenHandle, uint32 priv, logical enable);


/* use hp_Free() to release pointers returned by functions below */

PSID hp_GetSidFromAccessToken(HANDLE t);

char* hp_ConvertSidToString(PSID s);

char* hp_GetAccountNameFromSid(PSID s); // DOMAIN\User

HP_INLINE char const* hp_GetUserNameFromAccount(char const* domainAndUser)
{
  char const* p = hp_StrRChr(domainAndUser, '\\');
  if (p)
  {
    p++;
  }

  return p;
}

#endif // __OS_SECURITY_H__
