#ifndef __OS_STACKWALK_H__
#define __OS_STACKWALK_H__

typedef hp_iter_ret_t
(hp_foreach_stack_frame_cb_t)(
  PETHREAD  thread,
  uint32    frameNum,
  PCONTEXT  regs,
  void     *arg
);

//
// XXX: Only kernel-mode frames
// XXX: The thread should be suspended
// XXX: The function is very crude and works only in a few cases. You should
// use it only in extreme case and have good understanding why you use it :)
//

int32
hp_ForEachStackFrame(
  PETHREAD thread,
  hp_foreach_stack_frame_cb_t *cb,
  void *arg
);

#endif // __OS_STACKWALK_H__
