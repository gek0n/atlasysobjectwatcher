/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OSSTROPS_H__
#define __OSSTROPS_H__

int32 hp_VSnprintf(char *buf, size_t bufSize, const char *fmt, va_list args);


// Conversions functions below take an optional 'source length' parameter.
// Use HP_ENTIRE_STRING as a default length for NULL-terminated strings.
// Result strings are always NULL-terminated.
// NOTE: Length is in bytes for utf8 and in wchar_t for utf16 strings, depending on the DESTINATION string type.
// Functions that calculate buffer space, reserve room for '\0'.

#define HP_ENTIRE_STRING ((size_t)-1)

int32 hp_Utf8ToUtf16(const char *srcU8, size_t lenU8, wchar_t *dstU16, size_t lenU16);
size_t hp_Utf8ToUtf16Len(const char *srcU8, size_t lenU8);

int32 hp_Utf16ToUtf8(char *dstU8, size_t sizeU8, const wchar_t *srcU16, size_t sizeU16);
size_t hp_Utf16ToUtf8Len(const wchar_t *str, size_t len);

wchar_t* hp_Utf8ToUtf16Alloc(const char *strUtf8, size_t len);
char* hp_Utf16ToUtf8Alloc(const wchar_t *strUtf16, size_t len);

char* hp_UStrToUtf8Alloc(PUNICODE_STRING objNameUS);

// Use hp_StrFree to free string
// Resulting string is always NULL terminated
PUNICODE_STRING hp_UStrDup(PUNICODE_STRING str);


// Windows does not use UTF-8 for char*; instead, all *A() APIs and DbgPrintEx() expect some system-defined codepage like CP1252
char *hp_Utf16ToSystemLocale(const wchar_t *strUtf16, size_t len);


#endif /* __OSSTROPS_H__ */
