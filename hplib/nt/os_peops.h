/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_PEOPS_H__
#define __HP_PEOPS_H__


void* hp_PeLoadImage(const char* path);
void hp_PeUnloadImage(void* image);

PIMAGE_NT_HEADERS hp_PeGetNTImageHeader(void *base);
size_t hp_PeGetImageSize(void *handle);

int32 hp_PeRelocateImage(PIMAGE_NT_HEADERS nt, void *imgBase);
int32
hp_PeResolveImageReferences(
  PIMAGE_NT_HEADERS  nt,
  void              *imgBase,
  logical            systemImage,
  void              *proc
);

typedef hp_iter_ret_t
(hp_peforeach_export_t)(
  const char *name,
  void       *addr,
  void       *arg);

typedef enum hp_peforeach_export_mode {
  HP_PEFOREACH_EXPORT_MODE_LOOKUP, /* Binary search */
  HP_PEFOREACH_EXPORT_MODE_DIRECT, /* A -> Z */
  HP_PEFOREACH_EXPORT_MODE_REVERSE, /* Z -> A */
  HP_PEFOREACH_EXPORT_MODE_MAX
} hp_peforeach_export_mode_t;

int32
hp_PeForEachExport(
  void                       *handle,
  hp_peforeach_export_t      *cb,
  void                       *arg,
  hp_peforeach_export_mode_t  mode);

void* hp_PeGetExport(void *handle, const char *name);

int32
hp_PeGetModuleGuidAndPdbName(
  void   *base,
  char   *guidBuf,
  size_t  guidBufSize,
  char   *pdbNameBuf,
  size_t  pdbNameBufSize);

#endif /* __HP_PEOPS_H__ */
