/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_NAMEFORMAT_H__
#define __OS_NAMEFORMAT_H__

char *hp_UnixPathToWindowsPath(const char *path);

HP_INLINE logical hp_IsUnixPath(const char *path)
{
  if (path && *path == '/')
  {
    return TRUE;
  }

  return FALSE;
}

HP_INLINE logical hp_IsNtPath(const char *path)
{
  if (path && *path == '\\')
  {
    return TRUE;
  }

  return FALSE;
}

#endif // __OS_NAMEFORMAT_H__
