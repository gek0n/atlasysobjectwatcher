/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_PROCOPS_H__
#define __OS_PROCOPS_H__

#ifdef HP_WIN_KM

void* hp_ProcObjGetByPid(ptr_t pid);
char* hp_ProcObjGetCmdLine(void *eproc);
uint64 hp_ProcIdGetByThread(void *thread);

#define hp_CurrentProcess() ZwCurrentProcess()

#endif // HP_WIN_KM

uint32 hp_EnumerateProcesses(uint32 **pids);

#ifdef HP_WIN_UM

#define hp_CurrentProcess() GetCurrentProcess()

char* hp_GetProcessImageName(uint32 pid);

#endif // HP_WIN_UM


#endif // __OS_PROCOPS_H__
