/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */
#ifndef __HP_OS_DPC_H__
#define __HP_OS_DPC_H__

struct hp_dpc;

typedef void(*hp_dpc_proc_t)(struct hp_dpc *dpc);


typedef struct hp_dpc {
  KDPC dpc;
  hp_dpc_proc_t callback;
  void *userData;
} hp_dpc_t;


void hp_QueueDpc(hp_dpc_t *dpc, hp_dpc_proc_t callback, void *userData);


#endif // __HP_OS_DPC_H__

