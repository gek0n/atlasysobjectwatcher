/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_INC_H__
#define __OS_INC_H__

#include <hplib/nt/os_ntdef.h>
#include <hplib/nt/os_strops.h>
#include <hplib/nt/os_queryinfo.h>
#include <hplib/nt/os_peops.h>
#include <hplib/nt/os_nameformat.h>
#include <hplib/nt/os_objops.h>
#include <hplib/nt/os_procops.h>
#include <hplib/nt/os_threadops.h>
#include <hplib/nt/os_regops.h>
#include <hplib/nt/os_snprintf_conf.h>
#include <hplib/nt/os_security.h>
#include <hplib/nt/os_mmintern.h>
#include <hplib/nt/os_io.h>
#include <hplib/nt/os_stackwalk.h>

#if defined(HP_WIN_KM)
extern PDRIVER_OBJECT  gDriverObject;
extern PUNICODE_STRING gRegistryPath;
extern PUSHORT         NtBuildNumber;
#endif

#endif /* __OS_INC_H__ */
