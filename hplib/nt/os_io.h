/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_IO_H__
#define __OS_IO_H__

HP_INLINE void hp_CompleteIrp(PIRP irp, NTSTATUS status, ULONG_PTR info)
{
  irp->IoStatus.Status = status;
  irp->IoStatus.Information = info;
  IoCompleteRequest(irp, IO_NO_INCREMENT);
}

HP_INLINE int32 hp_GetBufFromReadIrp(PIRP irp, void **buf, uint32 *size)
{
  *buf = MmGetSystemAddressForMdlSafe(irp->MdlAddress, NormalPagePriority);
  if (!*buf) {
    return HPE_FAILURE;
  }

  *size = IoGetCurrentIrpStackLocation(irp)->Parameters.Read.Length;
  return HPE_SUCCESS;
}

#define HP_EXIT_STATUS(IRP, STATUS, INFO) \
  do { \
    hp_CompleteIrp((IRP), (STATUS), (INFO)); \
    return (STATUS); \
  } while (0)

int32
hp_SendIOCTLToDevice(
  PDEVICE_OBJECT devObj,
  uint32 ioctl,
  void  *inBuf,
  uint32 inBufSize,
  void *outBuf,
  uint32 outBufSize,
  PKEVENT event
);

#endif // __OS_IO_H__
