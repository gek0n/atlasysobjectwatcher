#ifndef __HP_SNPRINTF_H__
#define __HP_SNPRINTF_H__

#if defined(HP_WIN)
int rpl_snprintf(char *, size_t, const char *, ...);
int rpl_vsnprintf(char *, size_t, const char *, va_list);
int rpl_vasprintf(char **, const char *, va_list);

#undef vasprintf
#define vasprintf rpl_vasprintf

#undef vsnprintf
#define vsnprintf rpl_vsnprintf

#define hp_Snprintf(buf, bufSize, ...) rpl_snprintf(buf, bufSize, __VA_ARGS__)
#define hp_VSnprintf(buf, bufSize, fmt, va_list) rpl_vsnprintf(buf, bufSize, fmt, va_list)

#elif defined(HP_LINUX) || defined(HP_OSX)
#define hp_Snprintf(buf, bufSize, ...) snprintf(buf, bufSize, __VA_ARGS__)
#define hp_VSnprintf(buf, bufSize, fmt, va_list) vsnprintf(buf, bufSize, fmt, va_list)
#endif

#endif // __HP_SNPRINTF_H__
