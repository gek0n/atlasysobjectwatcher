/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPFILE_H__
#define __HPFILE_H__

#if defined(HP_WIN)
#include <hplib/nt/os_file.h>
#elif defined(HP_OSX_KM)
#include <hplib/xnu/os_file.h>
#elif defined(HP_OSX_UM)
#include <hplib/posix/os_file.h>
#elif defined(HP_LINUX_KM)
#include <hplib/lnx/os_file.h>
#endif

#if defined(HP_WIN_UM)
  #define TO_DOSPATH(PATH) ((PATH) + sizeof("\\??\\") - 1)
#else
  #define TO_DOSPATH(PATH) (PATH)
#endif

#if defined(HP_WIN)
  #define HP_PATH_SEPARATOR_EX_CHR '/'
  #define HP_PATH_SEPARATOR_CHR    '\\'
  #define HP_PATH_SEPARATOR        "\\"
#else
  #define HP_PATH_SEPARATOR_CHR '/'
  #define HP_PATH_SEPARATOR     "/"
#endif

typedef void (STDCALL *hp_FileIoCompletionRoutine)(hp_file_t *file, void *context);

/************************************************************************/
/* File create/open flags                                               */
/************************************************************************/

/* Read only */
#define FOPS_RDONLY 0x00000001
/* Write only */
#define FOPS_WRONLY 0x00000002
/* Read-write */
#define FOPS_RDWR   (FOPS_RDONLY | FOPS_WRONLY)
/* Delete */
#define FOPS_DEL    0x00000100


/* Create new file, error if exist */
#define FOPS_CRNEW      0x00000010
/* Open existing file, error if not exist */
#define FOPS_OPEN       0x00000020
/* Create new file, overwrite existing */
#define FOPS_CRNEW_OVER 0x00000040


/* Allow other processes to read or write this file */
#define FOPS_SHARE_RW   0x00001000
/* Allow other processes to read, write, or delete this file */
#define FOPS_SHARE_RWD  0x00002000

/* Caching option (cached by default) */
#define FOPS_FLUSH 0x10000000

#define FOPS_ASYNC 0x20000000

/* Seek Flags */
#define SEEK_SET     0
#define SEEK_CUR     1
#define SEEK_END     2


int32 hp_FileCreate(hp_file_t *f, const char *name, int32 flags);
void hp_FileClose(hp_file_t *f);

int32 hp_FileRead(hp_file_t *f, void *buf, uint32 size);
int32 hp_FileReadAsync(hp_file_t *f, void *buf, uint32 size, hp_FileIoCompletionRoutine callback, void *context);
int32 hp_FileReadFull(const char *name, void **buf, uint64 *size, logical nullTerm);
int32 hp_FileWrite(hp_file_t *f, void *buf, uint32 size);
int32 hp_FileWriteAsync(hp_file_t *f, void *buf, uint32 size, hp_FileIoCompletionRoutine callback, void *context);

int32 hp_FileGetSize(hp_file_t *f, uint64* size);


logical hp_FileExists(const char* name);
logical hp_DirectoryExists(const char* path);

int32 hp_DirectoryCreate(const char *path);
int32 hp_DirectoryDelete(const char *path);

int32 hp_GetSystemDirectory(char* buffer, size_t bufferSize);

logical hp_IsRemovableDrivePath(const char* path);

void hp_FileNameWithoutExt(const char *fileName, char *buf, size_t bufSize);

HP_INLINE const char *hp_FileBaseName(const char *path)
{
  const char *b;

  if (!path) {
    return NULL;
  }

  b = hp_StrRChr(path, HP_PATH_SEPARATOR_CHR);
#if defined(HP_WIN)
  if (!b) {
    b = hp_StrRChr(path, HP_PATH_SEPARATOR_EX_CHR);
  }
#endif
  return b == NULL ? path : b + 1;
}

int32 hp_FileRename(const char *filename, const char *newFilename);
int32 hp_FileDelete(const char *filename);

char* hp_GetSystemVolume(void);
char* hp_NormalizePath(char const *src);

int32 hp_FileFlush(hp_file_t *f);

#endif /* __HPFILE_H__ */
