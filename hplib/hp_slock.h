#ifndef __HP_SLOCK_H__
#define __HP_SLOCK_H__

typedef struct {
  hp_atomic32_t val;
} hp_slock_t;

void hp_SlockCreate(hp_slock_t *lock);
void hp_SlockAcquire(hp_slock_t *lock);
void hp_SlockRelease(hp_slock_t *lock);

#endif // __HP_SLOCK_H__
