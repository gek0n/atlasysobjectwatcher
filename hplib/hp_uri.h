#ifndef __HP_URI_H__
#define __HP_URI_H__

int32 hp_ParseURI(const char* _buf,
    const char **scheme, size_t *scheme_len,
    const char **host, size_t *host_len,
    uint16 *port,
    const char **path_query, size_t *path_query_len);

#endif // __HP_URI_H__
