#ifndef __CTCACHE_H__
#define __CTCACHE_H__


/* ------------------------------------------------------------------------- */
/* (c) Copyright 2008-2013 Atlas Consultants Group, Inc.                     */
/* (c) Copyright 2013 CounterTack, Inc.                                      */
/* All Rights Reserved                                                       */
/* ------------------------------------------------------------------------- */

typedef struct hp_cache hp_cache_t;

#pragma pack(push)
#pragma pack(1)
typedef union {
   struct {
      /* Flag the handle as having been deleted.  It will continue being
       * available to anyone who is already referencing it, but no new
       * references will be created, i.e. it will not be found via a lookup.
       */
      unsigned int  Deleted:1;
      unsigned int  bit01:1;
      unsigned int  bit02:1;
      unsigned int  bit03:1;
      unsigned int  bit04:1;
      unsigned int  bit05:1;
      unsigned int  bit06:1;
      unsigned int  bit07:1;
      unsigned int  bit08:1;
      unsigned int  bit09:1;
      unsigned int  bit10:1;
      unsigned int  bit11:1;
      unsigned int  bit12:1;
      unsigned int  bit13:1;
      unsigned int  bit14:1;
      unsigned int  bit15:1;

      unsigned int LastCount:16; /* bits 16-31 */
   } s;
   int32 Flags;
} UnionFlagsT, *UnionFlagsPtrT;
#pragma pack(pop)

typedef enum
{
   /* Maps to Deleted flag.  When set lookups will fail. */
   CACHE_HANDLE_FLAG_BIT_DELETED = 0,

   /* Maps to bit01 flag.  It has no special meaning to the Cache
    * management code, but normally it would be used by the consumer
    * when it needs to assure that an extra ref count is released only once.
    */
   CACHE_HANDLE_FLAG_BIT_EXTRA_REF_RELEASED = 1,
} hp_CacheHandleFlagT;

typedef struct hp_cache_handle
{
   UnionFlagsT           u;
   /* Don't change the order. The first 4 bytes of ag_CacheHandleT will be
    * trashed when freed.
    * RefCount must be valid even when deallocated.
    */
   hp_atomic32_t         RefCount;
   void*                 Data;
} hp_cache_handle_t;

typedef hp_cache_handle_t *hp_CacheHandlePtrT;

/*
 * Thread Safe Wait Free Cache Functions
 *
 * This set of functions allows for a fast key based insertion and lookup
 * of data in a thread safe, wait free fashion.
 * The cache functionality allows for the old data to be deleted
 * automatically from the lookup tables.
 */

/* Callback functions to be called when the cached object is first
 * cloned for storing in the cache and then freed, when removing from cache.
 */
typedef const void *(hp_CacheCloneObjectT) (const void *Object, size_t size, void *ctx);
typedef void  (hp_CacheFreeObjectT)  (void *Object, void *ctx);
typedef void  (hp_CacheHandleSetT)  (void *Object, hp_CacheHandlePtrT handle);

/*
 * hp_CacheCreate() will return a pointer to a data structure that would
 * contain the mapping between any type of data and a key associated with it.
 * The table will contain up to CacheSize elements.  Once the table size
 * exceeds this value the least recently looked up value will be removed
 * from it to make space for new values.
 *
 * Arguments:
 * ----------
 * KeyGet() - function that returns the value of type "(void *)" that
 * uniquely identifies the item.  Providing NULL will result in the address
 * of the data being treated as the key.

 * KeyCompare() - returns TRUE if the two keys are the same.  Providing NULL,
 * will result in the keys being compared as two scalars.

 * HashFunction() - returns an integer that represents a hash value of the key.
 * For example if a key is a string, the hash may be equal to the sum of all
 * full words plus any leftover bytes.  Providing NULL will result in the
 * Key being treated as a hash value.
 *
 * Ctx - context data to be associated with the table
 */
hp_cache_t * hp_CacheCreate(
   hp_HashKeyGetT       KeyGet,
   hp_HashKeyCompareT   KeyCompare,
   hp_HashFunctionT     HashFunction,
   hp_CacheCloneObjectT CacheCloneObject,
   hp_CacheFreeObjectT  CacheFreeObject,
   size_t             CacheSize,
   unsigned int         Flags,
   void                 *Ctx);

/* Limit Cache flags to upper 16 bits.  This way the lower 16 bits could be
 * used as hash flags without having to pass around 2 different set of flags.
 */
typedef enum {
   CACHE_CONSTANT_DESCRIPTOR = (1 << 16),
} hp_CacheFlagValuesT;

typedef struct
{
   hp_HashKeyGetT       *KeyGet;
   hp_HashKeyCompareT   *KeyCompare;
   hp_HashFunctionT     *HashFunction;
   hp_CacheCloneObjectT *CacheCloneObject;
   hp_CacheFreeObjectT  *CacheFreeObject;
   hp_CacheHandleSetT   *CacheHandleSet;
   unsigned int         Flags;
} hp_CacheDescriptorT;

typedef const hp_CacheDescriptorT* ag_CacheDescriptorPtrT;

hp_cache_t * hp_CacheCreateEx(
   ag_CacheDescriptorPtrT CacheDescriptor,
   size_t               CacheSize,
   void                   *Ctx);

size_t hp_CacheNumElements(hp_cache_t * CachePtr);

/*
 * Inserts the given pointer to the data in the hash table.
 * Parameters
 *    CachePtr - pointer to the hash table
 *    Data     - pointer to the data to insert
 *    Size     - size of the data to insert (make copy)
 * Returns
 *    Pointer to the data inserted in the table
 */
void *hp_CacheInsertEx(
   hp_cache_t * CachePtr,
   void         *Data,
   size_t        Size,
   void         *Ctx);
#define hp_CacheInsert(C,D,S)  hp_CacheInsertEx(C,D,S,NULL)

/*
 * Inserts the data into the hash table, if it has no data with the same key.
 * Parameters
 *    CachePtr - pointer to the hash table
 *    Data     - pointer to the data to insert
 *    Size     - size of the data to insert (make copy)
 * Returns
 *    Handle for the inserted data.
 */
hp_CacheHandlePtrT hp_CacheInsertHandleEx(
   hp_cache_t * CachePtr,
   void         *Data,
   size_t     Size,
   void         *Ctx);
#define   hp_CacheInsertHandle(C,D,S)  hp_CacheInsertHandleEx(C,D,S,NULL)

/*
 * Inserts the data into the hash table whether it is there already or not.
 * Parameters
 *    CachePtr - pointer to the hash table
 *    Data     - pointer to the data to insert
 *    Size     - size of the data to insert (make copy)
 * Returns
 *    Handle for the inserted data.
 */
hp_CacheHandlePtrT hp_CacheInsertHandleForceEx(
   hp_cache_t * CachePtr,
   const void   *Data,
   size_t     Size,
   void         *Ctx);
#define   hp_CacheInsertHandleForce(C,D,S)  hp_CacheInsertHandleForceEx(C,D,S,NULL)

/*
 * Inserts the given data into the cache table if not there already,
 * but doesn't increment its use count.  This function is useful
 * for inserting elements into a cache for future lookups.
 * The inserted data will enter the cache, and will age out if not
 * looked up.
 */
void hp_CacheInsertAndRelease(
   hp_cache_t * CachePtr,
   void         *Data,
   size_t     Size);

/*
 * Given the key lookup the data handle and increment its use count.
 */
hp_CacheHandlePtrT hp_CacheLookup(
   hp_cache_t * CachePtr,
   const void   *Key);


/*
 * Given the data handle get the data.
 */
__inline static void *hp_CacheHandleToData(hp_CacheHandlePtrT HandlePtr)
{
   return HandlePtr?HandlePtr->Data:NULL;
}

/*
 * Release the handle lookuped with ag_CacheLookup()
 * by decrementing its use count. Delete the data when RefCount == 0.
 */
void hp_CacheReleaseEx2(
   hp_cache_t *       CachePtr,
   hp_CacheHandlePtrT HandlePtr,
   int                ReleaseCount,
   void               *Ctx);
#define  hp_CacheReleaseEx(C,H,R)   hp_CacheReleaseEx2(C,H,R,NULL)
#define  hp_CacheRelease(C,H)   hp_CacheReleaseEx(C,H,1)

/* Atomically test and set a flag on the handle.
 * Return 0 if successfully set, 1 otherwise.
 */
int hp_CacheTestAndSetFlag(
   hp_CacheHandlePtrT HandlePtr,
   unsigned int       flagBit);

/* Get the ref count of a handle */
size_t hp_CacheRefCount(hp_CacheHandlePtrT HandlePtr);

/*
 * Lock the handle by incrementing its use count.
 */
hp_CacheHandlePtrT hp_CacheLock(hp_CacheHandlePtrT HandlePtr);

/*
 * Flush the given handle from the cache if it is there.
 * This will not remove the handle from the hash table
 * unless there are no more references to it from elsewhere
 * as determined by the handle use count.
 */
void hp_CacrheFlushHandle(
   hp_cache_t *       CachePtr,
   hp_CacheHandlePtrT HandlePtr);

/* Flush all handles from the cache */
void ag_CacheFlush(hp_cache_t * CachePtr);


/*
 * Release the data returned by ap_CacheInsert().  If this is the last
 * reference to the data, free it.  Note that additional weak references
 * may exist in the cache (if CacheSize > 0), so the data may linger
 * a beyond this call.
 * Return 0 if deleted OK, error code otherwise.
 */
int hp_CacheDeleteEx(
   hp_cache_t * CachePtr,
   void         *Data,
   void         *Ctx);

#define hp_CacheDelete(C,D)  hp_CacheDeleteEx(C,D,NULL)

/* Mark the handle as deleted. Handles marked as deleted will
 * not be found via subsequent lookups.
 * If the handle is marked as deleted for the first time
 * decrement its reference count by 1+ExtraReleaseCount,
 * otherwise decrement it by 1.
 * Return 0 if marked deleted for the first time, 1 otherwise.
 */
int hp_CacheDeleteHandleEx2(
   hp_cache_t *       CachePtr,
   hp_CacheHandlePtrT HandlePtr,
   int                ExtraReleaseCount,
   void               *Ctx);

/* hp_CacheDeleteHandle() when called on a locked handle
 * (e.g. from handle iterator, or after lookup) would normally
 * result in the handle's ref count going down to 0
 * and the data being freed. Other concurrent references to the
 * handle may increment the ref count and result in a delayed
 * freeing.
 */
#define hp_CacheDeleteHandleEx(C, H, R) hp_CacheDeleteHandleEx2(C, H, R, NULL)
#define hp_CacheDeleteHandle(C, H) hp_CacheDeleteHandleEx(C, H, 1)

/*
 * Free the data structures allocated for the hash table.
 */
void hp_CacheFree(hp_cache_t *   CachePtr);

/*
 * Attempt to delete all the cache entries by decrementing RefCount.
 * Return the number of elements remaining in the cache.
 */
unsigned int hp_CacheClear(hp_cache_t *   CachePtr);

/*
 * Given the cache table iterate over all the elements in it.
 * Return the last value returned by the IteratorFunction if any,
 * NULL otherwise.
 */
void* hp_CacheIterate(
   hp_cache_t *         CachePtr,
   hp_IteratorFunctionT IteratorFunction,
   void                 *Ctx);

typedef void* (*hp_CacheHandleIteratorT) (hp_CacheHandlePtrT, void *);

/*
 * Given the cache table iterate over all the elements in it that match the key.
 * Return the last value returned by the IteratorFunction if any,
 * NULL otherwise.
 */
void* hp_CacheHandleIterateByKey(
   hp_cache_t *            CachePtr,
   hp_CacheHandleIteratorT IteratorFunction,
   const void              *Key,
   void                    *Ctx);

/*
 * Given the cache table iterate over all the handles in it.
 * The ref count of each handle will be incremented before calling
 * the IteratorFunction.  It is the responsibility of the IteratorFunction
 * to decrement it back by calling hp_CacheRelease() on the handle.
 * Return the value from the last call to the IteratorFunction if any,
 * NULL otherwise.
 */
void *hp_CacheHandleIterate(
   hp_cache_t *            CachePtr,
   hp_CacheHandleIteratorT IteratorFunction,
   void                    *Ctx);

void *ag_Memcpy(void *t, const void *s, size_t size);


#define CACHE_FREE(C)                              \
   if (C) {                                        \
      hp_cache_t *tmp = C;                         \
      size_t n0;                                   \
      size_t n1;                                   \
      n0 = hp_CacheNumElements(tmp);               \
      hp_CacheClear(tmp);                          \
      n1 = hp_CacheNumElements(tmp);               \
      {hp_Assert(n1 == 0);}                        \
      hp_CacheFree(tmp);                           \
      C = NULL;                                    \
   }

void hp_CacheIni(void);
void hp_CacheFin(void);

#endif /* __CTCACHE_H__ */
