/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPINC_H__
#define __HPINC_H__

#if defined(HP_WIN_KM)
  #if defined(NETWORK_SUBSYSTEM)
    #include <ndis.h>
  #else
    #include <fltKernel.h>
    #include <Ntddstor.h>
    #include <ntifs.h>
    #include <ntstrsafe.h>
  #endif
  #include <stdlib.h>
  #include <ntimage.h>
#elif defined(HP_WIN_UM)
#if !defined(_CRT_SECURE_NO_WARNINGS)
  #define _CRT_SECURE_NO_WARNINGS
#endif
  #include <windows.h>
  #include <strsafe.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <stdint.h>
  #include <assert.h>
  #include <time.h>
#elif defined(HP_LINUX_KM)
  #include <linux/kernel.h>
  #include <linux/module.h>
  #include <linux/string.h>
  #include <linux/delay.h>
  #include <linux/version.h>
  #include <linux/ctype.h>
#elif defined(HP_LINUX_UM)
  #include <stdlib.h>
  #include <stdarg.h>
  #include <string.h>
  #include <stdio.h>
#elif defined(HP_OSX_KM)
  #include <stdint.h>
  #include <stdarg.h>
  #include <string.h>
  #include <ctype.h>
  #include <sys/types.h>
  #include <machine/types.h>
  #include <IOKit/IOLib.h>
  #include <sys/syslimits.h>
  #include <sys/_types/_wchar_t.h>
  #include <sys/_types/_ptrdiff_t.h>
  #include <sys/kernel_types.h>
#elif defined(HP_OSX_UM)
  #include <stdlib.h>
  #include <stdarg.h>
  #include <string.h>
  #include <stdio.h>
  #include <assert.h>
  #include <wchar.h>
  #include <pthread.h>
  #include <semaphore.h>
  #include <sys/syslimits.h>
#endif

#endif /* __HPINC_H__ */
