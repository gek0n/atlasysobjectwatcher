/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_SVCINSTALL_H__
#define __HP_SVCINSTALL_H__

int32 hp_SvcInstall(const char *svcPath, const char *svcName, const char *svcDisplayName);
int32 hp_SvcRun(const char *svcName);
void hp_SvcStop(const char *svcName);
logical hp_IsSvcRunning(const char *svcName);
void hp_SvcUninstall(const char *svcName);

#ifdef HP_WIN
int32 hp_CloseExistingAgentInstances(logical wait);
#endif

#endif // __HP_SVCINSTALL_H__
