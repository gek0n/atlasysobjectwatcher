/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_QUEUE_H__
#define __HP_QUEUE_H__

/* Pull out of a hat */
#define HP_QUEUE_MAX_ITEM_CNT 100000

typedef struct hp_queue hp_queue_t;

hp_queue_t* hp_QueueCreate(uint32 itemCnt);
/*
 * hp_QueuePut will put the given item on the queue, blocking if necessary.
 * Function forbid adding a NULL item.
 * Returns HP_SUCCESS if success, HP_FAILURE if queue is full and blocking is set to false.
 */
int32 hp_QueuePut(hp_queue_t *q, void *item, logical blocking);

/*
 * hp_QueueGet will get the next item from the queue, blocking if necessary.
 * It will return NULL if the queue is empty and blocking is set to false.
 * ap_QueueGet may be ambiguous when it returns NULL in non-blocking mode.
 */
void* hp_QueueGet(hp_queue_t *q, logical blocking);

uint32 hp_QueueItemCnt(hp_queue_t *q);

void hp_QueueDestroy(hp_queue_t *q);

#endif /* __HP_QUEUE_H__ */
