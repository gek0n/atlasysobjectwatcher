/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_MEMBUF_H__
#define __HP_MEMBUF_H__

typedef struct hp_membuf {
  char *bufStart; /* First buffer byte */
  char *bufEnd;  /* First overrun byte */
  char *dataPtr; /* Cur pointer */
} hp_membuf_t;

HP_INLINE void hp_MembufCreate(hp_membuf_t *mb, void *buf, size_t size)
{
  mb->bufStart = mb->dataPtr = (char *)buf;
  mb->bufEnd = mb->bufStart + size;
}

HP_INLINE void* hp_MembufGetBuf(hp_membuf_t* mb)
{
  return mb->bufStart;
}

HP_INLINE void* hp_MembufGetData(hp_membuf_t* mb)
{
  return mb->dataPtr;
}

HP_INLINE size_t hp_MembufGetTotalSize(hp_membuf_t *mb)
{
  return mb->bufEnd - mb->bufStart;
}

HP_INLINE size_t hp_MembufGetDataSize(hp_membuf_t *mb)
{
  return (mb->dataPtr - mb->bufStart);
}

HP_INLINE size_t hp_MembufGetRemainSize(hp_membuf_t *mb)
{
  return (mb->bufEnd - mb->dataPtr);
}

HP_INLINE logical hp_MembufIsInBounds(hp_membuf_t *mb, void *ptr)
{
  return ((char *)ptr >= mb->bufStart && (char *)ptr < mb->bufEnd);
}

/* Returns current data and adds size to data ptr */
HP_INLINE void* hp_MembufGetPush(hp_membuf_t* mb, size_t size)
{
  void* ret = NULL;
  char* tmp = mb->dataPtr + size;

  if (tmp <= mb->bufEnd && tmp > mb->bufStart) {
    ret = mb->dataPtr;
    mb->dataPtr = tmp;
  }

  return ret;
}

/* Adds size to data ptr and returns new data ptr */
HP_INLINE void* hp_MembufPushGet(hp_membuf_t* mb, size_t size)
{
  char* tmp = mb->dataPtr + size;

  if (tmp < mb->bufEnd && tmp > mb->bufStart) {
    mb->dataPtr = tmp;
    return mb->dataPtr;
  }

  return NULL;
}

HP_INLINE int32 hp_MembufDataCpy(hp_membuf_t* src, hp_membuf_t* dst, size_t size)
{
  void *s = hp_MembufGetPush(src, size);
  void *d = hp_MembufGetPush(dst, size);
  if (!s || !d) {
    return HPE_FAILURE;
  }

  memcpy(d, s, size);
  return HPE_SUCCESS;
}

#endif /* __HP_MEMBUF_H__ */
