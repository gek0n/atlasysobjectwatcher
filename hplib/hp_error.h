/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPERROR_H__
#define __HPERROR_H__

#define HPE_NODATA     (6)
#define HPE_MOREDATA   (5)
#define HPE_POSTNEEDED (4)
#define HPE_THREADTERM (3)
#define HPE_INPROGRESS (2)
#define HPE_ALREADY    (1)

#define HPE_SUCCESS    (0)

#define HPE_FAILURE      (-1)
#define HPE_NOMEM        (-2)
#define HPE_NOIMPL       (-3)
#define HPE_INVAL        (-4)
#define HPE_NOSUCHFILE   (-5)
#define HPE_INVALPATH    (-6)
#define HPE_SHARING      (-7)
#define HPE_IOFAIL       (-8)
#define HPE_NOPROC       (-9)
#define HPE_ACCDENIED    (-10)
#define HPE_PATCHFAIL    (-11)
#define HPE_LUAFAIL      (-12)
#define HPE_NOTFOUND     (-13)
#define HPE_TIMEOUT      (-14)
#define HPE_INVALEXEC    (-15)
#define HPE_PENDING      (-16)
#define HPE_CANCELED     (-17)
#define HPE_INVALSTATE   (-18)
#define HPE_NOCONFIG     (-19)
#define HPE_INVALCONFIG  (-20)
#define HPE_NOTRANSPORT  (-21)
#define HPE_DISCONN      (-22)
#define HPE_CONNREFUSED  (-23)
#define HPE_INVALSCRIPTS (-24)
#define HPE_NOTSUP       (-25)
#define HPE_INVALREQ     (-26)
#define HPE_BUFTOOSMALL  (-27)
#define HPE_NOTINIT      (-28)
#define HPE_GATECLOSED   (-29)
#define HPE_OVERFLOW     (-30)

#define HPSUCCESS(ERR) ((ERR) >= 0)
#define HPERROR(ERR)   ((ERR) < 0)


#endif /* __HPERROR_H__ */
