/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_SPINLOCK_H__
#define __HP_SPINLOCK_H__

#if defined(HP_WIN_KM)
  #include <hplib/nt/os_spinlock.h>
#elif defined(HP_WIN_UM)
  #include <hplib/win32/os_spinlock.h>
#elif defined(HP_LINUX_KM)
  #include <hplib/lnx/os_spinlock.h>
#elif defined(HP_OSX_KM)
  #include <hplib/xnu/os_spinlock.h>
#elif defined(HP_OSX_UM)
  #include <hplib/posix/os_spinlock.h>
#endif

int32 hp_SpinlockCreate(hp_spinlock_t *lock);
void hp_SpinlockAcquire(hp_spinlock_t *lock);
logical hp_SpinlockTryAcquire(hp_spinlock_t *lock);
void hp_SpinlockRelease(hp_spinlock_t *lock);
void hp_SpinlockDestroy(hp_spinlock_t *lock);

#endif // HP_SPINLOCK_H__
