#ifndef __HP_BASE64_H__
#define __HP_BASE64_H__

size_t hp_Base64Encode(const char *src, size_t srclength, char *target, size_t targsize);
int hp_Base64Decode(const char *src, char *target, size_t targsize);

#endif // __HP_BASE64_H__
