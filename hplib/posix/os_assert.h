/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_ASSERT_H__
#define __OS_ASSERT_H__

#if !defined(HP_DEBUG)
   #define _Assert(exp) 1
#else
   #define _Assert(exp) assert(exp)
#endif

#endif // __OS_ASSERT_H__
