/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_PROCOPS_H__
#define __OS_PROCOPS_H__

uint32 hp_EnumerateProcesses(uint32 **pids);

#endif // __OS_PROCOPS_H__
