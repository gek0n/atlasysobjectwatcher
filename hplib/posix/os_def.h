/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_DEF_H__
#define __OS_DEF_H__

// TODO: move this to top hplib
typedef struct _UNICODE_STRING {
  uint16 Length;
  uint16 MaximumLength;
  wchar_t *Buffer;
} UNICODE_STRING;
typedef UNICODE_STRING *PUNICODE_STRING;

#define min(a,b) (((a)<(b))?(a):(b))

#endif /* __OS_DEF_H__ */
