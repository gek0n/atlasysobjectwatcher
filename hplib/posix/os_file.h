/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_FILE_H__
#define __OS_FILE_H__

typedef struct hp_file {
   uint64 offset;
   int32 flags;
   FILE *file;
} hp_file_t;

#define HP_PATH_MAX PATH_MAX
#define HP_PATH_SEPARATOR_NATIVE '/'

#endif /* __OS_FILE_H__ */
