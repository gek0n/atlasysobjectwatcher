/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_EVENT_H__
#define __OS_EVENT_H__

struct ev_list_element;

typedef struct {
  sem_t *sem;
  int value;
  pthread_mutex_t mutex;
  hp_event_type_t type;
  logical signalled;
} hp_event_t;

#endif /* __OS_EVENT_H__ */
