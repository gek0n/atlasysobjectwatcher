/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_MUTEX_H__
#define __OS_MUTEX_H__

typedef struct hp_mutex {
   pthread_mutex_t mtx;
} hp_mutex_t;

#endif // __OS_MUTEX_H__
