/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __OS_THREAD_H__
#define __OS_THREAD_H__

#include <hplib/hp_hashtable.h>
#include <hplib/hp_atomic.h>

typedef enum
{
  THREAD_STATE_INITIALIZED = 0,
  THREAD_STATE_RUNNING,
  THREAD_STATE_TERMINATING
} thread_state_t;

typedef struct hp_thread {
  hp_thread_func_t *func;
  void             *ctx;
  hp_event_t        destroyEvent;
  pthread_t         thread;
  hp_atomic32_t     threadState;
#if HP_TWEAK_THREAD_LOGGING_ENABLE
  const char       *funcName;
#endif
} hp_thread_t;

int32 CurrentThreadAddWaitEvent(hp_event_t *evt);
int32 CurrentThreadRemoveWaitEvent(hp_event_t *evt);
logical CurrentThreadIsTerminating();

#endif /* __OS_THREAD_H__ */
