/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_CPU_H__
#define __HP_CPU_H__

#if defined(HP_WIN)

static __inline void cpuid(int32 funcId, int32 leafId, int32 *eax, int32 *ebx, int32 *ecx, int32 *edx)
{
  int32 info[4];
  __cpuidex(info, funcId, leafId);
  if (eax) { *eax = info[0]; }
  if (ebx) { *ebx = info[1]; }
  if (ecx) { *ecx = info[2]; }
  if (edx) { *edx = info[3]; }
}

#define wrmsr(NUM, VAL) __writemsr(NUM, VAL)
#define rdmsr(NUM) __readmsr(NUM)

#endif

typedef int32 (hp_for_each_cpu_t)(void *arg);

uint32 hp_CurrentCpu(void);
void hp_ForEachCpu(hp_for_each_cpu_t *cb, void *arg);
int32 hp_ExecOnCpuWithOthersLocked(uint32 cpuIdx, hp_for_each_cpu_t *cb, void *arg);

#endif // __HP_CPU_H__
