/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HPMALLOC_H__
#define __HPMALLOC_H__

int32 hp_MallocIni(void);
void hp_MallocFin(void);


void* os_Malloc(size_t size, logical exec);
void os_Free(void *ptr, logical exec);

#ifdef HP_ENGAGE_PAGED_POOL
void* os_MallocPaged(size_t size);
void os_FreePaged(void *ptr);
void* os_ReallocPaged(void *ptr, size_t sz);
void os_PagedDumpStats();
#endif


void *hp_MallocExec(size_t size);
void hp_FreeExec(void *ptr);


#if HP_TWEAK_MALLOC_TRACKING_ENABLE

enum
{
  HP_MALLOC_STAT_CURRENT = 0x01,
  HP_MALLOC_STAT_TOTAL = 0x02,
  HP_MALLOC_STAT_PEAK = 0x04,
  HP_MALLOC_STAT_STATS = (HP_MALLOC_STAT_CURRENT | HP_MALLOC_STAT_TOTAL | HP_MALLOC_STAT_PEAK),
  HP_MALLOC_STAT_BLOCKS = 0x08,
  HP_MALLOC_STAT_ALL = (HP_MALLOC_STAT_STATS | HP_MALLOC_STAT_BLOCKS)
};

void hp_MallocDumpStats(int flags);

void* hp_MallocHeap(size_t size, const char *file, int line);
void* hp_ReallocHeap(void *ptr, size_t size, const char *file, int line);

#else

#define hp_MallocDumpStats(f)

void* hp_MallocHeap(size_t size);
void* hp_ReallocHeap(void *ptr, size_t size);

#endif

void hp_FreeHeap(void *ptr);


#if HP_TWEAK_MALLOC_TRACKING_ENABLE
  #if HP_TWEAK_USE_LOCKFREE_MALLOC
    #define hp_Malloc(size) hp_MallocAtomicDbg(size, __FILE__, __LINE__)
    #define hp_Realloc(ptr, size) hp_ReallocAtomicDbg(ptr, size, __FILE__, __LINE__)
    #define hp_Free(ptr) hp_FreeAtomicDbg(ptr, __FILE__, __LINE__)
  #else
    #define hp_Malloc(size) hp_MallocHeap(size, __FILE__, __LINE__)
    #define hp_Realloc(ptr, size) hp_ReallocHeap(ptr, size, __FILE__, __LINE__)
    #define hp_Free(ptr) hp_FreeHeap(ptr)
  #endif
#else
  #if HP_TWEAK_USE_LOCKFREE_MALLOC
    #define hp_Malloc(size) hp_MallocAtomic(size)
    #define hp_Realloc(ptr, size) hp_ReallocAtomic(ptr, size)
    #define hp_Free(ptr) hp_FreeAtomic(ptr)
  #else
    #define hp_Malloc(size) hp_MallocHeap(size)
    #define hp_Realloc(ptr, size) hp_ReallocHeap(ptr, size)
    #define hp_Free(ptr) hp_FreeHeap(ptr)
  #endif
#endif

#endif /* __HPMALLOC_H__ */
