/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __HP_RUNDOWN_H__
#define __HP_RUNDOWN_H__

typedef struct hp_rundown_guard {
  void*  ptr;
} hp_refcnt_t;

void hp_RefcntCreate(hp_refcnt_t *rd);
logical hp_RefcntInc(hp_refcnt_t *rd);
void hp_RefcntDec(hp_refcnt_t *rd);
void hp_RefcntWait(hp_refcnt_t *rd);

#endif /* __HP_RUNDOWN_H__ */
