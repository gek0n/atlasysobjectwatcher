﻿#include <predefine.h>
#include <hplib/hplib.h>
#include <handle_table.h>

// NOTE Candidate to new file os_handle_table_ops.h

void *ObjGetAddr(__in HANDLE_TABLE_ENTRY *handleTableEntry)
{
  OBJECT_HEADER *objHeader;
  objHeader = ObjGetHeader(handleTableEntry);

  return &objHeader->Body;
}

OBJECT_HEADER *ObjGetHeader(__in HANDLE_TABLE_ENTRY *HandleTableEntry)
{
  return CalcObjHeaderPtr(HandleTableEntry->ObjectPointerBits);
}

OBJECT_HEADER *CalcObjHeaderPtr(__in UINT64 bits)
{
  return (OBJECT_HEADER *)((bits << 4) + 0xFFFF000000000000);
}
