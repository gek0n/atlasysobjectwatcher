#ifndef __HANDLERS_H__
#define __HANDLERS_H__

// Hooked functions names

#define CREATE_OBJ_HNDL_PATCH_NAME "ObpCreateHandle"
#define REMOVE_OBJ_PATCH_NAME "ObpRemoveObjectRoutine"

// Hooked functions addrs (for Win8.1 Build 9600)

#define ObpCreateHandle ((void *)0xfffff803f1a42a50)
#define ObpRemoveObjectRoutine ((void *)0xfffff803f1a5cbc8)

int32 CreateObjHndlPreHandler(
        hp_patch_t  *patch,
        hp_hashtable_t **trackedTypes,
        hp_exectx_t *regs);

int32 RemoveObjPreHandler(
        hp_patch_t  *patch,
        hp_hashtable_t **trackedTypes,
        hp_exectx_t *regs);

#endif  // __HANDLERS_H__
