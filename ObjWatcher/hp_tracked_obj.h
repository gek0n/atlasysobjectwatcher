﻿#ifndef __HP_TRACKED_OBJ_H__
#define __HP_TRACKED_OBJ_H__

typedef struct hp_tracked_obj {
  void *addr;
  char *fullname;
} hp_tracked_obj_t;

int CompareKeyForTrackedObj(
      const void *LeftKey,
      const void *RightKey,
      void *Ctx);

void* GetKeyFromTrackedObj(
        const hp_tracked_obj_t *trackedObj,
        void *Ctx);

unsigned int GetHashForTrackedObj(const void *Key, void *Ctx);

#endif  // __HP_TRACKED_OBJ_H__
