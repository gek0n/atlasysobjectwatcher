﻿#include <predefine.h>
#include <hplib/hplib.h>
#include <hp_objsearcher.h>
#include <helpers.h>
#include <hp_tracked_type.h>
#include <hp_tracked_obj.h>
#include <handle_table.h>

static hp_iter_ret_t AddObjToHashTableFromDir(
                       const char *root,
                       char *name,
                       char *type,
                       hp_tracked_type_t *trackedType);

static BOOLEAN AddHandleToHashtable(
                 HANDLE_TABLE *HandleTable,
                 HANDLE_TABLE_ENTRY *HandleTableEntry,
                 void *Handle,
                 hp_tracked_type_t *trackedType);

static uint32 ConstructCharPath(
                const char *root,
                const char *name,
                char *path,
                size_t pathLen);

static void* DriverIterator(hp_tracked_obj_t *trackedObj, hp_hashtable_t *hashtable);
static void* ProcessIterator(hp_tracked_obj_t *trackedObj, void *ctx);

int32 SearchInDirs(const char *typeName, hp_hashtable_t **found)
{
  *found = NULL;
  hp_hashtable_t *hashtable;

  hashtable = hp_HashTableCreate(
                GetKeyFromTrackedObj,
                CompareKeyForTrackedObj,
                GetHashForTrackedObj,
                sizeof(hp_tracked_obj_t),
                sizeof(hp_tracked_obj_t) *
                  DEFAULT_OBJS_COUNT,
                0,
                NULL);

  if (!hashtable) {
    HpLogErr(
      "Can't create hashtable for found objects ot type %s\n",
      typeName);
    return HPE_FAILURE;
  }

  int32 status;
  hp_tracked_type_t trackedType;
  trackedType.hashtable = hashtable;
  trackedType.typeName = typeName;
  status = hp_ObjDirectoryIterate("\\", AddObjToHashTableFromDir, &trackedType);

  *found = hashtable;

  return status;
}

int32 SearchProcs(hp_hashtable_t **found)
{
  *found = NULL;
  hp_hashtable_t *hashtable;

  hashtable = hp_HashTableCreate(
                GetKeyFromTrackedObj,
                CompareKeyForTrackedObj,
                GetHashForTrackedObj,
                sizeof(hp_tracked_obj_t),
                sizeof(hp_tracked_obj_t) *
                  DEFAULT_OBJS_COUNT,
                0,
                NULL);

  if (!hashtable) {
    HpLogErr("Can't create hashtable for found processes\n");
    return HPE_FAILURE;
  }

  int32 status;
  uint32 *pids;
  status = hp_EnumerateProcesses(&pids);
  if (HPERROR(status)) {
    HpLogErr("Can't enumerate processes\n");
    hp_HashTableFree(hashtable);
    return status;
  }

  int32 pid = *++pids;
  while (pid > 0) { // NOT CLEAR: Не корректно?
    void *procAddr;
    procAddr = hp_ProcObjGetByPid(pid);
    if (!procAddr) {
      HpLogErr("Can't get proc obj by pid %d\n", pid);
      hp_HashTableFree(hashtable);
      return HPE_FAILURE;
    }

    char *imageName;
    imageName = hp_ObjGetName(procAddr, TRUE);
    if (!imageName) {
      HpLogErr("Can't get proc name by addr %#p\n", procAddr);
      hp_ObjDeref(procAddr);
      hp_HashTableFree(hashtable);
      return status;
    }

    status = TryInsertObjToHashtable(hashtable, procAddr, imageName);
    if (HPERROR(status)) {
      HpLogErr("Can't add process %#p to hashtable\n", procAddr);
    }

    hp_StrFree(imageName);
    hp_ObjDeref(procAddr);

    pid = *++pids;
  }

  *found = hashtable;

  return HPE_SUCCESS;
}

int32 SearchDevices(hp_hashtable_t *drivers, hp_hashtable_t **found)
{
  *found = NULL;
  hp_hashtable_t *hashtable;

  hashtable = hp_HashTableCreate(
                GetKeyFromTrackedObj,
                CompareKeyForTrackedObj,
                GetHashForTrackedObj,
                sizeof(hp_tracked_obj_t),
                sizeof(hp_tracked_obj_t) *
                  DEFAULT_OBJS_COUNT,
                0,
                NULL);

  if (!hashtable) {
    HpLogErr("Can't create hashtable for found devices\n");
    return HPE_FAILURE;
  }

  hp_HashTableIterate(drivers, DriverIterator, hashtable);

  *found = hashtable;

  return HPE_SUCCESS;
}

int32 SearchInProcsHandles(
        __in hp_hashtable_t *procs,
        __in const char *typeName,
        __out hp_hashtable_t **objs)
{
  *objs = NULL;
  hp_hashtable_t *hashtable;

  hashtable = hp_HashTableCreate(
                GetKeyFromTrackedObj,
                CompareKeyForTrackedObj,
                GetHashForTrackedObj,
                sizeof(hp_tracked_obj_t),
                sizeof(hp_tracked_obj_t) *
                  DEFAULT_OBJS_COUNT,
                0,
                NULL);

  if (!hashtable) {
    HpLogErr("Can't create hashtable for found %s\n", typeName);
    return HPE_FAILURE;
  }

  hp_tracked_type_t trackedType;
  trackedType.hashtable = hashtable;
  trackedType.typeName = typeName;

  hp_HashTableIterate(procs, ProcessIterator, &trackedType);

  *objs = hashtable;

  return HPE_SUCCESS;
}

static void* ProcessIterator(hp_tracked_obj_t *trackedObj, void *ctx)
{
  PEPROCESS proc;
  proc = trackedObj->addr;

  PHANDLE_TABLE handleTable;
  handleTable = ObReferenceProcessHandleTable(proc);  // SMELLS: Плохо! Обращение по адресу
  if (!handleTable) {
    HpLogErr("Can't get process %#p handle table\n", proc);
    return NULL;
  }

  PHANDLE retHandle = NULL;
  BOOLEAN res;
  res = ExEnumHandleTable(handleTable, AddHandleToHashtable, ctx, retHandle);
  if (res) {
    HpLogDbg("Iterate was stop at %p handle\n", retHandle);
  }

  if (handleTable) {
    ObDereferenceProcessHandleTable(proc);  // SMELLS: Плохо! Обращение по адресу
  }

  return NULL;
}

static BOOLEAN AddHandleToHashtable(
                 __in HANDLE_TABLE *handleTable,
                 __inout HANDLE_TABLE_ENTRY *handleTableEntry,
                 __in void *handle,
                 __in hp_tracked_type_t *trackedType)
{
  HP_UNUSED_PARAM(handle);

  void *obj;
  obj = ObjGetAddr(handleTableEntry);
  
  hp_ObjRef(obj);

  if (IsObjOfLookedType(obj, trackedType->typeName)) {
    char *objName;
    objName = hp_ObjGetName(obj, TRUE);
    if (!objName) {
      HpLogDbg("Can't get name for obj %#p\n", obj);
    }

    int32 status;
    status = TryInsertObjToHashtable(trackedType->hashtable, obj, objName);

    if (objName) {
      hp_StrFree(objName);
    }

    if (HPERROR(status)) {
      HpLogErr("Can't add obj %#p to hashtable\n", obj);
    }
  }
  InterlockedExchangeAdd8(&handleTableEntry->VolatileLowValue, 1);
  if (handleTable->HandleContentionEvent.Value) {
    ExfUnblockPushLock((int64)&handleTable->HandleContentionEvent.Value, 0);
  }

  hp_ObjDeref(obj);
  return FALSE;
}

static void* DriverIterator(hp_tracked_obj_t *trackedObj, hp_hashtable_t *hashtable)
{
  DRIVER_OBJECT *drv;
  drv = trackedObj->addr;

  DEVICE_OBJECT *dev;
  dev = drv->DeviceObject;


  while (dev) {
    char *devName;
    devName = hp_ObjGetName(dev, TRUE);
    if (!devName) {
      HpLogDbg("Can't get name for device %#p\n", dev);
    }

    int32 status;
    status = TryInsertObjToHashtable(hashtable, dev, devName);

    if (devName) {
      hp_StrFree(devName);
    }

    if (HPERROR(status)) {
      break;
    }

    dev = dev->NextDevice;
  }

  return NULL;
}

static hp_iter_ret_t AddObjToHashTableFromDir(
                       const char *root,
                       char *name,
                       char *type,
                       hp_tracked_type_t *trackedType)
{
  int32 len;
  char path[DEFAULT_TYPE_NAME_SIZE] = { 0 };
  len = ConstructCharPath(
          root,
          name,
          path,
          sizeof(char) * DEFAULT_TYPE_NAME_SIZE);

  if (!len) {
    HpLogErr("Can't construct path\n");
    return HP_ITER_RET_FAIL;
  }

  int32 status;
  if (strcmp(type, "Directory") == 0) {
    status = hp_ObjDirectoryIterate(
               path,
               AddObjToHashTableFromDir,
               trackedType);

    if (HPERROR(status)) {
      return HP_ITER_RET_FAIL;
    }
  }

  if (strcmp(type, trackedType->typeName) == 0) {
    void *obj = NULL;
    status = ObjGetByName(path, type, &obj);
    if (HPERROR(status)) {
      return HP_ITER_RET_FAIL;
    }

    status = TryInsertObjToHashtable(trackedType->hashtable, obj, path);
    if (HPERROR(status)) {
      hp_ObjDeref(obj);
      return HP_ITER_RET_FAIL;
    }

    hp_ObjDeref(obj);
  }
  return HP_ITER_RET_CONTINUE;
}

static uint32 ConstructCharPath(
                const char *root,
                const char *name,
                char *path,
                size_t pathLen)
{
  if (strcmp(root, "\\") == 0) {
    return hp_Snprintf(
             path,
             pathLen,
             "\\%s",
             name);
  }
  return hp_Snprintf(
           path,
           pathLen,
           "%s\\%s",
           root,
           name);
}
