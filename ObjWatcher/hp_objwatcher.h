#ifndef __HP_OBJWATCHER_H__
#define __HP_OBJWATCHER_H__

// Inner struct, which describe installed track
typedef struct hp_tracked_type hp_tracked_type_t;

/*
  Routine Description:

    Initialize inner data structures. Call before using module

  Arguments:

    None

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
 */
int32 hp_ObjWatcherIni(void);

/*
  Routine Description:

    Free memory, used for inner structures. Call it after using module

  Arguments:

    None

  Return Value:

    None
*/
void hp_ObjWatcherFin(void);

/*
  Routine Description:

    Track lifecycle objects of specified type

  Arguments:

    typeName - object type name for tracking
    track - return created struct for managing tracking

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
*/
int32 hp_ObjWatcherTrackType(
        __in const char *typeName,
        __out hp_tracked_type_t **track);

/*
  Routine Description:

    Remove object type from tracked list

  Arguments:

    track - structure returned by hp_ObjWatcherTrack() function

  Return Value:

    None
*/
void hp_ObjWatcherUntrackType(__in hp_tracked_type_t *track);

/*
  Routine Description:

    Remove all object types from tracked list

  Arguments:

    None

  Return Value:

    None
*/
void hp_ObjWatcherUntrackAllTypes(void);

/*
  Routine Description:

    Get name for specifyied object address

  Arguments:

    obj - address object which name is looked up
    name - returned name for object

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
*/
int32 hp_ObjWatcherGetObjName(__in void *obj, __out char **name);

/*
  Routine Description:

    Get objects count of specifyied object type.
    Need use hp_StrFree() after successful call to free returned string

Arguments:

    typeName - name of object type, which objects will be counting

Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
*/
int32 hp_ObjWatcherCountObjsByName(__in const char *typeName);

/*
  Routine Description:

    Get objects count from tracked struct

  Arguments:

    track - structure returned by hp_ObjWatcherTrack() function

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
*/
int32 hp_ObjWatcherCountObjs(__in hp_tracked_type_t *track);

#endif // __HP_OBJWATCHER_H__
