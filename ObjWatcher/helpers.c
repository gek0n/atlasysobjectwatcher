#include <predefine.h>
#include <hplib/hplib.h>
#include <hp_tracked_obj.h>
#include <helpers.h>

static PUNICODE_STRING Utf8ToUStrAlloc(
                         __in const char *strUtf8,
                         __in const size_t len);

static POBJECT_TYPE GetObjTypeByName(const char *name);
static char *ObjGetTypeStrWithoutRef(void *obj);

// NOTE Candidate to os_objops.h
// Ref object by name and type name
// Need use hp_ObjDeref() after using
int32 ObjGetByName(
        __in const char *fullName,
        __in const char *typeName,
        __out void **retAddr)
{
  if (!fullName || !typeName || !retAddr) {
    HpLogErr("One or more of parameters is NULL\n");
    return HPE_FAILURE;
  }

  PUNICODE_STRING objPath;
  objPath = Utf8ToUStrAlloc(fullName, strlen(fullName));
  if (!objPath) {
    HpLogErr("Can't convert %s to unicode string\n", fullName);
    return HPE_FAILURE;
  }

  POBJECT_TYPE objType;
  objType = GetObjTypeByName(typeName);
  if (!objType) {
    HpLogErr("Can't ref obj type %s\n", typeName);
    return HPE_NOTFOUND;
  }

  NTSTATUS status;
  status = ObReferenceObjectByName(
             objPath,
             0,
             NULL,
             0,
             objType,
             KernelMode,
             NULL,
             retAddr);

  if (!NT_SUCCESS(status)) {
    HpLogErr("Can't reference object %wZ with status %#x\n", objPath, status);
    hp_FreeAtomic(objPath);
    ObDereferenceObject(objType);
    return HPE_NOTFOUND;
  }

  hp_FreeAtomic(objPath);
  ObDereferenceObject(objType);
  return HPE_SUCCESS;
}

int32 TryInsertObjToHashtable(
        hp_hashtable_t *hashtable,
        void *objAddr,
        const char *objName)
{
  hp_tracked_obj_t *found;
  found = hp_HashTableLookup(hashtable, objAddr, NULL);
  if (found) {
    HpLogDbg("Obj %#p already in the hashtable\n", objAddr);
    return HPE_ALREADY;
  }

  hp_tracked_obj_t trackedObj;
  trackedObj.addr = objAddr;
  trackedObj.fullname = hp_StrDup(objName);

  void *addr;
  addr = hp_HashTableInsert(hashtable, &trackedObj);
  if (!addr) {
    HpLogErr("Can't insert obj %#p to hashtable\n", objAddr);
    return HPE_FAILURE;
  }

  return HPE_SUCCESS;
}

int32 TryRemoveObjFromHashtable(
        hp_hashtable_t *hashtable,
        void *objAddr)
{
  hp_tracked_obj_t *found;
  found = hp_HashTableLookup(hashtable, objAddr, NULL);
  if (!found) {
    HpLogErr("Obj %#p not in the hashtable\n", objAddr);
    return HPE_NOTFOUND;
  }

  int32 status;
  status = hp_HashTableDeleteByKey(hashtable, objAddr);
  if (HPERROR(status)) {
    HpLogErr("Can't remove obj %#p from hashtable\n", objAddr);
    return status;
  }
  return HPE_SUCCESS;
}

logical IsObjOfLookedType(void *obj, const char *expectedTypeName)
{
  char *typeName = ObjGetTypeStrWithoutRef(obj);
  if (!typeName) {
    HpLogErr("Can't get obj %#p type name\n", obj);
    return FALSE;
  }

  int32 diff;
  diff = strcmp(typeName, expectedTypeName);
  hp_StrFree(typeName);
  return diff == 0;
}

// NOTE Candidate to os_objops.h
// Return character string if succeed, NULL otherwise
// Need use hp_StrFree() for free returned string
// For using in hooks, coz can't reference object in hook 
// (likewise hp_ObjGetTypeStr() works)
static char *ObjGetTypeStrWithoutRef(void *obj)
{
  OBJECT_TYPE *objType;
  objType = ObGetObjectType(obj);
  if (!objType) {
    HpLogErr("Can't get object %#p type\n", obj);
    return NULL;
  }

  return hp_UStrToUtf8Alloc(&objType->Name);
}

// NOTE Candidate to os_objops.h
// Return object type pointer if succed, NULL otherwise
static POBJECT_TYPE GetObjTypeByName(const char *name)
{
  int32 len;
  char path[256];
  len = hp_Snprintf(path, 256 * sizeof(char), "\\ObjectTypes\\%s", name);
  if (!len) {
    HpLogErr("Can't concat path\n");
    return NULL;
  }

  PUNICODE_STRING typePath;
  typePath = Utf8ToUStrAlloc(path, len);
  if (!typePath) {
    HpLogErr("Can't construct type path\n");
    return NULL;
  }

  POBJECT_TYPE ObpTypeObjectType;
  ObpTypeObjectType = hp_ObjGetType(*IoFileObjectType);
  if (!ObpTypeObjectType) {
    HpLogErr("Can't get object Type\n");
    return NULL;
  }

  NTSTATUS status;
  void *objType;
  status = ObReferenceObjectByName(
             typePath,
             0,
             NULL,
             0,
             ObpTypeObjectType,
             KernelMode,
             NULL,
             &objType);

  if (!NT_SUCCESS(status)) {
    HpLogErr(
      "Can't reference object type %wZ with status %#x\n",
      typePath,
      status);
    hp_FreeAtomic(typePath);
    return NULL;
  }

  hp_FreeAtomic(typePath);
  return objType;
}

// NOTE Candidate to os_strops.h
// Use hp_Free() after successful call
static PUNICODE_STRING Utf8ToUStrAlloc(
                         __in const char *strUtf8,
                         __in const size_t len)
{
  PUNICODE_STRING UStr = NULL;

  wchar_t *strUtf16;
  strUtf16 = hp_Utf8ToUtf16Alloc(strUtf8, len);
  if (strUtf16) {
    UStr = hp_Malloc(sizeof(UNICODE_STRING));
    if (UStr) RtlInitUnicodeString(UStr, strUtf16);
  }

  hp_FreeAtomic(strUtf16);
  return UStr;
}