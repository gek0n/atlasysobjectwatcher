#include <predefine.h>
#include <hplib/hplib.h>
#include <hp_tracked_obj.h>

void* GetKeyFromTrackedObj(const hp_tracked_obj_t *trackedObj, void *Ctx)
{
  return trackedObj->addr;
}

unsigned int GetHashForTrackedObj(const void *Key, void *Ctx)
{
  return (unsigned int)Key;
}

int CompareKeyForTrackedObj(
      const void *LeftKey,
      const void *RightKey,
      void *Ctx)
{
  return (uintptr_t)LeftKey == (uintptr_t)RightKey;
}
