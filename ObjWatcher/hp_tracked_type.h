﻿#ifndef __HP_TRACKED_TYPE_H__
#define __HP_TRACKED_TYPE_H__

#define DEFAULT_TYPE_NAME_SIZE 256

typedef struct hp_tracked_type {
  hp_hashtable_t *hashtable;
  char *typeName;
} hp_tracked_type_t;

unsigned int GetHashForTrackedType(const char *Key, void *Ctx);

void* GetKeyFromTrackedType(const hp_tracked_type_t *trackedType, void *Ctx);

int CompareKeyForTrackedType(
      const char *LeftKey,
      const char *RightKey,
      void *Ctx);

#endif  // __HP_TRACKED_TYPE_H__
