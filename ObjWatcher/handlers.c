#include <predefine.h>
#include <hplib/hplib.h>
#include <patchlib/patchlib.h>
#include <helpers.h>
#include <handlers.h>
#include <hp_tracked_type.h>

typedef struct _posthandler_arg{
  hp_hashtable_t *trackedTypes;
  void *obj;
} posthandler_arg;

static void *AddTrackedObjTypesIterator(hp_tracked_type_t *trackedType, void *obj)
{
  if (!IsObjOfLookedType(obj, trackedType->typeName)) {
    return NULL;
  }

  char *objName;
  objName = hp_ObjGetName(obj, FALSE);
  if (!objName) {
    HpLogDbg("Can't get obj name for addr %#p\n", obj);
  }

  int32 status;
  status = TryInsertObjToHashtable(trackedType->hashtable, obj, objName);
  if (HPERROR(status)) {
    HpLogErr("Can't add %#p to hashtable\n", obj);
    hp_StrFree(objName);
    return NULL;
  }

  hp_StrFree(objName);

#ifdef DBG
  OBJECT_TYPE *objType;
  objType = ObGetObjectType(obj);
  if (!objType) {
    return NULL;
  }
  
  HpLogDbg(
    "[ADD] In %s table: %u\tObjs: %u\tHndls: %u\n",
    trackedType->typeName,
    hp_HashTableNumElements(trackedType->hashtable),
    objType->TotalNumberOfObjects,
    objType->TotalNumberOfHandles);
#endif

  return NULL;
}

static void *DelTrackedObjTypesIterator(hp_tracked_type_t *trackedType, void *obj)
{
  if (!IsObjOfLookedType(obj, trackedType->typeName)) {
    return NULL;
  }

  int32 status;
  status = TryRemoveObjFromHashtable(trackedType->hashtable, obj);
  if (HPERROR(status)) {
    return NULL;
  }

#ifdef DBG
  OBJECT_TYPE *objType;
  objType = ObGetObjectType(obj);
  if (!objType) {
    return NULL;
  }
  
  HpLogDbg(
    "[DEL] In %s table: %u\tObjs: %u\tHndls: %u\n",
    trackedType->typeName,
    hp_HashTableNumElements(trackedType->hashtable),
    objType->TotalNumberOfObjects,
    objType->TotalNumberOfHandles);
#endif

  return NULL;
}

static void CreateObjHndlPostHandler(
              hp_patch_t  *patch,
              posthandler_arg *postArgs,
              hp_exectx_t *regs)
{
  NTSTATUS status;
  status = regs->rax;
  if (NT_SUCCESS(status)) {
    void *obj;
    obj = postArgs->obj;

    if (hp_HashTableNumElements(postArgs->trackedTypes)) {
      hp_HashTableIterate(
        postArgs->trackedTypes,
        AddTrackedObjTypesIterator,
        obj);
    }
  }

  ExFreePoolWithTag(postArgs, 'tsoP');
}

int32 CreateObjHndlPreHandler(
        hp_patch_t  *patch,
        hp_hashtable_t **trackedTypes,
        hp_exectx_t *regs)
{
  void **obj;
  obj = (void **)hp_GetParam(2, regs, HP_CALLCONV_FASTCALL);

  posthandler_arg *postArgs;
  postArgs = ExAllocatePoolWithTag(NonPagedPool, sizeof(posthandler_arg), 'tsoP');
  if (!postArgs) {
    HpLogErr("Can't allocate memory for post args\n");
    return HPE_FAILURE;
  }

  postArgs->obj = obj;
  postArgs->trackedTypes = *trackedTypes;
  hp_PatchRegisterPostHandler(patch, CreateObjHndlPostHandler, postArgs, regs);

  return HPE_SUCCESS;
}

int32 RemoveObjPreHandler(
        hp_patch_t  *patch,
        hp_hashtable_t **trackedTypes,
        hp_exectx_t *regs)
{
  OBJECT_HEADER *objHeader;
  objHeader = (OBJECT_HEADER *)hp_GetParam(1, regs, HP_CALLCONV_FASTCALL);

  void *obj;
  obj = &objHeader->Body;

  if (hp_HashTableNumElements(*trackedTypes)) {
    hp_HashTableIterate(*trackedTypes, DelTrackedObjTypesIterator, obj);
  }

  return HPE_SUCCESS;
}
