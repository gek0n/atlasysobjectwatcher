#include <predefine.h>
#include <hplib/hplib.h>
#include <patchlib/patchlib.h>
#include <hp_objwatcher.h>
#include <handlers.h>
#include <hp_objsearcher.h>
#include <hp_tracked_type.h>
#include <hp_tracked_obj.h>

static int32 SetPatches(void);
static void RemovePatches(void);
static logical IsTrackable(const char *typeName);
static void *UntrackIterator(hp_tracked_type_t *trackedType, void *obj);
static void *PrintIterator(hp_tracked_obj_t *objTrack, void *ctx);
static void *FreeObjNamesIterator(hp_tracked_obj_t *objTrack, void *ctx);

static int32 SetUpPatch(
               void *patchedAddr,
               hp_patch_pre_handler_t *prehandler,
               void *preHandlerArgs,
               const char *patchName,
               hp_patch_t **patch);

static const char* trackableObjTypes[] = {
  "Driver",
  "Device",
  "Process",
  "Event",
  "Mutant",
  "Semaphore",
  "Timer",
  "File",
  "Key",
  0
};

struct {
  hp_patch_t *createObjHndlPatch;
  hp_patch_t *removeObjPatch;
  hp_hashtable_t *trackedTypes;
} gData = { 0 };

int32 hp_ObjWatcherIni(void)
{
  /*
  * NOTE: ����� ������ �������� ������� ���������� ���-������
  */
  gData.trackedTypes = hp_HashTableCreate(
                            GetKeyFromTrackedType,
                            CompareKeyForTrackedType,
                            GetHashForTrackedType,
                            sizeof(struct hp_tracked_type),
                            sizeof(struct hp_tracked_type) * 10,
                            0,
                            NULL);
  if (!gData.trackedTypes) {
    HpLogErr("Can't create hashtable for tracked objects\n");
    return HPE_FAILURE;
  }

  int32 status;
  status = SetPatches();
  if (HPERROR(status)) {
    HpLogErr("Can't set patches\n");
    hp_HashTableFree(gData.trackedTypes);
    return status;
  }

  return HPE_SUCCESS;
}

void hp_ObjWatcherFin(void)
{
  RemovePatches();
  hp_ObjWatcherUntrackAllTypes();
  hp_HashTableFree(gData.trackedTypes);
}

int32 hp_ObjWatcherTrackType(
        __in const char *typeName,
        __out hp_tracked_type_t **track)
{
  if (!IsTrackable(typeName)) {
    HpLogErr("Type %s is not trackable\n", typeName);
    return HPE_NOTFOUND;
  }

  hp_tracked_type_t *newTrack;
  newTrack = ExAllocatePoolWithTag(
               NonPagedPool,
               sizeof(hp_tracked_type_t),
               'kcrT');

  if (!newTrack) {
    HpLogErr("Can't allocate memory for hp_objtrack_t\n");
    return HPE_FAILURE;
  }
  
  newTrack->typeName = hp_MallocAtomic(sizeof(char) * DEFAULT_TYPE_NAME_SIZE);

  if (!newTrack->typeName) {
    HpLogErr("Can't allocate memory for typeName\n");
    ExFreePoolWithTag(newTrack, 'kcrT');
    return HPE_FAILURE;
  }

  newTrack->typeName = hp_StrDup(typeName);
  if (!newTrack->typeName) {
    HpLogErr("Can't copy typename %s\n", typeName);
    hp_FreeAtomic(newTrack->typeName);
    ExFreePoolWithTag(newTrack, 'kcrT');
    return HPE_FAILURE;
  }

  int32 status;
  if (strcmp(typeName, "Driver") == 0) {
    status = SearchInDirs(typeName, &newTrack->hashtable);
  }
  else if(strcmp(typeName, "Process") == 0) {
    status = SearchProcs(&newTrack->hashtable);
  }
  else if (strcmp(typeName, "Device") == 0) {
    hp_tracked_type_t *drivers;

    drivers = hp_HashTableLookup(gData.trackedTypes, "Driver", NULL);
    if (!drivers) {
      HpLogErr("Drivers not tracked yet\n");
      hp_FreeAtomic(newTrack->typeName);
      ExFreePoolWithTag(newTrack, 'kcrT');
      return HPE_NOTFOUND;
    }

    status = SearchDevices(drivers->hashtable, &newTrack->hashtable);
  }
  else {
    hp_tracked_type_t *processes;
    processes = hp_HashTableLookup(gData.trackedTypes, "Process", NULL);
    if (!processes) {
      HpLogErr("Processes not tracked yet\n");
      hp_FreeAtomic(newTrack->typeName);
      ExFreePoolWithTag(newTrack, 'kcrT');
      return HPE_NOTFOUND;
    }

    status = SearchInProcsHandles(
               processes->hashtable,
               newTrack->typeName,
               &newTrack->hashtable);
  }

  if (HPERROR(status)) {
    HpLogErr("Can't fill hashtable for %s\n", typeName);
    hp_FreeAtomic(newTrack->typeName);
    ExFreePoolWithTag(newTrack, 'kcrT');
    return status;
  }

  void *addr;
  addr = hp_HashTableInsert(gData.trackedTypes, newTrack);
  if (!addr) {
    HpLogErr("Can't add object %s to tracked objects\n", newTrack->typeName);
    hp_HashTableFree(newTrack->hashtable);
    hp_FreeAtomic(newTrack->typeName);
    ExFreePoolWithTag(newTrack, 'kcrT');
    return HPE_FAILURE;
  }

  *track = newTrack;
  return HPE_SUCCESS;
}

void hp_ObjWatcherUntrackType(__in hp_tracked_type_t *track)
{
  hp_tracked_type_t *removed;
  removed = hp_HashTableRemoveByKey(gData.trackedTypes, track->typeName);
  if (!removed) {
    HpLogDbg("Can't remove %s entry from trackedObjTypes\n", track->typeName);
    return;
  }

#ifdef DBG
  HpLogDbg(
    "\nHASHTABLE: %s ITEMS: %d\n",
    removed->typeName,
    hp_HashTableNumElements(removed->hashtable));

  hp_HashTableIterate(removed->hashtable, PrintIterator, NULL);
#endif
  hp_HashTableIterate(removed->hashtable, FreeObjNamesIterator, NULL);

  hp_FreeAtomic(removed->typeName);
  hp_HashTableFree(removed->hashtable);
  hp_HashTableFreeItem(gData.trackedTypes, removed);
}

void hp_ObjWatcherUntrackAllTypes(void)
{
  hp_HashTableIterate(gData.trackedTypes, UntrackIterator, NULL);
}

int32 hp_ObjWatcherGetObjName(__in void *obj, __out char **name)
{
  *name = NULL;
  
  char *typeName;
  typeName = hp_ObjGetTypeStr(obj);
  if (!typeName) {
    HpLogErr("Can't get type str for %#p obj\n", obj);
    return HPE_FAILURE;
  }

  hp_tracked_type_t *trackedType;
  trackedType = hp_HashTableLookup(gData.trackedTypes, typeName, NULL);
  if (!trackedType) {
    HpLogErr("Objects type %s is untracked\n", typeName);
    hp_StrFree(typeName);
    return HPE_NOTFOUND;
  }

  hp_StrFree(typeName);

  hp_tracked_obj_t *trackedObj;
  trackedObj = hp_HashTableLookup(trackedType->hashtable, obj, NULL);
  if (!trackedObj) {
    HpLogErr("Object %#p of type %s is untracked\n", obj, typeName);
    return HPE_NOTFOUND;
  }

  *name = hp_StrDup(trackedObj->fullname);
  return HPE_SUCCESS;
}

int32 hp_ObjWatcherCountObjsByName(__in const char *typeName)
{
  hp_tracked_type_t *trackedType;
  trackedType = hp_HashTableLookup(gData.trackedTypes, typeName, NULL);
  if (!trackedType) {
    HpLogErr("%s not tracked yet\n", typeName);
    return HPE_NOTFOUND;
  }
  return hp_HashTableNumElements(trackedType->hashtable);
}

int32 hp_ObjWatcherCountObjs(__in hp_tracked_type_t *track)
{
  return hp_HashTableNumElements(track->hashtable);
}

static logical IsTrackable(const char *typeName)
{
  int8 index = 0;
  while (trackableObjTypes[index]) {
    if (strcmp(typeName, trackableObjTypes[index]) == 0) {
      return TRUE;
    }
    index++;
  }
  return FALSE;
}

static int32 SetPatches(void)
{
  int32 status;
  status = SetUpPatch(
             ObpCreateHandle,  // SMELLS: �����! ��������� �� ������
             CreateObjHndlPreHandler,
             &gData.trackedTypes,
             CREATE_OBJ_HNDL_PATCH_NAME,
             &gData.createObjHndlPatch);

  if (HPERROR(status)) return status;

  status = SetUpPatch(
             ObpRemoveObjectRoutine,  // SMELLS: �����! ��������� �� ������
             RemoveObjPreHandler,
             &gData.trackedTypes,
             REMOVE_OBJ_PATCH_NAME,
             &gData.removeObjPatch);

  return status;
}

static int32 SetUpPatch(
               void *patchedAddr,
               hp_patch_pre_handler_t *prehandler,
               void *preHandlerArgs,
               const char *patchName,
               hp_patch_t **patch)
{
  int32 status;
  status = hp_PatchCreate(
             patchedAddr,
             prehandler,
             preHandlerArgs,
             patchName,
             patch);

  if (HPERROR(status)) {
    HpLogErr(
      "Can't create patch %s with error: %#X\n",
      patchName,
      status);
    return status;
  }

  status = hp_PatchEnable(*patch);
  if (HPERROR(status)) {
    HpLogErr(
      "Can't enable patch %s with error: %#X\n",
      patchName,
      status);
    return status;
  }
  return HPE_SUCCESS;
}
  
static void RemovePatches(void)
{
  hp_PatchDestroy(gData.createObjHndlPatch);
  hp_PatchDestroy(gData.removeObjPatch);
}

static void *UntrackIterator(hp_tracked_type_t *trackedType, void *ctx)
{
  hp_ObjWatcherUntrackType(trackedType);
  return NULL;
}

static void *PrintIterator(hp_tracked_obj_t *trackedObj, void *ctx)
{
  HpLogDbg("Object: %#p Name: %s\n", trackedObj->addr, trackedObj->fullname);
  return NULL;
}

static void *FreeObjNamesIterator(hp_tracked_obj_t *trackedObj, void *ctx)
{
  if (trackedObj->fullname) {
    hp_StrFree(trackedObj->fullname);
  }
  return NULL;
}
