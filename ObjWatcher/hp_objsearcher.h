﻿#ifndef __HP_OBJSEARCHER_H__
#define __HP_OBJSEARCHER_H__

#define DEFAULT_OBJS_COUNT 100

/*
  Routine Description:

    Iterate over all directories and find named objects of given type

  Arguments:

    typeName - name of desired type
    found - hashtable with all found objects of given type (key is addr)

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
 */
int32 SearchInDirs(__in const char *typeName, __out hp_hashtable_t **found);

/*
  Routine Description:

    Iterate over all procs in system and store them in hashtable

  Arguments:

    procs - hashtable with all found process type objects (key is addr)

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
 */
int32 SearchProcs(__out hp_hashtable_t **procs);

/*
  Routine Description:

    Iterate over given hashtable with drivers, extract devices from them, and 
    put devices to hashtable

  Arguments:

    drivers - hashtable with drivers to iterate over
    devices - hashtable with all found device objects (key is addr)

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
 */
int32 SearchDevices(
        __in hp_hashtable_t *drivers,
        __out hp_hashtable_t **devices);

/*
  Routine Description:

    Iterate over given hashtable with processes, extract objects of given type,
    and put them to hashtable

  Arguments:

    procs - hashtable with processes to iterate over
    typeName - name of desired type
    objs - hashtable with all found objects of given type (key is addr)

  Return Value:

    Error code. Checks with HPERROR() or HPSUCCESS()
 */
int32 SearchInProcsHandles(
        __in hp_hashtable_t *procs,
        __in const char *typeName,
        __out hp_hashtable_t **objs);

#endif  // __HP_OBJSEARCHER_H__
