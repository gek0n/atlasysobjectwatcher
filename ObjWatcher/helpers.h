#ifndef __HELPERS_H__
#define __HELPERS_H_

int32 ObjGetByName(
        __in const char *fullName,
        __in const char *typeName,
        __out void** retAddr);

int32 TryInsertObjToHashtable(
        hp_hashtable_t *hashtable,
        void *objAddr,
        const char *objName);

int32 TryRemoveObjFromHashtable(
        hp_hashtable_t *hashtable,
        void *objAddr);

logical IsObjOfLookedType(void *obj, const char *expectedTypeName);

#endif  // __HELPERS_H__
