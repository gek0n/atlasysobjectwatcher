#ifndef __HANDLE_TABLE_H__
#define __HANDLE_TABLE_H__

// NOTE Candidate to new file os_handle_table_ops.h

typedef struct _HANDLE_TABLE {
  char SomeInfo[48];
  struct _EX_PUSH_LOCK HandleContentionEvent;
  char SomeInfo2[74];
}HANDLE_TABLE, *PHANDLE_TABLE;

typedef struct _HANDLE_TABLE_ENTRY {
  union {
    INT64 VolatileLowValue;
    INT64 LowValue;
    void *InfoTable;
    struct {
      UINT64 Unlocked : 1;
      UINT64 RefCnt : 16;
      UINT64 Attributes : 3;
      UINT64 ObjectPointerBits : 44;
    };
  };
  union {
    INT64 HighValue;
    void *NextFreeHandleEntry;
    INT64 LeafHandleValue;
    struct {
      INT32 GrantedAccessBits : 25;
      INT32 NoRightsUpgrade : 1;
      INT32 Spare : 6;
    };
  };
  UINT32 TypeInfo;
} HANDLE_TABLE_ENTRY, *PHANDLE_TABLE_ENTRY;


typedef PHANDLE_TABLE(NTAPI *OB_REF_PROC_HANDLE_TABLE)(
  void* process
  );

typedef int (NTAPI *OB_DEREF_PROC_HANDLE_TABLE)(
  void* process);

typedef int (NTAPI *EX_ENUMERATE_HANDLE_ROUTINE)(
  HANDLE_TABLE *HandleTable,
  HANDLE_TABLE_ENTRY* HandleTableEntry,
  void *Handle,
  void *trackedType
  );

#define ObReferenceProcessHandleTable \
          ((OB_REF_PROC_HANDLE_TABLE)0xfffff803f1a1a3f4)
#define ObDereferenceProcessHandleTable \
          ((OB_DEREF_PROC_HANDLE_TABLE)0xfffff803f1a9cc68)

NTKERNELAPI char NTAPI ExEnumHandleTable(
  __in PHANDLE_TABLE HandleTable,
  __in EX_ENUMERATE_HANDLE_ROUTINE EnumHandleProcedure,
  __in PVOID EnumParameter,
  __out_opt PHANDLE Handle);

NTKERNELAPI INT FASTCALL ExfUnblockPushLock(
  __in INT64 PushLock,
  __in PVOID WaitBlock OPTIONAL);

// Get object address from HANDLE_TABLE_ENTRY struct
void *ObjGetAddr(__in HANDLE_TABLE_ENTRY *handleTableEntry);

// Get object header ptr from HANDLE_TABLE_ENTRY struct
OBJECT_HEADER *ObjGetHeader(__in HANDLE_TABLE_ENTRY *handleTableEntry);

// Translate ObjectPointerBits field from HANDLE_TABLE_ENTRY struct
// to real object ptr
OBJECT_HEADER *CalcObjHeaderPtr(__in UINT64 bits);

#endif  // __HANDLE_TABLE_H__