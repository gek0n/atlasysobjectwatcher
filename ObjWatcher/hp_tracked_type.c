﻿#include <predefine.h>
#include <hplib/hplib.h>
#include <hp_tracked_type.h>

void* GetKeyFromTrackedType(const hp_tracked_type_t *trackedType, void *Ctx)
{
  return trackedType->typeName;
}

int CompareKeyForTrackedType(
      const char *LeftKey,
      const char *RightKey,
      void *Ctx)
{
  return strcmp(LeftKey, RightKey) == 0;
}

unsigned int GetHashForTrackedType(const char *Key, void *Ctx)
{
  uint32 sum = 0;
  uint8 index = 0;
  uint16 ch;
  ch = (uint16)Key[index++];
  while (ch) {
    sum += ch;
    ch = (uint16)Key[index++];
  }
  return sum;
}
