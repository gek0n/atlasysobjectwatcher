#ifndef __DIRHELPERS_H__
#define __DIRHELPERS_H__

struct _COUNT_ENTRY {
  const char *desiredTypeName;
  uint32 *total;
};

int32 PrintObjectTypes(void);

int32 PrintAllNamedObjectsCountByType(void);

logical PrintAllProcessHandlesCount(void);

#endif  // __DIRHELPERS_H__
