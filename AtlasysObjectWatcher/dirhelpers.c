#include <predefine.h>
#include <hplib/hplib.h>
#include <dirhelpers.h>

#define DEFAULT_TYPE_NAME_SIZE 256

static int32 CountEntriesInDir(
               const char *dir,
               const char * desiredTypeName,
               uint32 *total);

static hp_iter_ret_t PrintObjectTypeInfo(
                       const char *root,
                       char *name,
                       char *type,
                       void *arg);

static int32 CountEntriesInDir(
               const char *dir,
               const char * desiredTypeName,
               uint32 *total);

static hp_iter_ret_t PrintNamedObjectsCount(
                       const char *root,
                       char *name,
                       char *type,
                       void *arg);

static logical ConstructPathUnicodeString(
                 const char *root,
                 const char *name,
                 UNICODE_STRING *objPath);

static void PrintTableHeader(void);
static logical GetSysProcInfo(__out SYSTEM_PROCESS_INFORMATION **info);

int32 PrintAllNamedObjectsCountByType(void)
{
  int32 status;
  status = hp_ObjDirectoryIterate(
    "\\ObjectTypes",
    PrintNamedObjectsCount,
    NULL);

  if (HPERROR(status)) {
    HpLogDbg(
      "Can't iterate directory \\ with status %#x\n",
      status);
  }

  return status;
}

logical PrintAllProcessHandlesCount(void)
{
  SYSTEM_PROCESS_INFORMATION *info = NULL;
  logical res;
  res = GetSysProcInfo(&info);
  if (!res) {
    return FALSE;
  }

  uint64 totalHandles = 0;
  uint64 totalProcesses = 0;
  uint64 totalThreads = 0;
  totalHandles += info->HandleCount;
  totalThreads += info->NumberOfThreads;
  totalProcesses++;
  while (info->NextEntryOffset) {
    info = (SYSTEM_PROCESS_INFORMATION *)((uptr_t)info +
      (uptr_t)info->NextEntryOffset);
    totalHandles += info->HandleCount;
    totalThreads += info->NumberOfThreads;
    totalProcesses++;
  }

  if (info) {
    ExFreePoolWithTag(info, 'tseT');
  }

  HpLogDbg("Total handles count: %d\n", totalHandles);
  HpLogDbg("Total processes count: %d\n", totalProcesses);
  HpLogDbg("Total threads count: %d\n", totalThreads);
  HpLogDbg(
    "Total handles count with threads and processes: %d\n",
    totalHandles + totalProcesses + totalThreads);
  return TRUE;
}

int32 PrintObjectTypes(void)
{
  PrintTableHeader();

  int32 status;
  uint32 total = 0;
  status = hp_ObjDirectoryIterate(
    "\\ObjectTypes",
    PrintObjectTypeInfo,
    &total);

  if (HPERROR(status)) {
    HpLogDbg(
      "Can't iterate directory \\ObjectTypes with status %#x\n",
      status);
    return status;
  }

  HpLogDbg("Total objects number: %d\n", total);

  return status;
}

static logical GetSysProcInfo(__out SYSTEM_PROCESS_INFORMATION **info)
{
  uint32 bufSize;
  bufSize = sizeof(SYSTEM_PROCESS_INFORMATION);
  void *buf;
  buf = ExAllocatePoolWithTag(NonPagedPool, bufSize, 'tseT');
  if (!buf) {
    HpLogDbg("Can't allocate buffer\n");
    return FALSE;
  }

  uint32 retLen;
  NTSTATUS ntstatus;
  ntstatus = ZwQuerySystemInformation(
               SystemProcessInformation,
               buf,
               bufSize,
               &retLen);

  if (ntstatus != STATUS_INFO_LENGTH_MISMATCH
    && ntstatus != STATUS_SUCCESS) {
    HpLogDbg("Can't get system info with status %#x\n", ntstatus);
    if (buf) {
      ExFreePoolWithTag(buf, 'tseT');
    }
    return FALSE;
  }

  while (ntstatus == STATUS_INFO_LENGTH_MISMATCH) {
    if (buf) {
      ExFreePoolWithTag(buf, 'tseT');
    }

    bufSize = retLen;
    buf = ExAllocatePoolWithTag(NonPagedPool, bufSize, 'tseT');
    if (!buf) {
      HpLogDbg("Can't allocate buffer\n");
      return FALSE;
    }

    ntstatus = ZwQuerySystemInformation(
                 SystemProcessInformation,
                 buf,
                 bufSize,
                 &retLen);
  }

  if (!NT_SUCCESS(ntstatus)) {
    HpLogDbg("Can't get system info with status %#x\n", ntstatus);
    if (buf) {
      ExFreePoolWithTag(buf, 'tseT');
    }
    return FALSE;
  }
  *info = buf;
  return TRUE;
}

static void PrintTableHeader(void)
{
  char delimiter[121];
  memset(delimiter, (int)'=', 120); delimiter[120] = '\0';
  HpLogDbg(
    "\n%20s %30s %10s %8s %8s %25s\n%s\n",
    "Root dir",
    "Name",
    "Type",
    "Count",
    "Handles",
    "DeleteProcedure",
    delimiter);
}

static hp_iter_ret_t PrintObjectTypeInfo(
                       const char *root,
                       char *name,
                       char *type,
                       void *arg)
{
  logical success;
  UNICODE_STRING objPath;
  success = ConstructPathUnicodeString(root, name, &objPath);
  if (!success) {
    return HP_ITER_RET_FAIL;
  }

  POBJECT_TYPE ObpTypeObjectType;
  ObpTypeObjectType = hp_ObjGetType(*IoFileObjectType);
  if (!ObpTypeObjectType) {
    HpLogErr("Can't get object Type\n");
    return HP_ITER_RET_FAIL;
  }

  NTSTATUS status;
  OBJECT_TYPE *obj;
  status = ObReferenceObjectByName(
             &objPath,
             0,
             NULL,
             0,
             ObpTypeObjectType,
             KernelMode,
             NULL,
             &obj);

  if (!NT_SUCCESS(status)) {
    HpLogDbg("Can't reference object %wZ with status %#x\n", objPath, status);
    return HP_ITER_RET_FAIL;
  }

  HpLogDbg(
    "   %20s %30s %10s %8d %8d     %#p\n",
    root,
    name,
    type,
    obj->TotalNumberOfObjects,
    obj->TotalNumberOfHandles,
    obj->TypeInfo.DeleteProcedure);

  ObDereferenceObject(obj);

  *(uint32 *)arg += obj->TotalNumberOfObjects;

  return HP_ITER_RET_CONTINUE;
}

static hp_iter_ret_t CountEntry(
                       const char *root,
                       char *name,
                       char *type,
                       void *arg)
{
  struct _COUNT_ENTRY entry = *(struct _COUNT_ENTRY *)arg;
  if (!entry.desiredTypeName) {
    *entry.total += 1;
    return HP_ITER_RET_CONTINUE;
  }

  if (strcmp(entry.desiredTypeName, type) == 0) {
    *entry.total += 1;
  }

  if (strcmp(type, "Directory") == 0) {
    int32 len;
    char path[DEFAULT_TYPE_NAME_SIZE];
    if (strcmp(root, "\\") == 0) {
      len = hp_Snprintf(
              path,
              DEFAULT_TYPE_NAME_SIZE * sizeof(char),
              "\\%s",
              name);
    }
    else {
      len = hp_Snprintf(
              path,
              DEFAULT_TYPE_NAME_SIZE * sizeof(char),
              "%s\\%s",
              root,
              name);
    }
    if (!len) {
      HpLogDbg("Can't concat path\n");
      return HP_ITER_RET_FAIL;
    }

    CountEntriesInDir(
      path,
      entry.desiredTypeName,
      entry.total);
  }

  return HP_ITER_RET_CONTINUE;
}

static int32 CountEntriesInDir(
               const char *dir,
               const char * desiredTypeName,
               uint32 *total)
{
  struct _COUNT_ENTRY arg;
  arg.desiredTypeName = desiredTypeName;
  arg.total = total;
  int32 status;
  status = hp_ObjDirectoryIterate(dir, CountEntry, &arg);

  if (HPERROR(status)) {
    HpLogDbg(
      "Can't iterate directory %s with status %#x\n",
      dir,
      status)
      ;
  }

  return status;
}

static logical ConstructPathUnicodeString(
                 const char *root,
                 const char *name,
                 UNICODE_STRING *objPath)
{
  int32 len;
  char path[DEFAULT_TYPE_NAME_SIZE];
  len = hp_Snprintf(
          path,
          DEFAULT_TYPE_NAME_SIZE * sizeof(char),
          "%s\\%s",
          root,
          name);

  if (!len) {
    HpLogDbg("Can't concat path\n");
    return FALSE;
  }

  wchar_t *uPath;
  uPath = hp_Utf8ToUtf16Alloc(path, sizeof(char) * len);
  if (!uPath) {
    HpLogDbg("Can't convert utf8 to utf16\n");
    return FALSE;
  }

  RtlInitUnicodeString(objPath, uPath);

  return TRUE;
}

static hp_iter_ret_t PrintNamedObjectsCount(
                       const char *root,
                       char *name,
                       char *type,
                       void *arg)
{
  uint32 total = 0;
  CountEntriesInDir(
    "\\",
    name,
    &total);
  if (total) {
    HpLogDbg("Type %s : %d\n", name, total);
  }
  return HP_ITER_RET_CONTINUE;
}
