#include <predefine.h>
#include <hplib/hplib.h>
#include <patchlib/patchlib.h>
#include <dirhelpers.h>
#include <ObjWatcher/hp_objwatcher.h>

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD Unload;

NTSTATUS DriverEntry(__in DRIVER_OBJECT *drvObj, __in UNICODE_STRING *regPath)
{
  UNREFERENCED_PARAMETER(regPath);
  drvObj->DriverUnload = Unload;
  
  int32 status;
  status = HplibIni();
  if (HPERROR(status)) {
    HpLogDbg("Can't initialize Hplib with error: %#x\n", status);
    return STATUS_UNSUCCESSFUL;
  }

  status = hp_PatchLibIni();
  if (HPERROR(status)) {
    HpLogDbg("Can't initialize Patchlib with error: %#x\n", status);
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  status = hp_ObjWatcherIni();
  if(HPERROR(status)) {
    HpLogDbg("Can't initialize ObjWatcher\n");
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  hp_tracked_type_t *drivers;
  status = hp_ObjWatcherTrackType("Driver", &drivers);
  if (HPERROR(status)) {
    HpLogDbg("Can't track drivers\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Drivers: %d\n", hp_ObjWatcherCountObjs(drivers));

  hp_tracked_type_t *processes;
  status = hp_ObjWatcherTrackType("Process", &processes);
  if (HPERROR(status)) {
    HpLogDbg("Can't track processes\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Processes: %d\n", hp_ObjWatcherCountObjs(processes));

  hp_tracked_type_t *events;
  status = hp_ObjWatcherTrackType("Event", &events);
  if (HPERROR(status)) {
    HpLogDbg("Can't track events\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Events: %d\n", hp_ObjWatcherCountObjs(events));

  hp_tracked_type_t *semaphores;
  status = hp_ObjWatcherTrackType("Semaphore", &semaphores);
  if (HPERROR(status)) {
    HpLogDbg("Can't track semaphores\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Semaphores: %d\n", hp_ObjWatcherCountObjs(semaphores));

  hp_tracked_type_t *mutants;
  status = hp_ObjWatcherTrackType("Mutant", &mutants);
  if (HPERROR(status)) {
    HpLogDbg("Can't track mutants\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Mutants: %d\n", hp_ObjWatcherCountObjs(mutants));

  hp_tracked_type_t *timers;
  status = hp_ObjWatcherTrackType("Timer", &timers);
  if (HPERROR(status)) {
    HpLogDbg("Can't track timers\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Timers: %d\n", hp_ObjWatcherCountObjs(timers));

  hp_tracked_type_t *files;
  status = hp_ObjWatcherTrackType("File", &files);
  if (HPERROR(status)) {
    HpLogDbg("Can't track files\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Files: %d\n", hp_ObjWatcherCountObjs(files));

  hp_tracked_type_t *keys;
  status = hp_ObjWatcherTrackType("Key", &keys);
  if (HPERROR(status)) {
    HpLogDbg("Can't track keys\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Keys: %d\n", hp_ObjWatcherCountObjs(keys));

  hp_tracked_type_t *devices;
  status = hp_ObjWatcherTrackType("Device", &devices);
  if (HPERROR(status)) {
    HpLogDbg("Can't track devices\n");
    hp_ObjWatcherFin();
    hp_PatchLibFin();
    hp_PatchLibFullCleanup();
    HplibFin();
    return STATUS_UNSUCCESSFUL;
  }

  HpLogDbg("Devices: %d\n", hp_ObjWatcherCountObjs(devices));
  /*
  hp_ObjWatcherUntrack(devices);
  hp_ObjWatcherUntrack(drivers);
  hp_ObjWatcherUntrack(events);
  hp_ObjWatcherUntrack(keys);
  hp_ObjWatcherUntrack(files);
  hp_ObjWatcherUntrack(mutants);
  hp_ObjWatcherUntrack(semaphores);
  hp_ObjWatcherUntrack(timers);
  hp_ObjWatcherUntrack(processes);
  */
  /*
  if (!PrintAllProcessHandlesCount()) {
    return STATUS_UNSUCCESSFUL;
  }
  
  PrintAllNamedObjectsCountByType();
  */
  status = PrintObjectTypes();
  HpLogDbg("Print with status: %#x\n", status);
  return HPSUCCESS(status) ? STATUS_SUCCESS : STATUS_UNSUCCESSFUL;
}

void Unload(__in DRIVER_OBJECT *drvObj)
{
  UNREFERENCED_PARAMETER(drvObj);
  hp_ObjWatcherFin();
  hp_Sleep(1000 * 1000, FALSE);
  hp_PatchLibFin();
  hp_Sleep(2000 * 1000, FALSE);
  hp_PatchLibFullCleanup();
  HplibFin();
}