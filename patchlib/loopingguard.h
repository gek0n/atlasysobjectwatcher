/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __LOOPINGGUARD_H__
#define __LOOPINGGUARD_H__

typedef struct hp_looping_guard hp_looping_guard_t;

hp_looping_guard_t* hp_LoopingGuardCreate(void);
logical hp_LoopingGuardTryToAcquire(hp_looping_guard_t *lg);
void hp_LoopingGuardRelease(hp_looping_guard_t *lg);
void hp_LoopingGuardDestroy(hp_looping_guard_t *lg);

#endif /* __LOOPINGGUARD_H__ */
