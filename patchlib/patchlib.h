/*
 * Copyright (c) 2017 Hyperionix, Inc.
 * All rights reserved.
 */

#ifndef __PATCHLIB_H__
#define __PATCHLIB_H__

typedef struct hp_patch hp_patch_t;

typedef int32
(hp_patch_pre_handler_t)(
  hp_patch_t  *patch,
  void        *arg,
  hp_exectx_t *regs
);

typedef void
(hp_patch_post_handler_t)(
  hp_patch_t  *patch,
  void        *arg,
  hp_exectx_t *regs
);

int32
hp_PatchCreate(
  void                    *target,
  hp_patch_pre_handler_t  *preHandler,
  void                    *preHandlerArg,
  const char              *name,
  hp_patch_t             **patch
);

int32
hp_PatchEnable(
  hp_patch_t *patch
);

int32
hp_PatchDisable(
  hp_patch_t *patch
);

int32
hp_PatchEnableAll(
  void
);

void
hp_PatchDisableAll(
  void
);

void
hp_PatchDestroyAll(
  void
);

const char *
hp_PatchName(
  hp_patch_t *patch
);

void
hp_PatchDestroy(
  hp_patch_t *patch
);

int32
hp_PatchRegisterPostHandler(
  hp_patch_t              *patch,
  hp_patch_post_handler_t *postHandler,
  void                    *postHandlerArg,
  hp_exectx_t             *regs
);

// The function is called by hookexec to register it's handler
void 
hp_PatchSetLuaCallbackHandler(
  hp_patch_pre_handler_t *handler
);

hp_patch_pre_handler_t *
hp_PatchGetLuaCallbackHandler(
  void
);

int32 
hp_PatchLibIni(
  void
);

void 
hp_PatchLibFin(
  void
);

// to be called on driver unload
void
hp_PatchLibFullCleanup(
  void
);

#endif // __PATCHLIB_H__
