#include <predefine.h>
#include <hplib/hplib.h>
#include <helpers.h>
#include <HelpersUnitTests.h>
#include <unittesting.h>

void RunHelpersUnitTests()
{
  ut_run_test(
    ObjGetByName_EmptyFullName_Failure,
    "ObjGetByName_EmptyFullName_Failure");

  ut_run_test(
    ObjGetByName_EmptyTypeName_Failure,
    "ObjGetByName_EmptyTypeName_Failure");

  ut_run_test(
    ObjGetByName_InvalidRetAddr_Failure,
    "ObjGetByName_InvalidRetAddr_Failure");

  ut_run_test(
    ObjGetByName_NotCorrectFullName_Exception,
    "ObjGetByName_NotCorrectFullName_Exception");

  ut_run_test(
    ObjGetByName_NotCorrectTypeName_Exception,
    "ObjGetByName_NotCorrectTypeName_Exception");

  ut_run_test(
    ObjGetByName_NotRealTypeName_NotFound,
    "ObjGetByName_NotRealTypeName_NotFound");

  ut_run_test(
    ObjGetByName_NotRealFullName_NotFound,
    "ObjGetByName_NotRealFullName_NotFound");

  ut_run_test(
    ObjGetByName_RealObjNameAndType_Success,
    "ObjGetByName_RealObjNameAndType_Success");
}

char *ObjGetByName_EmptyFullName_Failure(void)
{
  void *retAddr;
  int32 expected = HPE_FAILURE;

  int32 actual;
  actual = ObjGetByName(NULL, "someType", &retAddr);

  char *errMsg = "Function does not return HPE_FAILURE while" \
                 "fullName param  is NULL\n";
  ut_assert(errMsg, actual == expected);
}

char *ObjGetByName_EmptyTypeName_Failure(void)
{
  void *retAddr;
  int32 expected = HPE_FAILURE;

  int32 actual;
  actual = ObjGetByName("someName", NULL, &retAddr);

  char *errMsg = "Function does not return HPE_FAILURE while" \
                 "typeName param  is NULL\n";
  ut_assert(errMsg, actual == expected);
}

char *ObjGetByName_InvalidRetAddr_Failure(void)
{
  int32 expected = HPE_FAILURE;

  int32 actual;
  actual = ObjGetByName("someName", "someType", NULL);

  char *errMsg = "Function does not return HPE_FAILURE while" \
                 "retAddr param  is NULL\n";
  ut_assert(errMsg, actual == expected);
}

char *ObjGetByName_NotCorrectFullName_Exception(void)
{
  void *retAddr;

  HP_TRY {
    ObjGetByName((char *)0x0012345, "someType", &retAddr);
  }
  HP_CATCH {
    ut_success;
  }
  HP_ENDTRY

  char *errMsg = "Function not failed\n";
  ut_fail(errMsg);
}

char *ObjGetByName_NotCorrectTypeName_Exception(void)
{
  void *retAddr;

  HP_TRY {
    ObjGetByName("someName", (char *)0x0012345, &retAddr);
  }
  HP_CATCH {
    ut_success;
  }
  char *errMsg = "Function not failed\n";
  ut_fail(errMsg);
}

char *ObjGetByName_NotRealTypeName_NotFound(void)
{
  void *retAddr;
  int32 expected = HPE_NOTFOUND;

  int32 actual;
  actual = ObjGetByName(
             "TestObjWatcher",
             "someStupid_notExistenType",
             &retAddr);

  char *errMsg = "Function does not return HPE_NOTFOUND while" \
                 "typeName is not real\n";
  ut_assert(errMsg, actual == expected);
}

char *ObjGetByName_NotRealFullName_NotFound(void)
{
  void *retAddr;
  int32 expected = HPE_NOTFOUND;

  int32 actual;
  actual = ObjGetByName("someStupid_NotExistentName", "Driver", &retAddr);

  char *errMsg = "Function does not return HPE_NOTFOUND while" \
                 "fullName is not real\n";
  ut_assert(errMsg, actual == expected);
}

char *ObjGetByName_RealObjNameAndType_Success(void)
{
  void *retAddr;
  int32 expected = HPE_SUCCESS;

  int32 actual;
  actual = ObjGetByName("\\FileSystem\\RAW", "Driver", &retAddr);

  char *errMsg;
  char *fmtStr = "Function does not find object %#p\n";
  errMsg = hp_Malloc(sizeof(char) * strlen(fmtStr));
  sprintf(errMsg, fmtStr, retAddr);
  hp_Free(errMsg);
  ut_assert(errMsg, (actual == expected) && retAddr);
}
