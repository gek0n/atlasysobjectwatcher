#include <predefine.h>
#include <hplib/hplib.h>
#include <hp_tracked_obj.h>
#include <TrackedObjUnitTests.h>
#include <unittesting.h>

void RunTrackedObjUnitTests(void)
{
  HpLogDbg("\n\t\t[TRACKED OBJ UNIT TESTS]\n");
  ut_run_test(to_GetKeyReturnExpectedKey, "to_GetKeyReturnExpectedKey");

  ut_run_test(to_CompareKeyEqualKey, "to_CompareKeyEqualKey");

  ut_run_test(to_GetHashNotEqualKeys, "to_GetHashNotEqualKeys");
}

char *to_GetKeyReturnExpectedKey(void)
{
  void *expected = 0x12345678;
  hp_tracked_obj_t trackedObj;
  trackedObj.addr = expected;

  void *actual = GetKeyFromTrackedObj(&trackedObj, NULL);

  char *errMsg = "Given key %s does not equal gotten key %s\n";
  ut_assert(errMsg, expected == actual);
}

char *to_CompareKeyEqualKey(void)
{
  void *leftKey = 0x12345678;
  void *rightKey = 0x12345678;

  const logical expected = TRUE;
  logical actual = CompareKeyForTrackedObj(leftKey, rightKey, NULL);

  char *errMsg = "Keys does not equals\n";
  ut_assert(errMsg, actual == expected);
}

char *to_GetHashNotEqualKeys(void)
{
  void *oneKey = 0x12345678;
  void *anotherKey = 0x12345679;

  unsigned int expected = GetHashForTrackedObj(oneKey, NULL);
  unsigned int actual = GetHashForTrackedObj(anotherKey, NULL);

  char *errMsg = "Different keys has same hashes\n";
  ut_assert(errMsg, actual != expected);
}
