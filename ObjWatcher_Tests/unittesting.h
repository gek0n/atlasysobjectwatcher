#ifndef __UNITTESTING_H__
#define __UNITTESTING_H__

#define ut_assert(msg, test) {if (!(test)) return msg; else return NULL;}

#define ut_success return 0;

#define ut_fail(msg) return msg;

#define ut_run_test(test, testname) {char *errMsg = test(); \
  if (errMsg) HpLogDbg("[FAIL]\t%s\n%s", testname, errMsg); \
  else HpLogDbg("[OK]\t%s\n", testname);}

#endif  // __UNITTESTING_H__
