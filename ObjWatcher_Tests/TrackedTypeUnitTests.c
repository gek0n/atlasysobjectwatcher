﻿#include <predefine.h>
#include <hplib/hplib.h>
#include <hp_tracked_type.h>
#include <TrackedTypeUnitTests.h>
#include <unittesting.h>

void RunTrackedTypeUnitTests(void)
{
  HpLogDbg("\n\t\t[TRACKED TYPE UNIT TESTS]\n");
  ut_run_test(tt_GetKeyReturnNotEmptyKey, "tt_GetKeyReturnNotEmptyKey");
  ut_run_test(tt_GetKeyReturnEmptyKey, "tt_GetKeyReturnEmptyKey");

  ut_run_test(tt_CompareKeyEqualKey, "tt_CompareKeyEqualKey");
  ut_run_test(tt_CompareKeySameKey, "tt_CompareKeySameKey");

  ut_run_test(tt_GetHashEqualKeys, "tt_GetHashEqualKeys");
  ut_run_test(tt_GetHashNotEqualKeys, "tt_GetHashNotEqualKeys");
}

char *tt_GetKeyReturnNotEmptyKey(void)
{
  const char *expected = "SomeKey";
  hp_tracked_type_t trackedType;
  trackedType.typeName = hp_StrDup(expected);

  char *actual = GetKeyFromTrackedType(&trackedType, NULL);
  hp_StrFree(trackedType.typeName);

  char *errMsg = "Given key %s does not equal gotten key %s\n";
  ut_assert(errMsg, strcmp(expected, actual) == 0);
}

char *tt_GetKeyReturnEmptyKey(void)
{
  const char *expected = "";
  hp_tracked_type_t trackedType;
  trackedType.typeName = hp_StrDup(expected);

  char *actual = GetKeyFromTrackedType(&trackedType, NULL);
  hp_StrFree(trackedType.typeName);

  char *errMsg = "Given key %s does not equal gotten key %s\n";
  ut_assert(errMsg, strlen(actual) == 0);
}

char *tt_CompareKeySameKey(void)
{
  const char *key = "SomeKey";

  const logical expected = TRUE;
  logical actual = CompareKeyForTrackedType(key, key, NULL);

  char *errMsg = "Same key does not equal\n";
  ut_assert(errMsg, actual == expected)
}

char *tt_CompareKeyEqualKey(void)
{
  const char *leftKey = "SomeKey";
  char *rightKey = hp_StrDup(leftKey);

  const logical expected = TRUE;
  logical actual = CompareKeyForTrackedType(leftKey, rightKey, NULL);
  hp_StrFree(rightKey);

  char *errMsg = "Same key does not equal\n";
  ut_assert(errMsg, actual == expected)
}

char *tt_GetHashEqualKeys(void)
{
  const char *leftKey = "SomeKey";
  char *rightKey = hp_StrDup(leftKey);

  unsigned int expected = GetHashForTrackedType(leftKey, NULL);
  unsigned int actual = GetHashForTrackedType(rightKey, NULL);
  hp_StrFree(rightKey);

  char *errMsg = "Same key has not equal hashes\n";
  ut_assert(errMsg, actual == expected)
}

char *tt_GetHashNotEqualKeys(void)
{
  const char *leftKey = "SomeKey";
  const char *rightKey = "OtherKey";

  unsigned int expected = GetHashForTrackedType(leftKey, NULL);
  unsigned int actual = GetHashForTrackedType(rightKey, NULL);

  char *errMsg = "Not same key has equal hashes\n";
  ut_assert(errMsg, actual != expected)
}
