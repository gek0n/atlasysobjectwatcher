#ifndef __TRACKEDOBJ_UNITTESTS_H__
#define __TRACKEDOBJ_UNITTESTS_H__

void RunTrackedObjUnitTests(void);

char *to_GetKeyReturnExpectedKey(void);

char *to_CompareKeyEqualKey(void);

char *to_GetHashNotEqualKeys(void);

#endif  // __TRACKEDOBJ_UNITTESTS_H__