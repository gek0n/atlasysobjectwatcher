#include <predefine.h>
#include <hplib/hplib.h>
#include <patchlib/patchlib.h>
#include <TrackedTypeUnitTests.h>
#include <TrackedObjUnitTests.h>
#include <HelpersUnitTests.h>

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD Unload;

NTSTATUS DriverEntry(__in DRIVER_OBJECT *drvObj, __in UNICODE_STRING *regPath)
{
  HP_UNUSED_PARAM(regPath);
  drvObj->DriverUnload = Unload;

  int32 status;
  status = HplibIni();
  if (HPERROR(status)) {
    HpLogErr("Can't init hplib with status %#x\n", status);
    return STATUS_APP_INIT_FAILURE;
  }

  status = hp_PatchLibIni();
  if (HPERROR(status)) {
    HpLogErr("Can't init patchlib with status %#x\n", status);
    HplibFin();
    return STATUS_APP_INIT_FAILURE;
  }

  RunTrackedTypeUnitTests();
  RunTrackedObjUnitTests();
  RunHelpersUnitTests();

  return STATUS_SUCCESS;
}

void Unload(__in DRIVER_OBJECT *drvObj)
{
  HP_UNUSED_PARAM(drvObj);
  hp_PatchLibFin();
  hp_Sleep(2000 * 1000, FALSE);
  hp_PatchLibFullCleanup();
  HplibFin();
}
