#ifndef __TRACKEDTYPE_UNITTESTS_H__
#define __TRACKEDTYPE_UNITTESTS_H__

void RunTrackedTypeUnitTests(void);

char *tt_GetKeyReturnEmptyKey(void);
char *tt_GetKeyReturnNotEmptyKey(void);

char *tt_CompareKeySameKey(void);
char *tt_CompareKeyEqualKey(void);

char *tt_GetHashNotEqualKeys(void);
char *tt_GetHashEqualKeys(void);

#endif  // __TRACKEDTYPE_UNITTESTS_H__