#ifndef __HELPERS_UNITTESTS_H__
#define __HELPERS_UNITTESTS_H__

void RunHelpersUnitTests(void);

char *ObjGetByName_EmptyFullName_Failure(void);
char *ObjGetByName_EmptyTypeName_Failure(void);
char *ObjGetByName_InvalidRetAddr_Failure(void);

char *ObjGetByName_NotCorrectFullName_Exception(void);
char *ObjGetByName_NotCorrectTypeName_Exception(void);

char *ObjGetByName_NotRealTypeName_NotFound(void);
char *ObjGetByName_NotRealFullName_NotFound(void);

char *ObjGetByName_RealObjNameAndType_Success(void);

#endif  // __HELPERS_UNITTESTS_H__